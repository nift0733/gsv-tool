#ifndef SYNTAXVALIDATOR_H
#define SYNTAXVALIDATOR_H

#include <QList>
#include <memory>

#include "../Models/VotingStructure.h"

class SyntaxValidator
{
public:
    class Result {
        friend class SyntaxValidator;
    public:
        bool isValid() { return valid; }
        bool hasRootParent() { return rootHasParent; }
        QList<std::shared_ptr<Node>> getUnreachableNodes() { return unreachableNodes; }
        QStringList getDuplicates() { return duplicates; }
    private:
        bool valid;
        bool rootHasParent;
        QList<std::shared_ptr<Node>> unreachableNodes;
        QStringList duplicates;
    };

    /**
     * @brief Validates the syntax of the voting structure.
     */
    static Result validate(std::shared_ptr<VotingStructure> votingStructure);

private:
    /**
     * @brief Finds nodes not connected to the root node by any path.
     */
    static void findNotConnected(std::shared_ptr<Node> node, std::shared_ptr<VotingStructure> votingStructure, QList<std::shared_ptr<Node>> &notVisited);

    /**
     * @brief Finds duplicates of nodes referring to their name.
     */
    static bool findDuplicates(std::shared_ptr<VotingStructure> votingStructure, QStringList &duplicates);
};

#endif // SYNTAXVALIDATOR_H
