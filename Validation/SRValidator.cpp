#include "SRValidator.h"

SRValidator::Result SRValidator::validate(std::vector<std::shared_ptr<QuorumList>> quorumLists, std::vector<QString> operations) {
    Result result;
    result.readValid = true;
    result.writeValid = true;

    std::shared_ptr<QuorumList> readList;
    std::shared_ptr<QuorumList> writeList;
    if(operations[0] == "read") {
        readList = quorumLists[0];
        writeList = quorumLists[1];
    } else {
        readList = quorumLists[1];
        writeList = quorumLists[0];
    }

    result.readEmpty = !readList->buckets();
    result.writeEmpty = !writeList->buckets();

    if(!checkIntersection(readList, writeList))
        result.readValid = false;
    if(!checkIntersection(writeList, writeList))
        result.writeValid = false;

    return result;
}

bool SRValidator::checkIntersection(std::shared_ptr<QuorumList> left, std::shared_ptr<QuorumList> right) {
    int quorumSize = left->getQuorumSize();
    for(int leftBucket=0; leftBucket<left->buckets(); ++leftBucket) {
        for(int leftIndex=0; leftIndex<left->bucketSize(leftBucket); ++leftIndex) {
            for(int rightBucket=0; rightBucket<right->buckets(); ++rightBucket) {
                for(int rightIndex=0; rightIndex<right->bucketSize(rightBucket); ++rightIndex) {
                    for(int i=0; i<quorumSize; ++i) {
                        if(!(left->at(leftBucket, leftIndex)[i] & right->at(rightBucket, rightIndex)[i]))
                            return false;
                    }
                }
            }
        }
    }
    return true;
}
