#ifndef SRVALIDATOR_H
#define SRVALIDATOR_H

#include <memory>
#include <vector>
#include <QString>

#include "../Models/QuorumList.h"

class SRValidator
{
public:
    class Result {
        friend class SRValidator;
    public:
        bool isValid() { return readValid && writeValid && !readEmpty && !writeEmpty; }
        bool isReadValid() { return readValid; }
        bool isWriteValid() { return writeValid; }
        bool isReadEmpty() { return readEmpty; }
        bool isWriteEmpty() { return writeEmpty; }
    private:
        bool readValid;
        bool writeValid;
        bool readEmpty;
        bool writeEmpty;
    };

    /**
     * @brief Validates the quorum list to be 1-SR.
     */
    static Result validate(std::vector<std::shared_ptr<QuorumList>> quorumLists, std::vector<QString> operations);

private:
    /**
     * @brief Checks for the intersection property required by 1-SR.
     */
    static bool checkIntersection(std::shared_ptr<QuorumList> left, std::shared_ptr<QuorumList> right);
};

#endif // SRVALIDATOR_H
