#include "SyntaxValidator.h"

#include <QVector>
#include <algorithm>

SyntaxValidator::Result SyntaxValidator::validate(std::shared_ptr<VotingStructure> votingStructure) {
    Result result;
    result.valid = true;
    result.rootHasParent = false;

    QList<std::shared_ptr<Node>> notVisited(QVector<std::shared_ptr<Node>>::fromStdVector(votingStructure->getNodes()).toList());
    findNotConnected(votingStructure->getRoot(), votingStructure, notVisited);
    if(!notVisited.isEmpty()) {
        result.unreachableNodes = notVisited;
        result.valid = false;
    }
    if(findDuplicates(votingStructure, result.duplicates)) {
        result.valid = false;
    }
    auto edges = votingStructure->getEdges();
    if(std::any_of(edges.begin(), edges.end(), [&votingStructure] (std::shared_ptr<Edge> edge) {return edge->to == votingStructure->getRoot();})) {
        result.rootHasParent = true;
        result.valid = false;
    }

    return result;
}

void SyntaxValidator::findNotConnected(std::shared_ptr<Node> node, std::shared_ptr<VotingStructure> votingStructure, QList<std::shared_ptr<Node>> &notVisited) {
    if(!notVisited.contains(node))
        return;

    notVisited.removeOne(node);

    for(auto edge : votingStructure->getEdges()) {
        if(edge->from != node)
            continue;
        findNotConnected(edge->to, votingStructure, notVisited);
    }
}

bool SyntaxValidator::findDuplicates(std::shared_ptr<VotingStructure> votingStructure, QStringList &duplicates) {
    QStringList firstOccurences;
    for(auto &&node : votingStructure->getNodes()) {
        if(firstOccurences.contains(node->identifier)) {
            if(!duplicates.contains(node->identifier))
                duplicates.append(node->identifier);
        } else {
            firstOccurences.append(node->identifier);
        }
    }
    return duplicates.size() > 0;
}
