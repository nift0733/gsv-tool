#include "TopologyWriter.h"

#include <QFile>
#include <QTextStream>

bool TopologyWriter::writeFullTopologyToFile(QString fileName, int physicalNodes)
{
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    try {
        QTextStream out(&file);

        out << "define variables" << "\r\n";
        out << "\r\n";
        out << "p" << "\r\n";
        out << "\r\n";
        out << "\r\n";
        out << "define nodes" << "\r\n";
        out << "\r\n";
        for(int i=1; i<=physicalNodes; ++i)
            out << "R" << i << "\tp\t1" << "\r\n";
        out << "\r\n";
        out << "\r\n";
        out << "define topology" << "\r\n";
        out << "\r\n";

        // topology header line
        out << "//";
        for(int i=1; i<=physicalNodes; ++i)
            out << "\tR" << i;
        out << "\r\n";

        // topology lines
        for(int i=1; i<=physicalNodes; ++i) {
            out << "R" << i;
            for(int j=1; j<=physicalNodes; ++j)
                out << "\t1";
            out << "\r\n";
        }
    } catch (std::exception &) {
        file.close();
        return false;
    }
    file.close();
    return true;
}
