#include "VotingStructureWriter.h"

#include <QBuffer>
#include <QFile>
#include <QXmlSchema>
#include <QXmlSchemaValidator>
#include <QtXml>

#include "Zip.h"
#include "QuorumListWriter.h"

bool VotingStructureWriter::writeToFile(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology, QString fileName) {
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly))
        return false;

    QFile topologyFile(topology->getFilePath());
    if(!topologyFile.exists())
        return false;

    QByteArray xml;
    if(!writeXml(votingStructure, xml))
        return false;

    QByteArray metaXml;
    if(!writeMetaXml(votingStructure, metaXml))
        return false;

    QBuffer xmlFile(&xml);
    QBuffer metaXmlFile(&metaXml);

    QString expectedQuorumLists = QuorumListWriter::writeQuorumLists(votingStructure, topology);
    QByteArray expectedQuorumListsData = expectedQuorumLists.toUtf8();
    QBuffer expectedQuorumListsFile(&expectedQuorumListsData);

    Zip out(file);
    out.addFile(topology->getFileName() + ".top", topologyFile);
    out.addFile("graph.xml", xmlFile);
    out.addFile("graph.meta.xml", metaXmlFile);
    if(expectedQuorumLists != "") {
        out.addFile("verifyQuorumLists.txt", expectedQuorumListsFile);
    }
    return out.save();
}

bool VotingStructureWriter::exportToFile(std::shared_ptr<VotingStructure> votingStructure, QString fileName) {
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly))
        return false;

    QByteArray xml;
    if(!writeXml(votingStructure, xml))
        return false;

    QBuffer xmlFile(&xml);
    return xml.length() == file.write(xml);
}

bool VotingStructureWriter::writeXml(std::shared_ptr<VotingStructure> votingStructure, QByteArray &result) {
    QDomDocument xml;
    xml.appendChild(xml.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));
    QDomElement rootElement = xml.createElement("votingStructure");
    rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
    rootElement.setAttribute("xsi:noNamespaceSchemaLocation", "votingstructure.xsd");

    QDomElement operations = xml.createElement("operations");
    for(auto opName : votingStructure->getOperations()) {
        QDomElement op = xml.createElement("op");
        op.setAttribute("name", opName);
        operations.appendChild(op);
    }
    rootElement.appendChild(operations);

    QDomElement graph = xml.createElement("graph");
    QDomElement root = xml.createElement("root");
    root.setAttribute("node", votingStructure->getRoot()->getId());
    graph.appendChild(root);

    for(auto nodeEntity : votingStructure->getNodes()) {
        QDomElement node = xml.createElement("node");
        node.setAttribute("id", nodeEntity->getId());
        node.setAttribute("type", nodeEntity->isVirtual ? "virtual" : "physical");
        node.setAttribute("name", nodeEntity->identifier);
        node.setAttribute("vote", nodeEntity->vote);
        for(size_t i=0; i<nodeEntity->quorumVector.size(); ++i) {
            QDomElement quorum = xml.createElement("quorum");
            quorum.setAttribute("op", votingStructure->getOperations()[i]);
            quorum.setAttribute("value", nodeEntity->quorumVector[i]);
            node.appendChild(quorum);
        }
        graph.appendChild(node);
    }

    for(auto edgeEntity : votingStructure->getEdges()) {
        QDomElement edge = xml.createElement("edge");

        QDomElement source = xml.createElement("source");
        source.setAttribute("node", edgeEntity->from->getId());
        edge.appendChild(source);

        QDomElement target = xml.createElement("target");
        target.setAttribute("node", edgeEntity->to->getId());
        edge.appendChild(target);

        for(size_t i=0; i<edgeEntity->penaltyList.size(); ++i) {
            QDomElement penalty = xml.createElement("penalty");
            penalty.setAttribute("op", votingStructure->getOperations()[i]);
            penalty.setAttribute("value", edgeEntity->penaltyList[i] >= MAX_PENALTY ? XSD_PRIO_INF : edgeEntity->penaltyList[i] < 0 ? XSD_PRIO_FILLUP : QString::number(edgeEntity->penaltyList[i]));
            edge.appendChild(penalty);
        }
        graph.appendChild(edge);
    }

    rootElement.appendChild(graph);
    xml.appendChild(rootElement);
    result = xml.toByteArray();

    QXmlSchema schema;
    if(!schema.load(votingstructureXsd) || !schema.isValid())
        return false;

    QXmlSchemaValidator validator(schema);
    return validator.validate(result);
}

bool VotingStructureWriter::writeMetaXml(std::shared_ptr<VotingStructure> votingStructure, QByteArray &result) {
    QDomDocument xml;
    xml.appendChild(xml.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));
    QDomElement rootElement = xml.createElement("votingStructureMeta");
    rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
    rootElement.setAttribute("xsi:noNamespaceSchemaLocation", "votingstructure_meta.xsd");

    for(auto nodeEntity : votingStructure->getNodes()) {
        QDomElement node = xml.createElement("node");
        node.setAttribute("id", nodeEntity->getId());
        node.setAttribute("x", nodeEntity->x);
        node.setAttribute("y", nodeEntity->y);
        rootElement.appendChild(node);
    }

    xml.appendChild(rootElement);
    result = xml.toByteArray();

    QXmlSchema schema;
    if(!schema.load(votingstructureMetaXsd) || !schema.isValid())
        return false;

    QXmlSchemaValidator validator(schema);
    return validator.validate(result);
}
