#ifndef UNZIP_H
#define UNZIP_H

#include <QIODevice>
#include <QStringList>

class Unzip
{
public:
    Unzip(QIODevice& inputFile);
    ~Unzip();

    /**
     * @brief Gets the file names of the archieve.
     */
    QStringList getFileNames();
    /**
     * @brief Gets the file named fileName.
     */
    QByteArray* getFile(QString fileName);

private:
    QIODevice& inputFile;
    QStringList fileNames;
    QList<uint32_t> fileOffsets;
    QList<uint32_t> fileSizes;
    QList<uint32_t> crc32;
    QList<QByteArray*> files;

    uint32_t crc32Table[256];

    /**
     * @brief Reads the file at index.
     */
    QByteArray* readFile(uint16_t index);
    /**
     * @brief Processes the archieve.
     */
    void processZipFile();

    /**
     * @brief Read one byte.
     */
    uint8_t read1(bool* ok);
    /**
     * @brief Read two bytes
     */
    uint16_t read2(bool* ok);
    /**
     * @brief Read four bytes.
     */
    uint32_t read4(bool* ok);
    /**
     * @brief Reads a string with length.
     */
    QString readString(bool* ok, uint16_t length);
    /**
     * @brief Searches th archieve for a sequence of bytes.
     */
    bool search(char byte1, char byte2, char byte3, char byte4, uint64_t maxSearch);

    /**
     * @brief Generates a table for calculation of CRC32 hash.
     */
    void generateCRC32Table();
};

#endif // UNZIP_H
