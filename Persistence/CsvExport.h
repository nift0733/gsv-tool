#ifndef CSVEXPORT_H
#define CSVEXPORT_H

#include <vector>
#include <QString>

class CsvExport
{
public:
    /**
     * @brief Exports the given values with the headers to a csv-file.
     */
    static bool exportValues(QString fileName, std::vector<QString> headers, std::vector<std::vector<double>> values);
};

#endif // CSVEXPORT_H
