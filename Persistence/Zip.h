#ifndef ZIP_H
#define ZIP_H

#include <QDateTime>
#include <QIODevice>
#include <QString>

class Zip
{
public:
    Zip(QIODevice& outputFile);

    /**
     * @brief Adds a file with name fileName to the archieve.
     */
    void addFile(QString fileName, QIODevice& file);
    /**
     * @brief Saves the archieve.
     */
    bool save();

private:
    QIODevice& out;
    uint16_t countOfFiles;
    uint32_t centralSize;
    bool triedToSave;
    uint32_t crc32Table[256];
    uint32_t timestamp;
    uint32_t centralOffset;
    QList<QIODevice*> files;
    QList<QString> fileNames;
    QList<uint32_t> crc32;
    QList<uint32_t> sizes;
    QList<uint32_t> localOffsets;

    /**
     * @brief Generates a table for calculation of CRC32 hash.
     */
    void generateCRC32Table();
    /**
     * @brief Generates the timestamp used in the archieve.
     */
    void generateTimestamp();
    /**
     * @brief Transform timestamp to used format in ZIP.
     */
    uint32_t toFatTimestamp(QDateTime datetime);

    /**
     * @brief Writes the local part of the file at index.
     */
    bool writeLocal(uint16_t index);
    /**
     * @brief Writes the content of the file at index.
     */
    bool writeContent(uint16_t index);
    /**
     * @brief Writes the central part of the file at index.
     */
    bool writeCentral(uint16_t index);
    /**
     * @brief Writes the end part of the archieve.
     */
    bool writeEnd();

    /**
     * @brief Writes the bytes at the current position.
     */
    bool writeBytes(QByteArray bytes);
    /**
     * @brief Writes the bytes at the current position.
     */
    bool writeBytes(uint8_t byte1, uint8_t byte2);
    /**
     * @brief Writes the bytes at the current position.
     */
    bool writeBytes(uint8_t byte1, uint8_t byte2, uint8_t byte3, uint8_t byte4);
    /**
     * @brief Writes the bytes at the current position.
     */
    bool writeUInt16(uint16_t value);
    /**
     * @brief Writes the bytes at the current position.
     */
    bool writeUInt32(uint32_t value);
};

#endif // ZIP_H
