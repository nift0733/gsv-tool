#include "VotingStructureReader.h"

#include <QFile>
#include <QMap>
#include <QRandomGenerator>
#include <QXmlSchema>
#include <QXmlSchemaValidator>
#include <QtXml>
#include <algorithm>
#include <vector>

#include "../Config/Path.h"
#include "../Persistence/TopologyReader.h"
#include "../Persistence/QuorumListReader.h"

#define VSR_ERROR_VALUE QPair<std::shared_ptr<VotingStructure>, std::shared_ptr<Topology>>(nullptr, nullptr)

QPair<std::shared_ptr<VotingStructure>, std::shared_ptr<Topology>> VotingStructureReader::readFromFile(QString fileName) {
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
        return VSR_ERROR_VALUE;
    Unzip unzip(file);

    QString top, xml, metaXml, expectedQuorumLists;
    if(!getFileNames(unzip, top, xml, metaXml, expectedQuorumLists))
        return VSR_ERROR_VALUE;

    auto topology = readTopology(unzip, top);
    if(!topology)
        return VSR_ERROR_VALUE;

    auto votingStructure = readXml(*unzip.getFile(xml));
    if(votingStructure == nullptr || !validateVotingStructure(votingStructure, topology) || !readMetaXml(unzip, metaXml, votingStructure))
        return VSR_ERROR_VALUE;

    if(expectedQuorumLists != "") {
        bool verifySimpleSet = false;
        auto quorumLists = readExpectedQuorumLists(*unzip.getFile(expectedQuorumLists), topology->getNodes(), verifySimpleSet);
        if(!operationsEqual(quorumLists, votingStructure->getOperations()))
            return VSR_ERROR_VALUE;
        votingStructure->verifySimpleSet = verifySimpleSet;
        votingStructure->setExpectedQuorumLists(quorumLists);
    }

    votingStructure->setFilePath(fileName);
    return QPair<std::shared_ptr<VotingStructure>, std::shared_ptr<Topology>>(votingStructure, topology);
}

std::shared_ptr<VotingStructure> VotingStructureReader::importFromFile(QString fileName, std::shared_ptr<Topology> topology) {
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
        return nullptr;

    auto buffer = file.readAll();
    auto votingStructure = readXml(buffer);
    if(votingStructure == nullptr || !validateVotingStructure(votingStructure, topology))
        return nullptr;

    arrangeImportedStructure(votingStructure);
    return votingStructure;
}

bool VotingStructureReader::getFileNames(Unzip &unzip, QString &top, QString &xml, QString &metaXml, QString &expectedQuorumLists) {
    auto fileNames = unzip.getFileNames();
    for(auto fileName : fileNames) {
        if(fileName == "verifyQuorumLists.txt")
            expectedQuorumLists = fileName;
        else if(fileName.endsWith(".top"))
            top = fileName;
        else if(fileName.endsWith(".meta.xml"))
            metaXml = fileName;
        else if(fileName.endsWith(".xml"))
            xml = fileName;
    }
    if(top == "" || xml == "" || metaXml == "")
        return false;
    return true;
}

std::shared_ptr<Topology> VotingStructureReader::readTopology(Unzip &unzip, QString fileName) {
    auto topologyFile = unzip.getFile(fileName);
    if(!topologyFile)
        return nullptr;
    QFile cacheTop(Path::getCachePath(fileName));
    if(!cacheTop.open(QIODevice::WriteOnly))
        return nullptr;
    if(cacheTop.write(*topologyFile, topologyFile->length()) != topologyFile->length())
        return nullptr;
    cacheTop.close();

    auto topology = TopologyReader::readFromFile(Path::getCachePath(fileName));
    if(!topology->isValid())
        return nullptr;
    return topology;
}

std::shared_ptr<VotingStructure> VotingStructureReader::readXml(QByteArray& xmlFile) {

#ifndef TEST_STRATEGIES
    QXmlSchema schema;
    if(!schema.load(votingstructureXsd) || !schema.isValid())
        return nullptr;

    QXmlSchemaValidator validator(schema);
    if(!validator.validate(xmlFile))
        return nullptr;
#endif

    QDomDocument xml;
    if(!xml.setContent(xmlFile, false))
        return nullptr;
    QDomElement root = xml.documentElement();

    QDomElement operations = root.firstChildElement("operations");
    int numberOfOperations = operations.childNodes().size();
    std::vector<QString> opResults(numberOfOperations);
    QMap<QString, int> opIndexes;
    for(int i=0; i<numberOfOperations; ++i) {
        auto op = operations.childNodes().at(i).toElement();
        opResults[i] = op.attribute("name");
        opIndexes.insert(opResults[i], i);
    }
    auto result = std::make_shared<VotingStructure>(opResults);

    QMap<int, std::shared_ptr<Node>> resultNodes;
    QDomElement graph = root.firstChildElement("graph");
    for(int i=0; i<graph.childNodes().size(); ++i) {
        auto element = graph.childNodes().at(i).toElement();
        if(element.tagName() != "node")
            continue;
        auto nodeId = element.attribute("id").toInt();
        auto resultNode = std::make_shared<Node>(nodeId, numberOfOperations);
        resultNode->identifier = element.attribute("name");
        resultNode->isVirtual = element.attribute("type") == "virtual";
        resultNode->vote = element.attribute("vote").toInt();
        for(int j=0; j<element.childNodes().size(); ++j) {
            auto quorum = element.childNodes().at(j).toElement();
            resultNode->quorumVector[opIndexes[quorum.attribute("op")]] = quorum.attribute("value").toInt();
        }
        if(!result->addNode(resultNode))
            return nullptr;
        resultNodes.insert(nodeId, resultNode);
    }

    uint32_t rootId = graph.firstChildElement("root").attribute("node").toUInt();
    result->setRoot(resultNodes[rootId]);

    for(int i=0; i<graph.childNodes().size(); ++i) {
        auto element = graph.childNodes().at(i).toElement();
        if(element.tagName() != "edge")
            continue;
        auto sourceId = element.firstChildElement("source").attribute("node").toInt();
        auto targetId = element.firstChildElement("target").attribute("node").toInt();
        auto resultEdge = result->addEdge(resultNodes[sourceId], resultNodes[targetId]);
        if(!resultEdge)
            return nullptr; // edge with opponent edge detected
        for(int j=0; j<element.childNodes().size(); ++j) {
            auto penalty = element.childNodes().at(j).toElement();
            if(penalty.tagName() != "penalty")
                continue;
            auto value = penalty.attribute("value");
            auto resultValue = value == XSD_PRIO_INF ? MAX_PENALTY : value == XSD_PRIO_FILLUP ? -1 : value.toInt();
            resultEdge->penaltyList[opIndexes[penalty.attribute("op")]] = resultValue;
        }
    }

    return result;
}

bool VotingStructureReader::validateVotingStructure(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology) {
    for(auto node : votingStructure->getNodes()) {
        if(node->isVirtual)
            continue;
        bool ok = false;
        for(auto name : topology->getNodes()) {
            if(name == node->identifier) {
                ok = true;
                break;
            }
        }
        if(!ok)
            return false;
    }
    return true;
}

bool VotingStructureReader::readMetaXml(Unzip &unzip, QString fileName, std::shared_ptr<VotingStructure> votingStructure) {
    QMap<int, std::shared_ptr<Node>> nodes;
    for(auto node : votingStructure->getNodes()) {
        nodes.insert(node->getId(), node);
    }

    auto xmlMetaFile = unzip.getFile(fileName);
#ifndef TEST_STRATEGIES
    QXmlSchema schema;
    if(!schema.load(votingstructureMetaXsd) || !schema.isValid())
        return false;

    QXmlSchemaValidator validator(schema);
    if(!validator.validate(*xmlMetaFile))
        return false;
#endif

    QDomDocument xml;
    xml.setContent(*xmlMetaFile, false);
    QDomElement root = xml.documentElement();
    if(root.childNodes().size() != nodes.size())
        return false;

    for(int i=0; i<root.childNodes().size(); ++i) {
        auto xmlNode = root.childNodes().at(i).toElement();
        auto xmlNodeId = xmlNode.attribute("id").toInt();
        if(!nodes.contains(xmlNodeId))
            return false;
        auto node = nodes[xmlNodeId];
        node->x = xmlNode.attribute("x").toInt();
        node->y = xmlNode.attribute("y").toInt();
    }
    return true;
}

void VotingStructureReader::arrangeImportedStructure(std::shared_ptr<VotingStructure> votingStructure) {
    QList<std::shared_ptr<Node>> list;
    auto maxDepth = getMaxDepth(votingStructure->getRoot(), votingStructure, list);
    QRandomGenerator rand;
    auto x = maxDepth * 200;
    auto y = 100;
    QList<QPair<std::shared_ptr<Node>, int>> nodes;
    nodes.append(QPair<std::shared_ptr<Node>, int>(votingStructure->getRoot(), maxDepth));
    while(!nodes.empty()) {
        auto next = nodes.takeFirst();
        if(next.second < 0) {
            x = next.first->x - next.second * 150;
            y = next.first->y;
        } else {
            nodes.insert(0, QPair<std::shared_ptr<Node>, int>(next.first, -next.second));
            next.first->x = x + rand.bounded(-10, 10);
            next.first->y = y + rand.bounded(-10, 10);
            auto children = votingStructure->getChildren(next.first);
            x -= (next.second - 1) * 75 * (children.size() - 1);
            y += 150;
            for(size_t i=0; i<children.size(); ++i) {
                bool insert = true;
                for(auto node : nodes)
                    insert &= node.first != children[i];
                if(insert)
                    nodes.insert(i, QPair<std::shared_ptr<Node>, int>(children[i], next.second - 1));
            }
        }
    }
}

int VotingStructureReader::getMaxDepth(std::shared_ptr<Node> root, std::shared_ptr<VotingStructure> votingStructure, QList<std::shared_ptr<Node>> &nodesReached) {
    int maxDepth = 1;
    for(auto node : votingStructure->getChildren(root)) {
        bool reached = false;
        for(auto reachedNode : nodesReached)
            reached |= reachedNode == node;
        if(reached)
            continue;
        nodesReached.push_back(node);

        int depth = getMaxDepth(node, votingStructure, nodesReached);
        if(depth >= maxDepth)
            maxDepth = depth + 1;
    }
    return maxDepth;
}

std::map<QString, std::shared_ptr<QuorumList>> VotingStructureReader::readExpectedQuorumLists(QByteArray& quorumListFile, std::vector<QString> nodes, bool &verifySimpleSet) {
    QString text = QTextCodec::codecForMib(106)->toUnicode(quorumListFile);
    return QuorumListReader::readQuorumLists(text, nodes, verifySimpleSet);
}

bool VotingStructureReader::operationsEqual(std::map<QString, std::shared_ptr<QuorumList> > quorumLists, QStringList operations) {
    QStringList qOperations;
    QStringList opOperations(operations);
    for(auto &&op : quorumLists) {
        qOperations.append(op.first);
    }
    std::sort(qOperations.begin(), qOperations.end());
    std::sort(opOperations.begin(), opOperations.end());
    return qOperations == opOperations;
}
