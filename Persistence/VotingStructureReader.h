#ifndef VOTINGSTRUCTUREREADER_H
#define VOTINGSTRUCTUREREADER_H

#include "VotingStructurePersistenceBase.h"

#include <memory>
#include <QPair>

#include "../Models/Topology.h"
#include "../Models/VotingStructure.h"
#include "Unzip.h"

class VotingStructureReader : protected VotingStructurePersistenceBase
{
public:
    /**
     * @brief Reads the voting structure from the given file.
     */
    static QPair<std::shared_ptr<VotingStructure>, std::shared_ptr<Topology>> readFromFile(QString fileName);
    /**
     * @brief Imports a voting structure from the file with the given topology.
     */
    static std::shared_ptr<VotingStructure> importFromFile(QString fileName, std::shared_ptr<Topology> topology);

private:
    /**
     * @brief Gets the file names of the parts of the archieve.
     */
    static bool getFileNames(Unzip& unzip, QString& top, QString& xml, QString& metaXml, QString &expectedQuorumLists);
    /**
     * @brief Reads the topology file of the archieve.
     */
    static std::shared_ptr<Topology> readTopology(Unzip& unzip, QString fileName);
    /**
     * @brief Reads the votings structure file of the archieve.
     */
    static std::shared_ptr<VotingStructure> readXml(QByteArray& xmlFile);
    /**
     * @brief Reads the expected quorum lists file of the archieve.
     */
    static std::map<QString, std::shared_ptr<QuorumList>> readExpectedQuorumLists(QByteArray& quorumListFile, std::vector<QString> nodes, bool &verifySimpleSet);
    /**
     * @brief Validates the read voting structure.
     */
    static bool validateVotingStructure(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology);
    /**
     * @brief Reads the meta xml file of the archieve.
     */
    static bool readMetaXml(Unzip& unzip, QString fileName, std::shared_ptr<VotingStructure> votingStructure);
    /**
     * @brief Arranges an imported voting structure without coordinate information.
     */
    static void arrangeImportedStructure(std::shared_ptr<VotingStructure> votingStructure);
    /**
     * @brief Determines the max depth of the voting structure starting from the root.
     */
    static int getMaxDepth(std::shared_ptr<Node> root, std::shared_ptr<VotingStructure> votingStructure, QList<std::shared_ptr<Node>> &nodesReached);
    /**
     * @brief Determines if the operations of a voting structure and the expected quorum lists are equal.
     */
    static bool operationsEqual(std::map<QString, std::shared_ptr<QuorumList>> quorumLists, QStringList operations);
};

#endif // VOTINGSTRUCTUREREADER_H
