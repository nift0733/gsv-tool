#include "TopologyReader.h"

#include <QFile>
#include <QTextStream>

std::shared_ptr<Topology> TopologyReader::readFromFile(QString fileName) {
    std::shared_ptr<Topology> result = std::make_shared<Topology>(fileName);

    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return result;

    ProcessStep step = Initial;
    QTextStream in(&file);
    while (!in.atEnd()) {
        QString line = in.readLine();
        if(!TopologyReader::processLine(line, result, step))
            return std::make_shared<Topology>("none");
    }
    if(step != ConnectionAvailabilities)
        return std::make_shared<Topology>("none");

    return result;
}

bool TopologyReader::processLine(QString line, std::shared_ptr<Topology> topology, ProcessStep& processStep) {
    if(line.trimmed().startsWith("//"))
        return true;
    switch(processStep) {
    case Initial:
        processStep = processInitial(line);
        break;
    case Variables:
        processStep = processVariable(line, topology);
        break;
    case Nodes:
        processStep = processNode(line, topology);
        break;
    case ConnectionAvailabilities:
        processStep = processConnectionAvailability(line, topology);
        break;
    case Error:
        return false;
    }
    return processStep != Error;
}

TopologyReader::ProcessStep TopologyReader::processInitial(QString line) {
    if(line == "define variables")
        return Variables;
    else if(line == "define nodes")
        return Nodes;
    return Initial;
}

TopologyReader::ProcessStep TopologyReader::processVariable(QString line, std::shared_ptr<Topology> topology) {
    if(line == "define nodes")
        return Nodes;

    QStringList words = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
    if(words.length() == 1) {
        return topology->addVariable(line.trimmed()) ? Variables : Error;
    }
    return words.length() == 0 ? Variables : Error;
}

TopologyReader::ProcessStep TopologyReader::processNode(QString line, std::shared_ptr<Topology> topology) {
    if(line == "define topology")
        return ConnectionAvailabilities;

    QStringList words = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
    if(words.length() == 3) {
        return topology->addNode(words[0], words[1], words[2]) ? Nodes : Error;
    }
    return words.length() == 0 ? Nodes : Error;
}

TopologyReader::ProcessStep TopologyReader::processConnectionAvailability(QString line, std::shared_ptr<Topology> topology) {
    QStringList words = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
    if(words.length() == 0)
        return ConnectionAvailabilities;

    if(words.length() == topology->getNumberOfNodes() + 1) {
        int index = 1;
        foreach (QString node, topology->getNodes()) {
            if(!topology->setConnectionAvailability(words[0], node, words[index++]))
                return Error;
        }
        return ConnectionAvailabilities;
    }
    return words.length() == 0 ? ConnectionAvailabilities : Error;
}
