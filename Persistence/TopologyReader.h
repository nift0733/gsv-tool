#ifndef TOPOLOGYREADER_H
#define TOPOLOGYREADER_H

#include <memory>
#include <QString>

#include "../Models/Topology.h"

class TopologyReader {
public:
    /**
     * @brief Reads a topology from the given file.
     */
    static std::shared_ptr<Topology> readFromFile(QString fileName);

private:
    enum ProcessStep {
        Initial,
        Variables,
        Nodes,
        ConnectionAvailabilities,
        Error
    };
    /**
     * @brief Processes one line regarding the current process step.
     */
    static bool processLine(QString line, std::shared_ptr<Topology> topology, ProcessStep& processStep);
    /**
     * @brief Processes the line when nothing has been read before.
     */
    static ProcessStep processInitial(QString line);
    /**
     * @brief Processes the line to read a variable.
     */
    static ProcessStep processVariable(QString line, std::shared_ptr<Topology> topology);
    /**
     * @brief Processes the line to read a node with availability and cost.
     */
    static ProcessStep processNode(QString line, std::shared_ptr<Topology> topology);
    /**
     * @brief Processes the line to read connection availabilities.
     */
    static ProcessStep processConnectionAvailability(QString line, std::shared_ptr<Topology> topology);
};

#endif // TOPOLOGYREADER_H
