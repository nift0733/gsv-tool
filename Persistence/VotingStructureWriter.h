#ifndef VOTINGSTRUCTUREWRITER_H
#define VOTINGSTRUCTUREWRITER_H

#include "VotingStructurePersistenceBase.h"

#include "../Models/Topology.h"
#include "../Models/VotingStructure.h"

class VotingStructureWriter : protected VotingStructurePersistenceBase
{
public:
    /**
     * @brief Writes the voting structure to the given file.
     */
    static bool writeToFile(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology, QString fileName);
    /**
     * @brief Exports the XML-part of a voting structure to the given file.
     */
    static bool exportToFile(std::shared_ptr<VotingStructure> votingStructure, QString fileName);

private:
    /**
     * @brief Writes the XML-part of a voting structure.
     */
    static bool writeXml(std::shared_ptr<VotingStructure> votingStructure, QByteArray& result);
    /**
     * @brief Writes the meta information part of a voting structure.
     */
    static bool writeMetaXml(std::shared_ptr<VotingStructure> votingStructure, QByteArray& result);
};

#endif // VOTINGSTRUCTUREWRITER_H
