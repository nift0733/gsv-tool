#ifndef QUORUMLISTWRITER_H
#define QUORUMLISTWRITER_H

#include <QString>
#include <map>
#include <memory>

#include "../Models/QuorumList.h"
#include "../Models/VotingStructure.h"
#include "../Models/Topology.h"

class QuorumListWriter
{
public:
    /**
     * @brief Writes the text represenation for the expected quorum lists of the voting structure.
     */
    static QString writeQuorumLists(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology);
    /**
     * @brief Writes the text representation for the quorum list including the given properties.
     */
    static QString writeQuorumList(std::shared_ptr<QuorumList> quorumList, std::vector<QString> nodes, bool binaryDisplay = false, bool lineByLineDisplay = false, bool minimalDisplay = false);

    /**
     * @brief Writes the text represention of one quorum.
     */
    static QString quorumToSetString(uint64_t *quorum, std::vector<QString> nodes);

private:
    /**
     * @brief Returns the binary representation of quorum.
     */
    static QString quorumToBinaryString(uint64_t *quorum, std::vector<QString> nodes);
    /**
     * @brief Returns the explanations string for the binary representation.
     */
    static QString nodesToExplainationString(std::vector<QString> nodes);
    /**
     * @brief Determines if the quorum at bucket,index in quorumList is a super quorum of a quorum in a prior bucket.
     */
    static bool isSuperQuorum(std::shared_ptr<QuorumList> quorumList, int bucket, int index);
};

#endif // QUORUMLISTWRITER_H
