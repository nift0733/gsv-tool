#ifndef VOTINGSTRUCTUREPERSISTENCEBASE_H
#define VOTINGSTRUCTUREPERSISTENCEBASE_H

#include <QByteArray>
#include <QIODevice>

#define XSD_PRIO_INF "inf"
#define XSD_PRIO_FILLUP "fillup"

class VotingStructurePersistenceBase
{
public:
    /**
     * @brief Initializes the Persistence class with the XSD definitions.
     */
    static void Init(QIODevice& votingstructureXsdFile, QIODevice& votingstructureMetaXsdFile);

protected:
    static QByteArray votingstructureXsd;
    static QByteArray votingstructureMetaXsd;
};

#endif // VOTINGSTRUCTUREPERSISTENCEBASE_H
