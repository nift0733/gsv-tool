#include "QuorumListWriter.h"

#include "../Algorithm/QuorumUtil.h"

QString QuorumListWriter::writeQuorumLists(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology) {
    QString result = "";
    for(auto &&pair : votingStructure->getExpectedQuorumMap()) {
        result += pair.first + ":\r\n";
        result += writeQuorumList(pair.second, topology->getNodes());
        result += "\r\n\r\n";
    }
    if(result != "")
        return QString("verifySimpleSet:") + (votingStructure->verifySimpleSet ? "true" : "false") + ";\r\n\r\n" + result;
    return result;
}

QString QuorumListWriter::writeQuorumList(std::shared_ptr<QuorumList> quorumList, std::vector<QString> nodes, bool binaryDisplay, bool lineByLineDisplay, bool minimalDisplay) {
    QString lb = lineByLineDisplay ? "\r\n" : "";
    QString lbt = lineByLineDisplay ? "\r\n\t" : "";
    QString output = "";
    if(binaryDisplay)
        output += nodesToExplainationString(nodes);
    output += "[";
    for(int bucket=0; bucket<quorumList->buckets(); ++bucket) {
        auto first = 0;
        if(minimalDisplay) {
            first = -1;
            for(int index=0; index<quorumList->bucketSize(bucket); ++index) {
                if(!isSuperQuorum(quorumList, bucket, index)) {
                    first = index;
                    break;
                }
            }
            if(first < 0)
                continue;
        }
        output += bucket ? ", "+lbt+"{ " : " "+lbt+"{ ";
        for(int index=first; index<quorumList->bucketSize(bucket); ++index) {
            if(minimalDisplay && isSuperQuorum(quorumList, bucket, index))
                continue;
            output += index != first ? ", " : "";
            uint64_t* quorum = quorumList->at(bucket, index);
            output += binaryDisplay ? quorumToBinaryString(quorum, nodes) : quorumToSetString(quorum, nodes);
        }
        output += " }";
    }
    output = output + lb + "]";
    return output;
}

QString QuorumListWriter::quorumToSetString(uint64_t *quorum, std::vector<QString> nodes) {
    QString output = "{";
    bool first = true;
    for(size_t node=0; node<nodes.size(); ++node) {
        if(*quorum & ((uint64_t)1 << node)) {
            output += first ? "" : ",";
            output += nodes[node];
            first = false;
        }
        if((node + 1) % 64 == 0)
            ++quorum;
    }
    output += "}";
    return output;
}

QString QuorumListWriter::quorumToBinaryString(uint64_t *quorum, std::vector<QString> nodes) {
    QString output = "";
    uint64_t value = *quorum;
    for(size_t j=0; j<nodes.size(); ++j) {
        output = output + QString::number(value % 2);
        value /= 2;
        if((j+1) % 64 == 0)
            value = *(++quorum);
    }
    return output;
}

QString QuorumListWriter::nodesToExplainationString(std::vector<QString> nodes) {
    QString output = "";
    bool first = true;
    for (auto &&node : nodes) {
        if(!first)
            output += "|";
        else
            first = false;
        output += node;
    }
    return output + "\r\n\r\n";
}

bool QuorumListWriter::isSuperQuorum(std::shared_ptr<QuorumList> quorumList, int bucket, int index) {
    uint64_t* quorum = quorumList->at(bucket, index);
    bool superset = false;
    for(int preBucket=0; preBucket<=bucket; ++preBucket) {
        for(int preIndex=0; preIndex<quorumList->bucketSize(preBucket); ++preIndex) {
            if(preBucket == bucket && preIndex == index)
                continue;
            uint64_t* preQuorum = quorumList->at(preBucket, preIndex);
            QUORUM_IS_SUBSET(superset, preQuorum, quorum, quorumList->getQuorumSize());
            if(superset)
                break;
        }
        if(superset)
            break;
    }
    return superset;
}
