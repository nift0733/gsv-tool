#include "Unzip.h"

#include <QBuffer>

#define LOCAL_FILE_HEADER_LENGTH 30
#define UNZIP_FILE_SIZE_MAX 0x10000000000

Unzip::Unzip(QIODevice& inputFile) : inputFile(inputFile) {
    generateCRC32Table();
    processZipFile();
}

Unzip::~Unzip() {
    for(uint16_t i=0; i<files.length(); ++i) {
        if(files[i])
            delete files[i];
    }
}

QStringList Unzip::getFileNames() {
    return fileNames;
}

QByteArray* Unzip::getFile(QString fileName) {
    for(uint16_t i=0; i<fileNames.length(); ++i) {
        if(fileNames[i] == fileName) {
            if(files[i])
                return files[i];
            else
                return readFile(i);
        }
    }
    return nullptr;
}

QByteArray* Unzip::readFile(uint16_t index) {
    if(!inputFile.seek(fileOffsets[index]))
        return nullptr;
    QByteArray* buffer = new QByteArray(fileSizes[index], '\0');
    uint32_t crc32 = 0xFFFFFFFF;
    for(uint32_t i=0; i<fileSizes[index]; ++i) {
        char byte;
        if(inputFile.read(&byte, 1) != 1)
            return nullptr;
        (*buffer)[i] = byte;
        crc32 = crc32Table[(crc32 ^ byte) & 0xFF] ^ (crc32 >> 8);
    }
    crc32 = crc32 ^ 0xFFFFFFFF;
    if(crc32 != this->crc32[index])
        return nullptr;
    files[index] = buffer;
    return files[index];
}

void Unzip::processZipFile() {
    // find end
    qint64 fileSize = inputFile.size();
    if(fileSize < 32 || fileSize > UNZIP_FILE_SIZE_MAX)
        return;
    uint64_t pos = fileSize - 32;
    inputFile.seek(pos);
    while(!search(0x50, 0x4B, 0x05, 0x06, 32)) {
        if(pos < 28)
            return;
        pos -= 28;
        inputFile.seek(pos);
    }
    // process end
    bool ok = true;
    ok &= read4(&ok) == 0;                  // disk nr. + disk nr. central
    uint16_t countOfFiles = read2(&ok);     // disk entries
    ok &= read2(&ok) == countOfFiles;       // total entries
    uint32_t centralSize = read4(&ok);      // central size
    uint32_t centralOffset = read4(&ok);    // central offset
    if(!ok)
        return;

    // find first central header
    inputFile.seek(centralOffset);
    for(uint16_t i=0; i<countOfFiles; ++i) {
        // process central header
        if(!search(0x50, 0x4B, 0x01, 0x02, 4))
            return;
        read2(&ok);                         // version made
        read2(&ok);                         // version need
        read2(&ok);                         // flags
        ok &= read2(&ok) == 0;              // compression method (must be 0 = store)
        read4(&ok);                         // date / time
        uint32_t crc32 = read4(&ok);        // CRC32
        uint32_t fileSize = read4(&ok);     // compressed file size
        ok &= read4(&ok) == fileSize;       // uncompressed file size
        uint16_t fileNameSize = read2(&ok); // file name length
        uint16_t extraSize = read2(&ok);    // extra field length
        read2(&ok);                         // file comment length
        ok &= read2(&ok) == 0;              // disk nr.
        read2(&ok);                         // internal attribute
        read4(&ok);                         // external attribute
        uint32_t localOffset = read4(&ok);  // offset of local file header
        QString fileName = readString(&ok, fileNameSize);
        if(!ok)
            return;
        uint32_t nextCentralHeader = inputFile.pos() + extraSize;

        // find local header
        if(!inputFile.seek(localOffset))
            return;
        // process local header
        if(!search(0x50, 0x4B, 0x03, 0x04, 4))
            return;
        read2(&ok);                         // version
        read2(&ok);                         // flags
        ok &= read2(&ok) == 0;              // compression method (must be 0 = store)
        read4(&ok);                         // date / time
        ok &= read4(&ok) == crc32;          // CRC32
        ok &= read4(&ok) == fileSize;       // compressed file size
        ok &= read4(&ok) == fileSize;       // uncompressed file size
        ok &= read2(&ok) == fileNameSize;   // file name length
        extraSize = read2(&ok);             // extra field length
        ok &= readString(&ok, fileNameSize) == fileName;
        if(!ok)
            return;

        // save information
        fileNames.append(fileName);
        fileOffsets.append(localOffset + LOCAL_FILE_HEADER_LENGTH + fileNameSize + extraSize);
        fileSizes.append(fileSize);
        this->crc32.append(crc32);
        files.append(nullptr);

        // find next central header
        if(nextCentralHeader > centralOffset + centralSize)
            return;
        if(!inputFile.seek(nextCentralHeader))
            return;
    }
}

uint8_t Unzip::read1(bool *ok) {
    if(!*ok)
        return 0;
    char byte;
    *ok = inputFile.read(&byte, 1) == 1;
    return (uint8_t) byte;
}

uint16_t Unzip::read2(bool *ok) {
    uint16_t result = 0;
    result |= read1(ok);
    result |= read1(ok) << 8;
    return result;
}

uint32_t Unzip::read4(bool *ok) {
    uint32_t result = 0;
    result |= read1(ok);
    result |= read1(ok) << 8;
    result |= read1(ok) << 16;
    result |= read1(ok) << 24;
    return result;
}

QString Unzip::readString(bool *ok, uint16_t length) {
    if(!*ok)
        return "";
    auto bytes = inputFile.read(length);
    return QString(bytes);
}

/**
 * @brief Unzip::seek searches for the given byte sequence in the inputFile beginning from current cursor position
 * and searches in the next 'maxSearch' bytes, returns true if the sequence was found and left the cursor on the next
 * position after the last byte of the found sequence.
 */
bool Unzip::search(char byte1, char byte2, char byte3, char byte4, uint64_t maxSearch) {
    char bytes[4] = {byte1, byte2, byte3, byte4};
    int foundIndex = 0;
    for(uint64_t i=0; i<maxSearch; ++i) {
        char byte;
        if(inputFile.read(&byte, 1) != 1)
            return false;
        if(bytes[foundIndex] == byte)
            ++foundIndex;
        else
            foundIndex = 0;
        if(foundIndex >= 4)
            return true;
    }
    return false;
}

void Unzip::generateCRC32Table() {
    // https://gist.github.com/timepp/1f678e200d9e0f2a043a9ec6b3690635
    uint32_t polynomial = 0xEDB88320;
    for (uint32_t i = 0; i < 256; i++)
    {
        uint32_t c = i;
        for (size_t j = 0; j < 8; j++)
        {
            if (c & 1) {
                c = polynomial ^ (c >> 1);
            }
            else {
                c >>= 1;
            }
        }
        crc32Table[i] = c;
    }
}
