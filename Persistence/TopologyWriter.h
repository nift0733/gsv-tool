#ifndef TOPOLOGYWRITER_H
#define TOPOLOGYWRITER_H

#include <QString>

class TopologyWriter
{
public:
    /**
     * @brief Writes a full topology with the given number of physical nodes to the file.
     */
    static bool writeFullTopologyToFile(QString fileName, int physicalNodes);
};

#endif // TOPOLOGYWRITER_H
