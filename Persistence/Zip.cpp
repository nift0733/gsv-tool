#include "Zip.h"

#define LOCAL_CRC32_OFFSET 14
#define LOCAL_FILE_HEADER_LENGTH 30
#define CENTRAL_FILE_HEADER_LENGTH 46

Zip::Zip(QIODevice& outputFile) : out(outputFile), countOfFiles(0), centralSize(0), triedToSave(false) {
    generateCRC32Table();
}

void Zip::addFile(QString fileName, QIODevice &file) {
    fileNames.push_back(fileName);
    files.push_back(&file);
    crc32.push_back(0xFFFFFFFF);
    sizes.push_back(0);
    localOffsets.push_back(0);
    ++countOfFiles;
}

bool Zip::save() {
    if(triedToSave) {
        triedToSave = true;
        return false;
    }
    triedToSave = true;

    for(int i=0; i<countOfFiles; ++i) {
        if(!files[i]->isOpen())
            files[i]->open(QIODevice::ReadOnly);
        else
            return false;
    }

    generateTimestamp();
    for(int i=0; i<countOfFiles; ++i) {
        if(!writeLocal(i))
            return false;
    }

    if(!out.seek(centralOffset))
        return false;

    for(int i=0; i<countOfFiles; ++i) {
        if(!writeCentral(i))
            return false;
    }

    return writeEnd();
}

bool Zip::writeLocal(uint16_t index) {
    if(!out.seek(localOffsets[index]))
        return false;

    bool r = true;
    r &= writeBytes(0x50, 0x4B, 0x03, 0x04);        // LocalHeaderSign
    r &= writeUInt16(10);                           // version
    r &= writeUInt16(0);                            // flags
    r &= writeUInt16(0);                            // compression level (store)
    r &= writeUInt32(timestamp);                    // date / time
    r &= writeUInt32(0);                            // CRC32 (filled later)
    r &= writeUInt32(0);                            // compressed size (filled later)
    r &= writeUInt32(0);                            // uncompressed size (filled later)
    r &= writeUInt16(fileNames[index].length());    // file name size
    r &= writeUInt16(0);                            // extra field length

    r &= writeBytes(fileNames[index].toUtf8());     // file name
    if(!r)
        return false;
    r &= writeContent(index);                       // file content

    r &= out.seek(localOffsets[index] + LOCAL_CRC32_OFFSET);
    r &= writeUInt32(crc32[index]);                 // CRC32
    r &= writeUInt32(sizes[index]);                 // compressed size
    r &= writeUInt32(sizes[index]);                 // uncompressed size
    return r;
}

bool Zip::writeContent(uint16_t index) {
    while(!files[index]->atEnd()) {
        char buffer[32];
        auto bytes = files[index]->read(buffer, 32);
        if(bytes < 0 || bytes > 32)
            return false;
        if(out.write(buffer, bytes) != bytes)
            return false;
        // calculate size and CRC32 on the fly
        sizes[index] += bytes;
        for(uint16_t i=0; i<bytes; ++i)
            crc32[index] = crc32Table[(crc32[index] ^ buffer[i]) & 0xFF] ^ (crc32[index] >> 8);
    }
    crc32[index] = crc32[index] ^ 0xFFFFFFFF;
    uint32_t nextOffset = localOffsets[index] + LOCAL_FILE_HEADER_LENGTH + fileNames[index].length() + sizes[index];
    if(index + 1 < countOfFiles)
        localOffsets[index + 1] = nextOffset;
    else
        centralOffset = nextOffset;
    return true;
}

bool Zip::writeCentral(uint16_t index) {
    bool r = true;
    r &= writeBytes(0x50, 0x4B, 0x01, 0x02);        // CentralHeaderSign
    r &= writeUInt16(10);                           // version made
    r &= writeUInt16(10);                           // version need
    r &= writeUInt16(0);                            // flags
    r &= writeUInt16(0);                            // compression level (store)
    r &= writeUInt32(timestamp);                    // date / time
    r &= writeUInt32(crc32[index]);                 // CRC32
    r &= writeUInt32(sizes[index]);                 // compressed size
    r &= writeUInt32(sizes[index]);                 // uncompressed size
    r &= writeUInt16(fileNames[index].length());    // file name size
    r &= writeUInt16(0);                            // extra field length
    r &= writeUInt16(0);                            // file comment length
    r &= writeUInt16(0);                            // disk nr.
    r &= writeUInt16(0);                            // internal attribute
    r &= writeBytes(0x00, 0x00, 0xA4, 0x81);        // external attribute
    r &= writeUInt32(localOffsets[index]);          // offset of local file header

    r &= writeBytes(fileNames[index].toUtf8());     // file name

    centralSize += CENTRAL_FILE_HEADER_LENGTH + fileNames[index].length();
    return r;
}

bool Zip::writeEnd() {
    bool r = true;
    r &= writeBytes(0x50, 0x4B, 0x05, 0x06);        // CentralEndSign
    r &= writeUInt16(0);                            // disk nr.
    r &= writeUInt16(0);                            // disk nr. central
    r &= writeUInt16(countOfFiles);                 // disk entries
    r &= writeUInt16(countOfFiles);                 // total entries
    r &= writeUInt32(centralSize);                  // central size
    r &= writeUInt32(centralOffset);                // central offset
    r &= writeUInt16(0);                            // archive comment length

    return r;
}

bool Zip::writeBytes(QByteArray bytes) {
    return out.write(bytes) == bytes.length();
}

bool Zip::writeBytes(uint8_t byte1, uint8_t byte2) {
    QByteArray bytes(2, '\0');
    bytes[0] = byte1;
    bytes[1] = byte2;
    return out.write(bytes) == 2;
}

bool Zip::writeBytes(uint8_t byte1, uint8_t byte2, uint8_t byte3, uint8_t byte4) {
    QByteArray bytes(4, '\0');
    bytes[0] = byte1;
    bytes[1] = byte2;
    bytes[2] = byte3;
    bytes[3] = byte4;
    return out.write(bytes) == 4;
}

bool Zip::writeUInt16(uint16_t value) {
    return writeBytes(value & 255, (value >> 8) & 255);
}

bool Zip::writeUInt32(uint32_t value) {
    return writeBytes(value & 255, (value >> 8) & 255, (value >> 16) & 255, (value >> 24) & 255);
}

uint32_t Zip::toFatTimestamp(QDateTime datetime) {
    uint16_t y = datetime.date().year();
    uint8_t M = datetime.date().month();
    uint8_t d = datetime.date().day();
    uint8_t h = datetime.time().hour();
    uint8_t m = datetime.time().minute();
    uint8_t s = datetime.time().second();

    uint32_t timestamp = 0;
    timestamp |= s / 2;
    timestamp |= m << 5;
    timestamp |= h << 11;
    timestamp |= d << 16;
    timestamp |= M << 21;
    timestamp |= (y - 1980) << 25;
    return timestamp;
}

void Zip::generateTimestamp() {
    timestamp = toFatTimestamp(QDateTime::currentDateTime());
}

void Zip::generateCRC32Table() {
    // https://gist.github.com/timepp/1f678e200d9e0f2a043a9ec6b3690635
    uint32_t polynomial = 0xEDB88320;
    for (uint32_t i = 0; i < 256; i++)
    {
        uint32_t c = i;
        for (size_t j = 0; j < 8; j++)
        {
            if (c & 1) {
                c = polynomial ^ (c >> 1);
            }
            else {
                c >>= 1;
            }
        }
        crc32Table[i] = c;
    }
}
