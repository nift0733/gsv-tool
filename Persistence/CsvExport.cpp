#include "CsvExport.h"

#include <QFile>
#include <QTextStream>

bool CsvExport::exportValues(QString fileName, std::vector<QString> headers, std::vector<std::vector<double>> values) {
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    try {
        QTextStream out(&file);

        bool first = true;
        for(auto &&header : headers) {
            out << (first ? "" : ";") << header;
            first = false;
        }
        out << "\r\n";
        for(auto &&row : values) {
            first = true;
            for(auto &&value : row) {
                out << (first ? "" : ";") << value;
                first = false;
            }
            out << "\r\n";
        }
    } catch (std::exception &) {
        file.close();
        return false;
    }
    file.close();
    return true;
}
