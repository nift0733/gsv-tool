#ifndef QUORUMLISTREADER_H
#define QUORUMLISTREADER_H

#include <QString>
#include <map>
#include <memory>

#include "../Models/QuorumList.h"

class QuorumListReader
{
public:
    /**
     * @brief Reads the quorum lists given by text.
     */
    static std::map<QString, std::shared_ptr<QuorumList>> readQuorumLists(QString text, std::vector<QString> nodes, bool &verifySimpleSet);
    /**
     * @brief Reads the quorum list given by text.
     */
    static std::shared_ptr<QuorumList> readQuorumList(QString text, std::vector<QString> nodes);

private:
    /**
     * @brief Reads the operation to be used from text.
     */
    static QString readOperation(QString text, int &index);
    /**
     * @brief Reads the quorum list part from text.
     */
    static std::shared_ptr<QuorumList> readQuorumList(QString text, int &index, std::vector<QString> nodes);
    /**
     * @brief Splits text into the sets within text.
     */
    static QStringList splitSets(QString text);
    /**
     * @brief Fills quorum with the read elements.
     */
    static bool fillQuorum(uint64_t *quorum, QStringList elements, std::vector<QString> nodes);
};

#endif // QUORUMLISTREADER_H
