#include "QuorumListReader.h"

#include <QStringList>

std::map<QString, std::shared_ptr<QuorumList>> QuorumListReader::readQuorumLists(QString text, std::vector<QString> nodes, bool &verifySimpleSet) {
    text = text.simplified().replace(" ", "");
    int index = 0;
    std::map<QString, std::shared_ptr<QuorumList>> noResult;
    std::map<QString, std::shared_ptr<QuorumList>> result;
    while(index < text.length() && text.right(text.length() - index) != "") {
        auto op = readOperation(text, index);
        if(op == nullptr || op == "")
            return noResult;
        if(op == "verifySimpleSet") {
            auto until = text.indexOf(';', index);
            verifySimpleSet = text.left(until).right(until-index) == "true";
            index = until + 1;
            continue;
        }
        auto quorumList = readQuorumList(text, index, nodes);
        if(quorumList == nullptr)
            return noResult;
        result[op] = quorumList;
    }
    return result;
}

QString QuorumListReader::readOperation(QString text, int &index) {
    auto until = text.indexOf(':', index);
    if(until < 0)
        return "";
    auto result = text.left(until).right(until-index);
    index = until + 1;
    return result;
}

std::shared_ptr<QuorumList> QuorumListReader::readQuorumList(QString text, int &index, std::vector<QString> nodes) {
    text = text.simplified().replace(" ", "");
    auto first = text.indexOf('[', index);
    if(first < 0 || text.left(first).right(first-index) != "")
        return nullptr;
    auto second = text.indexOf(']', first);
    if(second < 0)
        return nullptr;
    index = second + 1;
    return readQuorumList(text.left(second + 1).right(second-first + 1), nodes);
}

std::shared_ptr<QuorumList> QuorumListReader::readQuorumList(QString text, std::vector<QString> nodes) {
    text = text.simplified().replace(" ", "");
    if(text[0] != '[' || text[text.size()-1] != ']')
        return nullptr;
    text = text.right(text.size()-1).left(text.size()-2);
    auto setsOfSets = splitSets(text);
    auto result = std::make_shared<QuorumList>(nodes.size());
    auto penalty = new uint32_t[2] {};
    penalty[0] = 1;
    penalty[1] = 1;
    for(auto &&setOfSets : setsOfSets) {
        auto sets = splitSets(setOfSets);
        for(auto &&set : sets) {
            auto quorum = new uint64_t[(nodes.size() + QUORUM_BITS - 1) / QUORUM_BITS] {};
            auto elements = set.split(',');
            if(!fillQuorum(quorum, elements, nodes)) {
                delete[] quorum;
                return nullptr;
            }
            result->addQuorum(quorum, penalty);
            delete[] quorum;
        }
        // increment penalty (just for correct order)
        ++penalty[1];
    }
    delete[] penalty;
    return result;
}

QStringList QuorumListReader::splitSets(QString text) {
    QStringList result;
    int lastIndex = 0;
    int bracket = 0;
    for(int i=0; i<text.size(); ++i) {
        if(text[i] == '{')
            ++bracket;
        if(text[i] == '}')
            --bracket;
        if((text[i] == ',') && bracket == 0) {
            result.append(text.left(i - 1).right(i - lastIndex - 2));
            lastIndex = text.indexOf('{', i);
        }
    }
    result.append(text.left(text.size() - 1).right(text.size() - lastIndex - 2));
    return result;
}

bool QuorumListReader::fillQuorum(uint64_t *quorum, QStringList elements, std::vector<QString> nodes) {
    for(auto &&elem : elements) {
        auto index = (size_t) (std::find(nodes.begin(), nodes.end(), elem) - nodes.begin());
        if(index >= nodes.size())
            return false;
        auto arrIndex = index / QUORUM_BITS;
        auto indexIndex = index % QUORUM_BITS;
        quorum[arrIndex] |= (uint64_t)1 << indexIndex;
    }
    return true;
}
