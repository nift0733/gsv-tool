#include "VotingStructurePersistenceBase.h"

QByteArray VotingStructurePersistenceBase::votingstructureXsd;
QByteArray VotingStructurePersistenceBase::votingstructureMetaXsd;

void VotingStructurePersistenceBase::Init(QIODevice &votingstructureXsdFile, QIODevice &votingstructureMetaXsdFile) {
    if(votingstructureXsdFile.isOpen() || votingstructureXsdFile.open(QIODevice::ReadOnly))
        votingstructureXsd = votingstructureXsdFile.readAll();
    if(votingstructureMetaXsdFile.isOpen() || votingstructureMetaXsdFile.open(QIODevice::ReadOnly))
        votingstructureMetaXsd = votingstructureMetaXsdFile.readAll();
}
