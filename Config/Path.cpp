#include "Path.h"

#include <QDir>

#define CONFIG_PATH (QDir::homePath() + "/.config/gsv-tool/path.conf")
#define CONFIG_DEFAULT_DIR QDir::homePath()
#define CONFIG_KEY_VALUE_SEPARATOR ":"

#define CONFIG_READ_KEY_VALUE(KEY,MEMBER) if(parts[0] == KEY) \
    MEMBER = parts[1]
#define CONFIG_WRITE_KEY_VALUE(KEY,MEMBER) conf.write((QString(KEY) + CONFIG_KEY_VALUE_SEPARATOR + MEMBER).toUtf8() + "\r\n")

QString Path::topologyDefault;
QString Path::topologyLast;
QString Path::structureDefault;
QString Path::structureLast;
QString Path::structureXmlDefault;
QString Path::structureXmlLast;
QString Path::exportDefault;
QString Path::exportLast;

QString Path::getTopologyDefault() {
    if(topologyDefault != "")
        return topologyDefault;
    readConfig();
    return topologyDefault;
}

QString Path::getStructureDefault() {
    if(structureDefault != "")
        return structureDefault;
    readConfig();
    return structureDefault;
}

QString Path::getStructureXmlDefault() {
    if(structureXmlDefault != "")
        return structureXmlDefault;
    readConfig();
    return structureXmlDefault;
}

QString Path::getExportDefault() {
    if(exportDefault != "")
        return exportDefault;
    readConfig();
    return exportDefault;
}

QString Path::getCacheFolder() {
    auto dir = QDir(QDir::homePath() + "/.cache/gsv-tool");
    if(!dir.exists())
        dir.mkpath(dir.path());
    return dir.path();
}

QString Path::getCachePath(QString fileName) {
    return getCacheFolder() + QDir::separator() + fileName;
}

void Path::usedTopologyFile(QString filePath) {
    auto path = getFilePathDir(filePath);
    if(topologyLast == path || topologyLast == "")
        topologyDefault = path;
    topologyLast = path;
    writeConfig();
}

void Path::usedStructureFile(QString filePath) {
    auto path = getFilePathDir(filePath);
    if(structureLast == path || structureLast == "")
        structureDefault = path;
    structureLast = path;
    writeConfig();
}

void Path::usedStructureXmlFile(QString filePath) {
    auto path = getFilePathDir(filePath);
    if(structureXmlLast == path || structureXmlLast == "")
        structureXmlDefault = path;
    structureXmlLast = path;
    writeConfig();
}

void Path::usedExportFile(QString filePath) {
    auto path = getFilePathDir(filePath);
    if(exportLast == path || exportLast == "")
        exportDefault = path;
    exportLast = path;
    writeConfig();
}

QString Path::getFileName(QString filePath) {
    QStringList parts = filePath.split(QDir::separator());
    QStringList nameParts = parts.last().split(".");
    return nameParts.first();
}

QString Path::getFilePathDir(QString filePath) {
    QStringList parts = filePath.split(QDir::separator());
    parts[parts.length() - 1] = "";
    return parts.join(QDir::separator());
}

void Path::writeConfig() {
    QDir confDir = getFilePathDir(CONFIG_PATH);
    if(!confDir.exists() && !confDir.mkpath(confDir.path()))
        return;
    QFile conf(CONFIG_PATH);
    if(!conf.exists()) {
        topologyDefault = topologyLast;
        structureDefault = structureLast;
        structureXmlDefault = structureXmlLast;
        exportDefault = exportLast;
    }
    if(!conf.open(QIODevice::WriteOnly | QIODevice::Text))
        return;
    CONFIG_WRITE_KEY_VALUE("top_default", topologyDefault);
    CONFIG_WRITE_KEY_VALUE("top_last", topologyLast);
    CONFIG_WRITE_KEY_VALUE("gsv_default", structureDefault);
    CONFIG_WRITE_KEY_VALUE("gsv_last", structureLast);
    CONFIG_WRITE_KEY_VALUE("xml_default", structureXmlDefault);
    CONFIG_WRITE_KEY_VALUE("xml_last", structureXmlLast);
    CONFIG_WRITE_KEY_VALUE("export_default", exportDefault);
    CONFIG_WRITE_KEY_VALUE("export_last", exportLast);
}

void Path::readConfig() {
    QFile conf(CONFIG_PATH);
    if(!conf.exists() || !conf.open(QIODevice::ReadOnly | QIODevice::Text)) {
        topologyDefault = CONFIG_DEFAULT_DIR;
        topologyLast = CONFIG_DEFAULT_DIR;
        structureDefault = CONFIG_DEFAULT_DIR;
        structureLast = CONFIG_DEFAULT_DIR;
        structureXmlDefault = CONFIG_DEFAULT_DIR;
        structureXmlLast = CONFIG_DEFAULT_DIR;
        exportDefault = CONFIG_DEFAULT_DIR;
        exportLast = CONFIG_DEFAULT_DIR;
        return;
    }
    while(!conf.atEnd()) {
        auto line = QString(conf.readLine());
        line = line.trimmed();
        auto parts = line.split(CONFIG_KEY_VALUE_SEPARATOR);
        if(parts.length() != 2)
            continue;
        CONFIG_READ_KEY_VALUE("top_default", topologyDefault);
        CONFIG_READ_KEY_VALUE("top_last", topologyLast);
        CONFIG_READ_KEY_VALUE("gsv_default", structureDefault);
        CONFIG_READ_KEY_VALUE("gsv_last", structureLast);
        CONFIG_READ_KEY_VALUE("xml_default", structureXmlDefault);
        CONFIG_READ_KEY_VALUE("xml_last", structureXmlLast);
        CONFIG_READ_KEY_VALUE("export_default", exportDefault);
        CONFIG_READ_KEY_VALUE("export_last", exportLast);
    }
}
