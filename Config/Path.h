#ifndef PATH_H
#define PATH_H

#include <QString>

class Path
{
public:
    /**
     * @brief Returns the current default path for topology files stored in the config.
     */
    static QString getTopologyDefault();
    /**
     * @brief Returns the current default path for voting structure files stored in the config.
     */
    static QString getStructureDefault();
    /**
     * @brief Returns the current default path for the import/export of voting structures stored in the config.
     */
    static QString getStructureXmlDefault();
    /**
     * @brief Returns the current default path for the export of availabilities and costs stored in the config.
     */
    static QString getExportDefault();
    /**
     * @brief Returns path to the cached file with name fileName.
     */
    static QString getCachePath(QString fileName);

    /**
     * @brief Note the usage of file at filePath as a topology file.
     */
    static void usedTopologyFile(QString filePath);
    /**
     * @brief Note the usage of file at filePath as a voting structure file.
     */
    static void usedStructureFile(QString filePath);
    /**
     * @brief Note the usage of file at filePath as a xml file for import/export of voting structures.
     */
    static void usedStructureXmlFile(QString filePath);
    /**
     * @brief Note the usage of file at filePath as a csv file for export of availabilities or costs.
     */
    static void usedExportFile(QString filePath);

    /**
     * @brief Helper to get the fileName part of filePath.
     */
    static QString getFileName(QString filePath);
    /**
     * @brief Helper to get the directory part of filePath.
     */
    static QString getFilePathDir(QString filePath);

private:
    static QString topologyDefault;
    static QString topologyLast;
    static QString structureDefault;
    static QString structureLast;
    static QString structureXmlDefault;
    static QString structureXmlLast;
    static QString exportDefault;
    static QString exportLast;

    /**
     * @brief Reads the config file and stores the values in the corresponding fields.
     */
    static void readConfig();
    /**
     * @brief Writes the config file with the stored values in the corresponding fields.
     */
    static void writeConfig();
    /**
     * @brief Returns the folder path for the cache.
     */
    static QString getCacheFolder();
};

#endif // PATH_H
