QT       += core gui xml xmlpatterns charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

LIBS += -lstdc++

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Algorithm/AvailabilityCostCalculator.cpp \
    Algorithm/QuorumListCalculator.cpp \
    Algorithm/QuorumListComparator.cpp \
    Algorithm/QuorumUtil.cpp \
    Config/Path.cpp \
    Generators/TlpGenerator.cpp \
    Models/MainModel.cpp \
    Models/Edge.cpp \
    Models/Node.cpp \
    Models/QuorumList.cpp \
    Models/Topology.cpp \
    Models/VotingStructure.cpp \
    Persistence/CsvExport.cpp \
    Persistence/QuorumListReader.cpp \
    Persistence/QuorumListWriter.cpp \
    Persistence/TopologyReader.cpp \
    Persistence/TopologyWriter.cpp \
    Persistence/Unzip.cpp \
    Persistence/VotingStructurePersistenceBase.cpp \
    Persistence/VotingStructureReader.cpp \
    Persistence/VotingStructureWriter.cpp \
    Persistence/Zip.cpp \
    Validation/SRValidator.cpp \
    Validation/SyntaxValidator.cpp \
    Views/AboutDialog.cpp \
    Views/AnalysisDialog.cpp \
    Views/MainWindow.cpp \
    Views/EdgePropertiesView.cpp \
    Views/NewVotingStructureDialog.cpp \
    Views/PhysicalNodePropertiesView.cpp \
    Views/QuorumListDialog.cpp \
    Views/VerificationDialog.cpp \
    Views/VirtualNodePropertiesView.cpp \
    Widgets/GraphRenderer.cpp \
    Widgets/PenaltyResetButton.cpp \
    main.cpp

HEADERS += \
    Algorithm/AvailabilityCostCalculator.h \
    Algorithm/QuorumListCalculator.h \
    Algorithm/QuorumListComparator.h \
    Algorithm/QuorumUtil.h \
    Config/Path.h \
    Generators/TlpGenerator.h \
    Models/MainModel.h \
    Models/Edge.h \
    Models/Node.h \
    Models/QuorumList.h \
    Models/Topology.h \
    Models/VotingStructure.h \
    Persistence/CsvExport.h \
    Persistence/QuorumListReader.h \
    Persistence/QuorumListWriter.h \
    Persistence/TopologyReader.h \
    Persistence/TopologyWriter.h \
    Persistence/Unzip.h \
    Persistence/VotingStructurePersistenceBase.h \
    Persistence/VotingStructureReader.h \
    Persistence/VotingStructureWriter.h \
    Persistence/Zip.h \
    Validation/SRValidator.h \
    Validation/SyntaxValidator.h \
    Views/AboutDialog.h \
    Views/AnalysisDialog.h \
    Views/MainWindow.h \
    Views/EdgePropertiesView.h \
    Views/NewVotingStructureDialog.h \
    Views/PhysicalNodePropertiesView.h \
    Views/QuorumListDialog.h \
    Views/VerificationDialog.h \
    Views/VirtualNodePropertiesView.h \
    Widgets/GraphRenderer.h \
    Widgets/PenaltyResetButton.h

FORMS += \
    Views/AboutDialog.ui \
    Views/AnalysisDialog.ui \
    Views/MainWindow.ui \
    Views/EdgePropertiesView.ui \
    Views/NewVotingStructureDialog.ui \
    Views/PhysicalNodePropertiesView.ui \
    Views/QuorumListDialog.ui \
    Views/VerificationDialog.ui \
    Views/VirtualNodePropertiesView.ui

TRANSLATIONS += \
    gsv-tool_en_US.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    .gitlab-ci.yml \
    res/app.png \
    res/toolOptionDelete.png \
    res/toolOptionEdge.png \
    res/toolOptionMove.png \
    res/toolOptionPhysicalNode.png \
    res/toolOptionVirtualNode.png \
    res/votingstructure.xsd \ \
    res/votingstructure_meta.xsd

RESOURCES += \
    resources.qrc
