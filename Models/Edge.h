#ifndef EDGE_H
#define EDGE_H

#include <memory>

#include "Node.h"

#define MAX_PENALTY 0xFFFF
#define PRIO_INF "∞"
#define PRIO_FILLUP "ε"

class Edge {
public:
    Edge(int numberOfOperations);

    std::shared_ptr<Node> from;
    std::shared_ptr<Node> to;
    std::vector<int> penaltyList;
};

#endif // EDGE_H
