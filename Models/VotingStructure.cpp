#include "VotingStructure.h"

#include <algorithm>
#include <QVector>

#include "../Config/Path.h"

VotingStructure::VotingStructure(std::vector<QString> operations) : verifySimpleSet(false), filePath(""), operations(), expectedQuorumLists(), root(nullptr), nodes(), edges() {
    for(QString op : operations)
        this->operations.push_back(op);
}

QString VotingStructure::getFilePath() {
    return filePath;
}

QString VotingStructure::getFileName() {
    return Path::getFileName(filePath);
}

void VotingStructure::setFilePath(QString filePath) {
    this->filePath = filePath;
}

int VotingStructure::getNumberOfOperations() {
    return operations.size();
}

QStringList VotingStructure::getOperations() {
    QStringList result;
    for(QString str : operations)
        result.push_back(str);
    return result;
}

std::shared_ptr<Node> VotingStructure::getRoot() {
    return root;
}

void VotingStructure::setRoot(std::shared_ptr<Node> node) {
    if(node)
        root = node;
}

std::vector<std::shared_ptr<Node>> VotingStructure::getNodes() {
    return nodes;
}

std::shared_ptr<Node> VotingStructure::addNode(bool isVirtual, int x, int y) {
    int nextId = 1;
    for(size_t i=0; i<nodes.size(); ++i)
        if(nodes[i]->getId() >= nextId)
            nextId = nodes[i]->getId() + 1;

    std::shared_ptr<Node> node = std::make_shared<Node>(nextId, getNumberOfOperations());
    node->isVirtual = isVirtual;
    node->x = x;
    node->y = y;
    nodes.push_back(node);
    return node;
}

bool VotingStructure::addNode(std::shared_ptr<Node> node) {
    for(auto existing : nodes) {
        if(existing->getId() == node->getId())
            return false;
    }
    nodes.push_back(node);
    return true;
}

bool VotingStructure::removeNode(std::shared_ptr<Node> node) {
    auto it = edges.begin();
    while(it < edges.end()) {
        if((*it)->from == node || (*it)->to == node)
            it = edges.erase(it);
        else
            ++it;
    }
    for(auto it2=nodes.begin(); it2<nodes.end(); ++it2) {
        if(*it2 == node) {
            if(root == node)
                root = nullptr;
            nodes.erase(it2);
            return true;
        }
    }
    return false;
}

std::vector<std::shared_ptr<Edge>> VotingStructure::getEdges() {
    return edges;
}

std::shared_ptr<Edge> VotingStructure::addEdge(std::shared_ptr<Node> from, std::shared_ptr<Node> to) {
    if(from == to)
        return nullptr;
    for(auto edge : edges) {
        if((edge->from == from && edge->to == to))
            return nullptr;
    }

    std::shared_ptr<Edge> edge = std::make_shared<Edge>(getNumberOfOperations());
    edge->from = from;
    edge->to = to;
    edges.push_back(edge);
    return edge;
}

bool VotingStructure::removeEdge(std::shared_ptr<Edge> edge) {
    for(auto it=edges.begin(); it<edges.end(); ++it) {
        if(*it == edge) {
            edges.erase(it);
            return true;
        }
    }
    return false;
}

void VotingStructure::translate(int dx, int dy) {
    for(auto node : nodes) {
        node->x += dx;
        node->y += dy;
    }
}

VotingStructure::ValidationResult VotingStructure::isFullyDefined() {
    if(root == nullptr)
        return ValidationResult::NoRoot;
    for(auto node : nodes) {
        if(node->identifier == "")
            return ValidationResult::MissingNameForNode;
    }
    return ValidationResult::FullyDefined;
}

bool VotingStructure::isPartialLeaf(std::shared_ptr<Node> node, std::vector<std::shared_ptr<Node>> &path) {
    auto children = getChildren(node);
    if(children.size() == 0)
        return true;
    for(auto &&pathNode : path) {
        auto it = std::find(children.begin(), children.end(), pathNode);
        if(it != children.end())
            children.erase(it);
    }
    return children.size() == 0;
}

std::vector<std::shared_ptr<Node>> VotingStructure::getChildren(std::shared_ptr<Node> node) {
    std::vector<std::shared_ptr<Node>> result;
    for(auto edge : edges) {
        if(edge->from == node)
            result.push_back(edge->to);
    }
    return result;
}

std::vector<std::shared_ptr<Edge>> VotingStructure::getEdgesOrderedByPenalty(std::shared_ptr<Node> node, int opIndex) {
    std::vector<std::shared_ptr<Edge>> result;
    for(auto edge : edges) {
        if(edge->from == node) {
            bool last = true;
            for(auto it=result.begin(); it<result.end(); ++it) {
                if(edge->penaltyList[opIndex] < (*it)->penaltyList[opIndex] ||
                  (edge->penaltyList[opIndex] == (*it)->penaltyList[opIndex] && !edge->to->isVirtual && (*it)->to->isVirtual)) {
                    result.insert(it, edge);
                    last = false;
                    break;
                }
            }
            if(last)
                result.push_back(edge);
        }
    }
    return result;
}

void VotingStructure::setExpectedQuorumLists(std::map<QString, std::shared_ptr<QuorumList> > quorumLists) {
    expectedQuorumLists = quorumLists;
}

std::vector<std::shared_ptr<QuorumList>> VotingStructure::getExpectedQuorumLists() {
    std::vector<std::shared_ptr<QuorumList>> result;
    for(auto &&op : operations) {
        auto it = expectedQuorumLists.find(op);
        if(it != expectedQuorumLists.end())
            result.push_back(it->second);
    }
    return result;
}

std::map<QString, std::shared_ptr<QuorumList>> VotingStructure::getExpectedQuorumMap() {
    return expectedQuorumLists;
}
