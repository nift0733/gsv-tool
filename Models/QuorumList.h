#ifndef QUORUMLIST_H
#define QUORUMLIST_H

#include <inttypes.h>
#include <vector>

#define QUORUM_BITS 64

class QuorumList
{
public:
    QuorumList(int physicalNodes);
    ~QuorumList();

    /**
     * @brief Adds the quorum with the penalty to the list.
     */
    void addQuorum(uint64_t* quorum, uint32_t* penalty);
    /**
     * @brief Changes the element at bucket, index to the given quorum.
     */
    void setQuorum(int bucket, int index, uint64_t* quorum);
    /**
     * @brief Changes the penalty of the quorum at bucket, index to the specified one.
     */
    void setPenalty(int bucket, int index, uint32_t* penalty);

    /**
     * @brief Returns the quorum at bucket, index.
     */
    uint64_t* at(int bucket, int index);
    /**
     * @brief Returns the penalty of bucket.
     */
    uint32_t* getPenalty(int bucket);
    /**
     * @brief Returns the number of buckets.
     */
    int buckets();
    /**
     * @brief Returns the count of elements in bucket.
     */
    int bucketSize(int bucket);
    /**
     * @brief Returns the quorumSize of the quorums in the list used for the representation in a uint64_t*.
     */
    int getQuorumSize();

    /**
     * @brief Compares two penalties and returns a negative value if the first is smaller and a positive value if the second is smaller.
     */
    static int comparePenalties(uint32_t* penalty1, uint32_t* penalty2);

private:
    const int quorumSize;
    std::vector<int> nextIndex;
    std::vector<uint32_t*> penalties;
    std::vector<std::vector<uint64_t*>> values;
};

#endif // QUORUMLIST_H
