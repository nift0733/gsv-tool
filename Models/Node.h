#ifndef NODE_H
#define NODE_H

#include <vector>
#include <QString>

class Node {
public:
    Node(int id, int numberOfOperations);

    /**
     * @brief Gets the (internal) id of the node.
     */
    int getId();
    bool isVirtual = false;
    QString identifier = "";
    int vote = 0;
    std::vector<int> quorumVector;
    int x = 0;
    int y = 0;

private:
    int id;
};

#endif // NODE_H
