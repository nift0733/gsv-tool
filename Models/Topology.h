#ifndef TOPOLOGY_H
#define TOPOLOGY_H

#include <memory>
#include <vector>
#include <QString>

class Topology {
public:
    Topology(QString filePath);

    /**
     * @brief Returns the fileName of the topology.
     */
    QString getFileName();
    /**
     * @brief Returns the full filePath of the topology with directory and name.
     */
    QString getFilePath();
    /**
     * @brief Adds the variable identifier to the topology.
     */
    bool addVariable(QString identifier);
    /**
     * @brief Sets the value of variable identifier to value.
     */
    bool setVariable(QString identifier, double value);
    /**
     * @brief Adds a node to the topology named name with the availability and cost specified.
     * If availability (cost) is a number, the number is used. If it is a identifier it must match a variable name.
     */
    bool addNode(QString name, QString availability, QString cost);
    /**
     * @brief Sets the availability of the connection between the given two nodes from the first to the second.
     * If availability is a number, the number is used. If it is a identifier it must match a variable name.
     */
    bool setConnectionAvailability(QString node1, QString node2, QString availability);

    /**
     * @brief Returns wether the topology is valid.
     */
    bool isValid();
    /**
     * @brief Returns the number of nodes.
     */
    int getNumberOfNodes();
    /**
     * @brief Returns the names of all nodes.
     */
    std::vector<QString> getNodes();
    /**
     * @brief Gets the availability of the node named node.
     * If the availability is given by a variable, the current value is returned.
     */
    double getAvailability(QString node);
    /**
     * @brief Gets the cost of the node named node.
     * If the cost is given by a variable, the current value is returned.
     */
    double getCost(QString node);
    /**
     * @brief Gets the availability of the connection from the node named node1 to node2.
     * If the availability is given by a variable, the current value is returned.
     */
    double getAvailability(QString node1, QString node2);
    /**
     * @brief Gets the index of the node named name within the vector of nodes.
     */
    size_t getNodeIndex(QString name);

private:
    QString filePath;
    int numberOfNodes;
    std::vector<std::pair<QString, double>> variables;
    std::vector<QString> nodes;
    std::vector<std::pair<double, QString>> availabilities;
    std::vector<std::pair<double, QString>> costs;
    std::vector<std::vector<std::pair<double, QString>>> connectionAvailabilites;

    /**
     * @brief Reads the value.
     * If value is a number, the number is used. If it is a identifier it must match a variable name.
     */
    std::pair<double, QString> readValue(QString value);
    /**
     * @brief Gets the value.
     * If value is a number, the number is used. If it is a identifier it must match a variable name.
     */
    double getValue(std::pair<double, QString> value);
    /**
     * @brief Check the name of a node or variable to be valid.
     */
    bool checkNaming(QString name);
};

#endif // TOPOLOGY_H
