#include "MainModel.h"

MainModel::MainModel()
    : topology(nullptr),
      votingStructure(nullptr),
      selectedNode(nullptr),
      selectedEdge(nullptr),
      votingStructureChanged(false) {
    //
}

bool MainModel::isTopologyLoaded() {
    return topology != nullptr && topology->isValid();
}

bool MainModel::isVotingStructureLoaded() {
    return votingStructure != nullptr;
}

bool MainModel::isVotingStructureChanged() {
    return votingStructureChanged;
}

void MainModel::setVotingStructureChanged(bool value) {
    if(value == votingStructureChanged)
        return;
    votingStructureChanged = value;
    emit viewChanged();
}

std::shared_ptr<Node> MainModel::getSelectedNode() {
    return selectedNode;
}

std::shared_ptr<Edge> MainModel::getSelectedEdge() {
    return selectedEdge;
}

void MainModel::selectNode(std::shared_ptr<Node> node) {
    if(!node)
        return;
    auto nodes = votingStructure->getNodes();
    if(!std::any_of(nodes.begin(), nodes.end(), [&node] (std::shared_ptr<Node> n) { return n == node; }))
        throw std::invalid_argument("node");
    selectedEdge.reset();
    selectedNode = node;
    emit viewChanged();
}

void MainModel::selectEdge(std::shared_ptr<Edge> edge) {
    if(!edge)
        return;
    auto edges = votingStructure->getEdges();
    if(!std::any_of(edges.begin(), edges.end(), [&edge] (std::shared_ptr<Edge> e) { return e == edge; }))
        throw std::invalid_argument("edge");
    selectedNode.reset();
    selectedEdge = edge;
    emit viewChanged();
}

void MainModel::unselectAll() {
    selectedNode.reset();
    selectedEdge.reset();
    emit viewChanged();
}
