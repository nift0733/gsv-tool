#include "QuorumList.h"

#include <cstring>

#define QUORUM_LIST_SINGLE_LENGTH 1024
// TODO: check if dynamic allocation can be optimized (e.g. use fibonacci)

QuorumList::QuorumList(int physicalNodes) : quorumSize((physicalNodes + (QUORUM_BITS - 1)) / QUORUM_BITS), nextIndex(), penalties(), values() {
    //
}

QuorumList::~QuorumList() {
    for(size_t i=0; i<values.size(); ++i)
        for(size_t j=0; j<values[i].size(); ++j)
            delete [] values[i][j];
    for(size_t i=0; i<penalties.size(); ++i)
        delete [] penalties[i];
}

void QuorumList::addQuorum(uint64_t *quorum, uint32_t* penalty) {
    int bucket = 0;
    for(auto it=penalties.begin(); it<penalties.end(); ++it) {
        if(comparePenalties(penalty, *it) < 0) {
            uint32_t* temp = new uint32_t[penalty[0] + 1];
            for(uint32_t j=0; j<=*penalty; ++j)
                temp[j] = penalty[j];
            penalties.insert(it, temp);
            nextIndex.insert(nextIndex.begin()+bucket, 0);
            values.insert(values.begin()+bucket, std::vector<uint64_t*>());
            break;
        }
        if(comparePenalties(penalty, *it) == 0) {
            break;
        }
        ++bucket;
    }
    if(bucket == (int)penalties.size()) {
        uint32_t* temp = new uint32_t[penalty[0] + 1];
        for(uint32_t j=0; j<=*penalty; ++j)
            temp[j] = penalty[j];
        penalties.push_back(temp);
        nextIndex.push_back(0);
        values.push_back(std::vector<uint64_t*>());
    }

    for(size_t i=0; i<values[bucket].size(); ++i) {
        auto max = i == values[bucket].size()-1 && nextIndex[bucket] ? nextIndex[bucket] : QUORUM_LIST_SINGLE_LENGTH * quorumSize;
        for(int j=0; j<max; j+=quorumSize) {
            auto equal = true;
            for(int q=0; q<quorumSize; ++q) {
                if(quorum[q] != values[bucket][i][j+q]) {
                    equal = false;
                    break;
                }
            }
            if(equal)
                return;
        }
    }

    if(!nextIndex[bucket])
        values[bucket].push_back(new uint64_t[QUORUM_LIST_SINGLE_LENGTH * quorumSize]);

    for(int i=0; i<quorumSize; ++i)
        values[bucket][values[bucket].size() - 1][nextIndex[bucket]++] = quorum[i];

    if(nextIndex[bucket] >= QUORUM_LIST_SINGLE_LENGTH * quorumSize)
        nextIndex[bucket] = 0;
}

void QuorumList::setQuorum(int bucket, int index, uint64_t *quorum) {
    int vectorIndex = index / QUORUM_LIST_SINGLE_LENGTH;
    int arrayIndex = index % QUORUM_LIST_SINGLE_LENGTH;
    for(int i=0; i<quorumSize; ++i)
        values[bucket][vectorIndex][arrayIndex + i] = quorum[i];
}

void QuorumList::setPenalty(int bucket, int index, uint32_t *penalty) {
    int vectorIndex = index / QUORUM_LIST_SINGLE_LENGTH;
    int arrayIndex = index % QUORUM_LIST_SINGLE_LENGTH;
    addQuorum(values[bucket][vectorIndex] + arrayIndex * quorumSize, penalty);

    int bucketVectorCount = values[bucket].size();
    while(vectorIndex < bucketVectorCount) {
        if(vectorIndex == bucketVectorCount - 1) {
            if(nextIndex[bucket]) {
                std::memmove(values[bucket][vectorIndex] + arrayIndex * quorumSize, values[bucket][vectorIndex] + (arrayIndex + 1) * quorumSize, nextIndex[bucket] - (arrayIndex + 1) * quorumSize);
                nextIndex[bucket] -= quorumSize;
            } else {
                delete [] values[bucket][values[bucket].size() - 1];
                values[bucket].pop_back();
                if(values[bucket].size()) {
                    nextIndex[bucket] = (QUORUM_LIST_SINGLE_LENGTH - 2) * quorumSize;
                } else {
                    values.erase(values.begin() + bucket);
                    nextIndex.erase(nextIndex.begin() + bucket);
                }
            }
            ++vectorIndex;
        } else {
            std::memmove(values[bucket][vectorIndex] + arrayIndex * quorumSize, values[bucket][vectorIndex] + (arrayIndex + 1) * quorumSize, (QUORUM_LIST_SINGLE_LENGTH - arrayIndex - 1) * quorumSize);
            std::memmove(values[bucket][vectorIndex] + (QUORUM_LIST_SINGLE_LENGTH - 1) * quorumSize, values[bucket][vectorIndex+1], quorumSize);
            arrayIndex = 0;
            ++vectorIndex;
        }
    }
}

uint64_t* QuorumList::at(int bucket, int index) {
    int vectorIndex = index / QUORUM_LIST_SINGLE_LENGTH;
    int arrayIndex = index % QUORUM_LIST_SINGLE_LENGTH;
    return values[bucket][vectorIndex] + arrayIndex * quorumSize;
}

uint32_t* QuorumList::getPenalty(int bucket) {
    return penalties[bucket];
}

int QuorumList::bucketSize(int bucket) {
    if(nextIndex[bucket])
        return (values[bucket].size() - 1) * QUORUM_LIST_SINGLE_LENGTH + nextIndex[bucket] / quorumSize;
    else
        return values[bucket].size() * QUORUM_LIST_SINGLE_LENGTH;
}

int QuorumList::buckets() {
    return values.size();
}

int QuorumList::getQuorumSize() {
    return quorumSize;
}

int QuorumList::comparePenalties(uint32_t *penalty1, uint32_t *penalty2) {
    int size1 = penalty1[0];
    int size2 = penalty2[0];
    for(int i=1; i<=size1 && i<=size2; ++i) {
        if(penalty1[i] != penalty2[i])
            return penalty1[i] < penalty2[i] ? -1 : 1;
    }
    return size1 == size2 ? 0 : size1 < size2 ? -1 : 1;
}
