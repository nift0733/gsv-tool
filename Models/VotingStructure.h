#ifndef VOTINGSTRUCTURE_H
#define VOTINGSTRUCTURE_H

#include <map>
#include <memory>
#include <vector>
#include <QString>

#include "Node.h"
#include "Edge.h"
#include "QuorumList.h"

class VotingStructure
{
public:
    enum ValidationResult {
        FullyDefined,
        NoRoot,
        MissingNameForNode
    };

    VotingStructure(std::vector<QString> operations);
    /**
     * @brief Gets the full path of the voting structure with directory and name.
     */
    QString getFilePath();
    /**
     * @brief Gets the file name of the voting structure.
     */
    QString getFileName();
    /**
     * @brief Sets the full path of the voting structure with directory and name.
     */
    void setFilePath(QString filePath);
    /**
     * @brief Gets the number of operation used in the voting structure.
     */
    int getNumberOfOperations();
    /**
     * @brief Gets the names of the used operations.
     */
    QStringList getOperations();
    /**
     * @brief Gets the root node.
     */
    std::shared_ptr<Node> getRoot();
    /**
     * @brief Sets the root node.
     */
    void setRoot(std::shared_ptr<Node> node);
    /**
     * @brief Gets all nodes of the voting structure.
     */
    std::vector<std::shared_ptr<Node>> getNodes();
    /**
     * @brief Adds a node virtual or physical at coordinate x,y.
     */
    std::shared_ptr<Node> addNode(bool isVirtual, int x, int y);
    /**
     * @brief Adds the node.
     */
    bool addNode(std::shared_ptr<Node> node);
    /**
     * @brief Removes the node.
     */
    bool removeNode(std::shared_ptr<Node> node);
    /**
     * @brief Gets all edges of the voting structure.
     */
    std::vector<std::shared_ptr<Edge>> getEdges();
    /**
     * @brief Adds an edge between the nodes from pointing at to.
     */
    std::shared_ptr<Edge> addEdge(std::shared_ptr<Node> from, std::shared_ptr<Node> to);
    /**
     * @brief Removes the edge.
     */
    bool removeEdge(std::shared_ptr<Edge> edge);
    /**
     * @brief Move each node by dx,dy.
     */
    void translate(int dx, int dy);
    /**
     * @brief Checks if the voting structure is fully defined.
     */
    ValidationResult isFullyDefined();
    /**
     * @brief Checks if the given node has no successor except of those in path.
     */
    bool isPartialLeaf(std::shared_ptr<Node> node, std::vector<std::shared_ptr<Node>> &path);
    /**
     * @brief Gets the child nodes of node.
     */
    std::vector<std::shared_ptr<Node>> getChildren(std::shared_ptr<Node> node);
    /**
     * @brief Gets all edges of node ordered by penalty related to operation at opIndex.
     */
    std::vector<std::shared_ptr<Edge>> getEdgesOrderedByPenalty(std::shared_ptr<Node> node, int opIndex);
    /**
     * @brief Sets the expected quorum lists for the voting structure.
     */
    void setExpectedQuorumLists(std::map<QString, std::shared_ptr<QuorumList>> quorumLists);
    /**
     * @brief Gets the expected quorum lists of the voting structure.
     */
    std::vector<std::shared_ptr<QuorumList>> getExpectedQuorumLists();
    /**
     * @brief Returns a map with the operations of the voting structure related to the expected quorum lists for eack operation.
     */
    std::map<QString, std::shared_ptr<QuorumList>> getExpectedQuorumMap();
    /**
     * @brief Determines if the comparision of calculated to expected quorum lists is to be done in quorum set semantics.
     */
    bool verifySimpleSet;

private:
    QString filePath;
    std::vector<QString> operations;
    std::map<QString, std::shared_ptr<QuorumList>> expectedQuorumLists;
    std::shared_ptr<Node> root;
    std::vector<std::shared_ptr<Node>> nodes;
    std::vector<std::shared_ptr<Edge>> edges;
};

#endif // VOTINGSTRUCTURE_H
