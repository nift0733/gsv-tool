#ifndef MAINMODEL_H
#define MAINMODEL_H

#include <QObject>
#include <memory>

#include "Topology.h"
#include "VotingStructure.h"

class MainModel : public QObject {
    Q_OBJECT

public:
    MainModel();

    std::shared_ptr<Topology> topology;
    std::shared_ptr<VotingStructure> votingStructure;

    /**
     * @brief Returns if a topology is loaded.
     */
    bool isTopologyLoaded();
    /**
     * @brief Returns if a voting structure is loaded.
     */
    bool isVotingStructureLoaded();
    /**
     * @brief Returns if changes to the loaded voting structure has been made.
     */
    bool isVotingStructureChanged();
    /**
     * @brief Note a change to the loading structure has been performed.
     */
    void setVotingStructureChanged(bool value);
    /**
     * @brief Gets the selected node.
     */
    std::shared_ptr<Node> getSelectedNode();
    /**
     * @brief Gets the selected edge.
     */
    std::shared_ptr<Edge> getSelectedEdge();

    /**
     * @brief Selects the given node.
     */
    void selectNode(std::shared_ptr<Node> node);
    /**
     * @brief Selects the given edge.
     */
    void selectEdge(std::shared_ptr<Edge> edge);
    /**
     * @brief Unselects all nodes and edges.
     */
    void unselectAll();

signals:
    /**
     * @brief Signals an action has been performed changing the state of the view.
     */
    void viewChanged();

private:
    std::shared_ptr<Node> selectedNode;
    std::shared_ptr<Edge> selectedEdge;
    bool votingStructureChanged;
};

#endif // MAINMODEL_H
