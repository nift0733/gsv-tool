#include "Topology.h"

#include <exception>
#include <QRegExp>

#include "../Config/Path.h"

#define newQString(X) QString(X.toLocal8Bit().data())

Topology::Topology(QString filePath) : filePath(filePath), numberOfNodes(0), variables(), nodes(), availabilities(), costs(), connectionAvailabilites() {
    //
}

QString Topology::getFilePath() {
    return filePath;
}

QString Topology::getFileName() {
    return Path::getFileName(filePath);
}

bool Topology::addVariable(QString identifier) {
    if(!checkNaming(identifier))
        return false;
    if(std::any_of(variables.begin(), variables.end(), [identifier] (std::pair<QString, double> variable) { return identifier == variable.first; } ))
        return false;

    variables.push_back(std::pair<QString, double>(newQString(identifier), 0.0));
    return true;
}

bool Topology::setVariable(QString identifier, double value) {
    for(size_t i=0; i<variables.size(); ++i) {
        if(variables[i].first == identifier) {
            variables[i] = std::pair<QString, double>(newQString(identifier), value);
            return true;
        }
    }
    return false;
}

bool Topology::addNode(QString name, QString availability, QString cost) {
    if(!checkNaming(name))
        return false;
    if(std::any_of(variables.begin(), variables.end(), [name] (std::pair<QString, double> variable) { return name == variable.first; } ))
        return false;
    if(std::any_of(nodes.begin(), nodes.end(), [name] (QString node) { return name == node; } ))
        return false;

    try {
        std::pair<double, QString> availabilityPair = readValue(availability);
        std::pair<double, QString> costPair = readValue(cost);

        nodes.push_back(newQString(name));
        availabilities.push_back(availabilityPair);
        costs.push_back(costPair);
        for(size_t i=0; i<connectionAvailabilites.size(); ++i) {
            connectionAvailabilites[i].push_back(std::pair<double, QString>(0.0, ""));
        }
        connectionAvailabilites.push_back(std::vector<std::pair<double, QString>>());
        for(size_t i=0; i<connectionAvailabilites.size(); ++i) {
            connectionAvailabilites.back().push_back(std::pair<double, QString>(0.0, ""));
        }
        ++numberOfNodes;
        return true;
    } catch (const std::exception&) {
        return false;
    }
}

bool Topology::setConnectionAvailability(QString node1, QString node2, QString availability) {
    try {
        size_t index1 = getNodeIndex(node1);
        size_t index2 = getNodeIndex(node2);

        std::pair<double, QString> value = readValue(availability);
        connectionAvailabilites[index1][index2] = value;
        return true;
    } catch(const std::exception&) {
        return false;
    }
}

bool Topology::isValid() {
    return numberOfNodes > 0;
}

int Topology::getNumberOfNodes() {
    return numberOfNodes;
}

std::vector<QString> Topology::getNodes() {
    return nodes;
}

double Topology::getAvailability(QString node) {
    size_t index = getNodeIndex(node);
    return getValue(availabilities[index]);
}

double Topology::getCost(QString node) {
    size_t index = getNodeIndex(node);
    return getValue(costs[index]);
}

double Topology::getAvailability(QString node1, QString node2) {
    size_t index1 = getNodeIndex(node1);
    size_t index2 = getNodeIndex(node2);
    return getValue(connectionAvailabilites[index1][index2]);
}

std::pair<double, QString> Topology::readValue(QString value) {
    try {
        double dValue = std::stod(value.toLocal8Bit().data());
        return std::pair<double, QString>(dValue, "");
    } catch (const std::exception&) {
        std::vector<QString> varIdentifiers(variables.size());
        std::transform(variables.begin(), variables.end(), varIdentifiers.begin(), [] (std::pair<QString, double> pair) {return pair.first;});

        for(QString identifier : varIdentifiers) {
            if(identifier == value)
                return std::pair<double, QString>(-1, newQString(value));
        }
        throw std::invalid_argument("value");
    }
}

double Topology::getValue(std::pair<double, QString> value) {
    if(value.second == "")
        return value.first;
    for(size_t i=0; i<variables.size(); ++i) {
        if(variables[i].first == value.second)
            return variables[i].second;
    }
    return value.first;
}

bool Topology::checkNaming(QString name) {
    return name.indexOf(QRegExp("[A-Za-z][A-Za-z0-9]*")) >= 0;
}

size_t Topology::getNodeIndex(QString name) {
    for(size_t i=0; i<nodes.size(); ++i) {
        if(nodes[i] == name)
            return i;
    }
    throw std::out_of_range("name");
}
