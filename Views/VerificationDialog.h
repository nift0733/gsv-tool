#ifndef VERIFICATIONDIALOG_H
#define VERIFICATIONDIALOG_H

#include <QDialog>

#include <QLabel>
#include <QTextBrowser>
#include <memory>
#include <vector>

#include "../Models/VotingStructure.h"
#include "../Models/Topology.h"

namespace Ui {
class VerificationDialog;
}

class VerificationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit VerificationDialog(QWidget *parent = nullptr);
    ~VerificationDialog();

    /**
     * @brief Shows the dialog and returns if it was accepted.
     */
    bool show(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology);

private:
    Ui::VerificationDialog *ui;

    std::vector<QLabel*> labels;
    std::vector<QTextEdit*> textEdits;

    /**
     * @brief Changes the expected quorum lists of the given voting structure to the entered ones.
     */
    QString changeExpectedQuorumLists(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology);
    /**
     * @brief The main part of the show method, which is called recursively on failure.
     */
    bool showInternal(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology);
};

#endif // VERIFICATIONDIALOG_H
