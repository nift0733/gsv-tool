#include "NewVotingStructureDialog.h"
#include "ui_NewVotingStructureDialog.h"

#include <QRegExpValidator>

#define ADD_OP(X) if(ui->X->text() != "") operationResults.push_back(ui->X->text())

NewVotingStructureDialog::NewVotingStructureDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewVotingStructureDialog)
{
    ui->setupUi(this);

    const char *reg = "[A-Za-z_][A-Za-z0-9_]*";
    ui->op1->setValidator(new QRegExpValidator(QRegExp(reg)));
    ui->op2->setValidator(new QRegExpValidator(QRegExp(reg)));
    ui->op3->setValidator(new QRegExpValidator(QRegExp(reg)));
    ui->op4->setValidator(new QRegExpValidator(QRegExp(reg)));
    ui->op5->setValidator(new QRegExpValidator(QRegExp(reg)));
}

NewVotingStructureDialog::~NewVotingStructureDialog()
{
    delete ui;
}

bool NewVotingStructureDialog::show(std::vector<QString> &operationResults) {
    if(exec() != DialogCode::Accepted)
        return false;

    ADD_OP(op1);
    ADD_OP(op2);
    ADD_OP(op3);
    ADD_OP(op4);
    ADD_OP(op5);
    return operationResults.size() > 0;
}
