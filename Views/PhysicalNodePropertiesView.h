#ifndef PHYSICALNODEPROPERTIESVIEW_H
#define PHYSICALNODEPROPERTIESVIEW_H

#include <QWidget>
#include <QTableWidgetItem>
#include <memory>

#include "../Models/Node.h"

namespace Ui {
class PhysicalNodePropertiesView;
}

class PhysicalNodePropertiesView : public QWidget
{
    Q_OBJECT

public:
    explicit PhysicalNodePropertiesView(QWidget *parent = nullptr);
    ~PhysicalNodePropertiesView();

    /**
     * @brief Initializes the view.
     */
    void initView(std::shared_ptr<Node> node, QStringList topologyNodeNames, QStringList operations);

signals:
    void editDone();

private slots:
    void on_nameEdit_currentTextChanged(const QString &arg1);

    void on_voteEdit_textChanged(const QString &arg1);

    void on_quorumEdit_itemChanged(QTableWidgetItem *item);

private:
    Ui::PhysicalNodePropertiesView *ui;

    bool isInInit;
    std::shared_ptr<Node> node;
};

#endif // PHYSICALNODEPROPERTIESVIEW_H
