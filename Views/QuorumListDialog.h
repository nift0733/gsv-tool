#ifndef QUORUMLISTDIALOG_H
#define QUORUMLISTDIALOG_H

#include <QPushButton>
#include <QDialog>
#include <QLabel>
#include <QTextBrowser>
#include <memory>

#include "../Models/QuorumList.h"
#include "../Models/VotingStructure.h"

namespace Ui {
class QuorumListDialog;
}

class QuorumListDialog : public QDialog
{
    Q_OBJECT

public:
    explicit QuorumListDialog(QWidget *parent = nullptr);
    ~QuorumListDialog();

    /**
     * @brief Fills the data for the dialog.
     */
    void fillData(std::shared_ptr<VotingStructure> votingStructure, std::vector<std::shared_ptr<QuorumList>> &quorumLists, std::vector<QString> nodes, uint64_t duration);

signals:
    void closed();

protected:
    void closeEvent(QCloseEvent *) override;

private slots:
    void on_optionDisplayLine_clicked();

    void on_optionDisplayCompact_clicked();

    void on_optionDisplaySet_clicked();

    void on_optionDisplayBinary_clicked();

    void on_optionDisplayMinimal_clicked();

    void on_optionDisplayComplete_clicked();

private:
    Ui::QuorumListDialog *ui;

    int numberOfLists;
    std::shared_ptr<VotingStructure> votingStructure;
    std::shared_ptr<QuorumList>* quorumLists;
    QLabel* quorumListLabels;
    QTextBrowser* quorumListDisplays;
    QPushButton* quorumListTesters;
    std::vector<QString> nodes;

    /**
     * @brief Sets the texts of the output fields for quorum lists.
     */
    void setQuorumStrings();
    /**
     * @brief Transforms the quorumList to its text representation.
     */
    QString quorumListToString(std::shared_ptr<QuorumList> quorumList);
    /**
     * @brief Verify the quorum list for operation op against the expected quorum list and shows the results in a message box.
     */
    void verify(const QString& op);
    /**
     * @brief Cleans up the dialog.
     */
    void cleanup();
};

#endif // QUORUMLISTDIALOG_H
