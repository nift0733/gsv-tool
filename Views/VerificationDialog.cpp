#include "VerificationDialog.h"
#include "ui_VerificationDialog.h"

#include <QMessageBox>

#include "../Persistence/QuorumListReader.h"
#include "../Persistence/QuorumListWriter.h"

VerificationDialog::VerificationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VerificationDialog)
{
    ui->setupUi(this);
}

VerificationDialog::~VerificationDialog()
{
    for(auto label : labels)
        delete label;
    for(auto textEdit : textEdits)
        delete textEdit;
    delete ui;
}

bool VerificationDialog::show(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology) {
    bool first = true;
    for(auto &&node : topology->getNodes()) {
        ui->nodes->insertPlainText((first ? "" : ", ") + node);
        first = false;
    }

    ui->verifySimpleSetOption->setChecked(votingStructure->verifySimpleSet);

    int row = 3;
    for(auto &&op : votingStructure->getOperations()) {
        auto label = new QLabel(ui->contentWidget);
        auto textEdit = new QTextEdit(ui->contentWidget);
        label->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
        labels.push_back(label);
        textEdits.push_back(textEdit);

        label->setText(op);
        auto quorumMap = votingStructure->getExpectedQuorumMap();
        auto it = quorumMap.find(op);
        if(it != quorumMap.end())
            textEdit->setText(QuorumListWriter::writeQuorumList(it->second, topology->getNodes()));

        ui->contentWidgetLayout->addWidget(label, row, 1);
        ui->contentWidgetLayout->addWidget(textEdit, row++, 2);
    }

    return showInternal(votingStructure, topology);
}

bool VerificationDialog::showInternal(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology) {
    if(exec() != DialogCode::Accepted)
        return false;

    if(std::all_of(textEdits.begin(), textEdits.end(), [](QTextEdit* expected){return expected->toPlainText().trimmed() == "";})) {
        votingStructure->setExpectedQuorumLists(std::map<QString, std::shared_ptr<QuorumList>>());
        return true;
    }
    auto failedOperation = changeExpectedQuorumLists(votingStructure, topology);
    if(failedOperation == "")
        return true;

    QMessageBox::warning(this, tr("Error"), tr("The quorum list could not be read correctly for operation: ") + failedOperation);
    return showInternal(votingStructure, topology);
}

QString VerificationDialog::changeExpectedQuorumLists(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology) {
    std::map<QString, std::shared_ptr<QuorumList>> result;
    for(size_t i=0; i<labels.size(); ++i) {
        auto quorumList = QuorumListReader::readQuorumList(textEdits[i]->toPlainText().trimmed(), topology->getNodes());
        if(!quorumList)
            return labels[i]->text();
        if(ui->verifySimpleSetOption->isChecked() && quorumList->buckets() != 1)
            return labels[i]->text() + tr(" must contain exactly one quorum set.");
        result[labels[i]->text()] = quorumList;
    }
    votingStructure->verifySimpleSet = ui->verifySimpleSetOption->isChecked();
    votingStructure->setExpectedQuorumLists(result);
    return "";
}
