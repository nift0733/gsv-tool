#ifndef VIRTUALNODEPROPERTIESVIEW_H
#define VIRTUALNODEPROPERTIESVIEW_H

#include <QWidget>
#include <QLineEdit>
#include <QTableWidgetItem>
#include <memory>

#include "../Models/Node.h"

namespace Ui {
class VirtualNodePropertiesView;
}

class VirtualNodePropertiesView : public QWidget
{
    Q_OBJECT

public:
    explicit VirtualNodePropertiesView(QWidget *parent = nullptr);
    ~VirtualNodePropertiesView();

    /**
     * @brief Initializes the view.
     */
    void initView(std::shared_ptr<Node> node, QStringList operations);

signals:
    void editDone();

private slots:
    void on_nameEdit_textChanged(const QString &arg1);

    void on_voteEdit_textChanged(const QString &arg1);

    void on_quorumEdit_itemChanged(QTableWidgetItem *item);

private:
    Ui::VirtualNodePropertiesView *ui;

    bool isInInit;
    std::shared_ptr<Node> node;

    /**
     * @brief Signals a change to the name, vote or scalar quorum.
     */
    void emitEditDone(QLineEdit *edit);
};

#endif // VIRTUALNODEPROPERTIESVIEW_H
