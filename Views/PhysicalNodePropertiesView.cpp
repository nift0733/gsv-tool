#include "PhysicalNodePropertiesView.h"
#include "ui_PhysicalNodePropertiesView.h"

PhysicalNodePropertiesView::PhysicalNodePropertiesView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PhysicalNodePropertiesView),
    isInInit(false),
    node(nullptr)
{
    ui->setupUi(this);

    ui->voteEdit->setValidator(new QIntValidator(0, 999999999, this));

    QStringList quorumHeaders;
    quorumHeaders.append(tr("Name"));
    quorumHeaders.append(tr("Value"));
    ui->quorumEdit->setHorizontalHeaderLabels(quorumHeaders);
}

PhysicalNodePropertiesView::~PhysicalNodePropertiesView()
{
    delete ui;
}

void PhysicalNodePropertiesView::initView(std::shared_ptr<Node> node, QStringList topologyNodeNames, QStringList operations) {
    isInInit = true;

    this->node = node;
    ui->idEdit->setText(QString::number(node->getId()));
    ui->nameEdit->clear();
    ui->nameEdit->addItems(topologyNodeNames);
    ui->nameEdit->setCurrentText(node->identifier);
    ui->voteEdit->setText(QString::number(node->vote));
    ui->quorumEdit->setRowCount(0);
    ui->quorumEdit->setRowCount(operations.size());
    for(int i=0; i<operations.size(); ++i) {
        QTableWidgetItem *op = new QTableWidgetItem(operations[i]);
        ui->quorumEdit->setItem(i, 0, op);
        QTableWidgetItem *quorum = new QTableWidgetItem(QString::number(node->quorumVector[i]));
        ui->quorumEdit->setItem(i, 1, quorum);
    }

    isInInit = false;
}

void PhysicalNodePropertiesView::on_nameEdit_currentTextChanged(const QString &arg1)
{
    if(isInInit)
        return;

    if(node && arg1 != "") {
        node->identifier = arg1;
        emit editDone();
    }
}

void PhysicalNodePropertiesView::on_voteEdit_textChanged(const QString &arg1)
{
    if(isInInit)
        return;

    bool ok = false;
    int value = arg1.toInt(&ok);
    if(node && ok && value >= 0) {
        node->vote = value;
        int cPos = ui->voteEdit->cursorPosition();
        emit editDone();
        ui->voteEdit->setFocus();
        ui->voteEdit->setCursorPosition(cPos);
    }
}

void PhysicalNodePropertiesView::on_quorumEdit_itemChanged(QTableWidgetItem *item)
{
    if(isInInit || item->column() != 1)
        return;

    int value = 0;
    bool ok = false;
    value = item->text().toInt(&ok);
    if(node && ok && value >= 0) {
        node->quorumVector[item->row()] = value;
        emit editDone();
    } else if(node) {
        item->setText(QString::number(node->quorumVector[item->row()]));
    }
}
