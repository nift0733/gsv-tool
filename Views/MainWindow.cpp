#include "Views/MainWindow.h"
#include "ui_MainWindow.h"

#include "../Algorithm/QuorumListCalculator.h"
#include "../Config/Path.h"
#include "../Generators/TlpGenerator.h"
#include "../Persistence/TopologyReader.h"
#include "../Persistence/TopologyWriter.h"
#include "../Persistence/VotingStructureReader.h"
#include "../Persistence/VotingStructureWriter.h"
#include "../Validation/SRValidator.h"
#include "../Validation/SyntaxValidator.h"
#include "../Views/AboutDialog.h"
#include "../Views/NewVotingStructureDialog.h"
#include "../Views/VerificationDialog.h"

#include <QCloseEvent>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QPainter>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QFile xsd(":/xsd/res/votingstructure.xsd");
    QFile metaXsd(":/xsd/res/votingstructure_meta.xsd");
    VotingStructurePersistenceBase::Init(xsd, metaXsd);

    quorumListDialog = nullptr;
    availabilityDialog = nullptr;
    costDialog = nullptr;

    edgePropertiesView = new EdgePropertiesView(ui->propertiesBox);
    edgePropertiesView->setVisible(false);
    ui->propertiesBox->layout()->addWidget(edgePropertiesView);

    physicalNodePropertiesView = new PhysicalNodePropertiesView(ui->propertiesBox);
    physicalNodePropertiesView->setVisible(false);
    ui->propertiesBox->layout()->addWidget(physicalNodePropertiesView);

    virtualNodePropertiesView = new VirtualNodePropertiesView(ui->propertiesBox);
    virtualNodePropertiesView->setVisible(false);
    ui->propertiesBox->layout()->addWidget(virtualNodePropertiesView);

    statusLabel = new QLabel(tr("Nothing loaded."));
    ui->statusbar->addPermanentWidget(statusLabel);

    model = std::make_shared<MainModel>();
    QObject::connect(model.get(), &MainModel::viewChanged, this, &MainWindow::on_viewChanged);
    QObject::connect(virtualNodePropertiesView, &VirtualNodePropertiesView::editDone, this, &MainWindow::on_somethingChanged);
    QObject::connect(physicalNodePropertiesView, &PhysicalNodePropertiesView::editDone, this, &MainWindow::on_somethingChanged);
    QObject::connect(edgePropertiesView, &EdgePropertiesView::editDone, this, &MainWindow::on_somethingChanged);
    graphRenderer = new GraphRenderer(ui->mainBox, model);
    ui->mainBoxLayout->addWidget(graphRenderer);
}

MainWindow::~MainWindow()
{
    delete statusLabel;
    delete ui;
    delete graphRenderer;
    delete edgePropertiesView;
    delete physicalNodePropertiesView;
    delete virtualNodePropertiesView;
    if(quorumListDialog)
        delete quorumListDialog;
    if(availabilityDialog)
        delete availabilityDialog;
    if(costDialog)
        delete costDialog;
}

void MainWindow::closeEvent(QCloseEvent *event) {
    if(!checkVotingStructureToSave()) {
        event->ignore();
        return;
    }

    QMainWindow::closeEvent(event);
}

void MainWindow::on_somethingChanged() {
    model->setVotingStructureChanged(true);
    updateUi();
}

void MainWindow::on_viewChanged() {
    updateUi();
}

void MainWindow::on_actionQuit_triggered()
{
    close();
}

void MainWindow::on_actionNewTopology_triggered()
{
    if(!checkVotingStructureToSave())
        return;

    bool ok = false;
    int count = QInputDialog::getInt(this, tr("New full topology"), tr("Count of physical nodes"), 5, 0, INT_MAX, 1, &ok);
    if(!ok)
        return;

    if(createTopology(count))
        updateUi();
}

void MainWindow::on_actionLoadTopology_triggered()
{
    if(!checkVotingStructureToSave())
        return;

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Topology"), Path::getTopologyDefault(), tr("Topology Files (*.top)"));
    if(fileName == "")
        return;

    Path::usedTopologyFile(fileName);
    model->topology = TopologyReader::readFromFile(fileName);
    model->unselectAll();
    model->votingStructure.reset();
    noteAvailabilitiesOfConnectionsIgnored();
    updateUi();
}

void MainWindow::on_actionNewStructure_triggered()
{
    if(!checkVotingStructureToSave())
        return;

    std::vector<QString> ops;
    NewVotingStructureDialog dialog;
    if(!dialog.show(ops))
        return;

    model->unselectAll();
    model->votingStructure = std::make_shared<VotingStructure>(ops);
    updateUi();
}

void MainWindow::on_actionLoadStructure_triggered()
{
    if(!checkVotingStructureToSave())
        return;

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Voting Structure"), Path::getStructureDefault(), tr("Voting Structure (*.gsv)"));
    if(fileName == "")
        return;

    Path::usedStructureFile(fileName);
    auto result = VotingStructureReader::readFromFile(fileName);
    auto votingStructure = result.first;
    auto topology = result.second;
    if(votingStructure && topology && topology->isValid()) {
        model->votingStructure = votingStructure;
        model->topology = topology;
        model->setVotingStructureChanged(false);
        noteAvailabilitiesOfConnectionsIgnored();
        updateUi();
    } else {
        QMessageBox::warning(this, tr("Error"), tr("Voting structure could not be loaded."));
    }
}

void MainWindow::on_actionSaveStructure_triggered()
{
    if(!handleVotingStructureFullyDefined(tr("Can't save")))
        return;
    QString fileName = model->votingStructure->getFilePath();
    if(fileName == "")
        return;
    saveVotingStructure(fileName);
}

void MainWindow::on_actionSaveAsStructure_triggered()
{
    if(!handleVotingStructureFullyDefined(tr("Can't save")))
        return;
    saveVotingStructureAs();
}

void MainWindow::on_actionImportStructure_triggered()
{
    if(!checkVotingStructureToSave())
        return;

    QString fileName = QFileDialog::getOpenFileName(this, tr("Import voting structure"), Path::getStructureXmlDefault(), tr("Xml Files (*.xml)"));
    if(fileName == "")
        return;

    auto votingStructure = VotingStructureReader::importFromFile(fileName, model->topology);
    if(votingStructure) {
        this->model->votingStructure = votingStructure;
        this->model->setVotingStructureChanged(true);
        updateUi();
    } else {
        QMessageBox::warning(this, tr("Error"), tr("Voting structure could not be imported. Check if the voting structure fits to the loaded topology."));
    }
}

void MainWindow::on_actionExportStructure_triggered()
{
    if(!handleVotingStructureFullyDefined(tr("Can't export")))
        return;

    QString fileName = QFileDialog::getSaveFileName(this, tr("Export voting structure"), Path::getStructureXmlDefault(), tr("Xml Files (*.xml)"));
    if(fileName == "")
        return;
    if(!fileName.endsWith(".xml"))
        fileName = fileName + ".xml";

    Path::usedStructureXmlFile(fileName);
    if(!VotingStructureWriter::exportToFile(model->votingStructure, fileName))
        QMessageBox::warning(this, tr("Error"), tr("Voting structure could not be exported."));
}

void MainWindow::on_action_TLP_triggered()
{
    if(!checkVotingStructureToSave())
        return;

    bool ok = false;
    int columns = QInputDialog::getInt(this, tr("Generate TLP"), tr("Count of columns"), 3, 2, INT_MAX, 1, &ok);
    if(!ok)
        return;
    int rows = QInputDialog::getInt(this, tr("Generate TLP"), tr("Count of rows"), 3, 2, INT_MAX, 1, &ok);
    if(!ok)
        return;

    if(!createTopology(columns * rows))
        return;

    model->votingStructure = TlpGenerator::generate(model->topology, columns, rows);
    updateUi();
}

void MainWindow::on_actionVerificationOfStructure_triggered()
{
    VerificationDialog dialog(this);
    if(!dialog.show(model->votingStructure, model->topology))
        return;

    model->setVotingStructureChanged(true);
    updateUi();
}

void MainWindow::on_actionValidateSyntax_triggered() {
    if(!handleVotingStructureFullyDefined(tr("Can't validate")))
        return;
    auto result = SyntaxValidator::validate(this->model->votingStructure);
    if(result.isValid()) {
        QMessageBox::information(this, tr("Syntax validation"), tr("The voting structure is syntactically correct."));
    }
    if(result.hasRootParent()) {
        QMessageBox::warning(this, tr("Syntax validation"), tr("The marked root node has a parent node."));
    }
    if(!result.getUnreachableNodes().isEmpty()) {
        QStringList nodeNames;
        for(auto node : result.getUnreachableNodes())
            nodeNames.append(node->identifier);
        if(nodeNames.length() > 1)
            QMessageBox::warning(this, tr("Syntax validation"), tr("The nodes ") + nodeNames.join(", ") + tr(" are not connected."));
        else
            QMessageBox::warning(this, tr("Syntax validation"), tr("The node ") + nodeNames[0] + tr(" is not connected."));
    }
    if(!result.getDuplicates().isEmpty()) {
        auto nodeNames = result.getDuplicates();
        if(nodeNames.length() > 1)
            QMessageBox::warning(this, tr("Syntax validation"), tr("The nodes ") + nodeNames.join(", ") + tr(" are not unique."));
        else
            QMessageBox::warning(this, tr("Syntax validation"), tr("The node ") + nodeNames[0] + tr(" is not unique."));
    }
}

void MainWindow::on_actionValidate1SR_triggered()
{
    auto ops = model->votingStructure->getOperations();
    if(ops.length() != 2 || (ops[0] != "read" && ops[1] != "read") || (ops[0] != "write" && ops[1] != "write")) {
        QMessageBox::warning(this, tr("Can't validate"), tr("1SR validation is only possible with operations 'read' and 'write'."));
        return;
    }
    if(!handleSyntaxError(tr("Can't validate")))
        return;

    auto quorumLists = QuorumListCalculator::calculate(model->votingStructure, model->topology);
    auto opList = model->votingStructure->getOperations();
    auto result = SRValidator::validate(quorumLists, std::vector<QString>(opList.begin(), opList.end()));
    QString errorText = tr("The voting structure does not satisfy the 1SR requirements.");
    if(result.isValid()) {
        QMessageBox::information(this, tr("1SR validation"), tr("The voting structure satisfies the 1SR requirements."));
        return;
    }
    if(result.isReadEmpty())
        errorText += "\r\n\r\n" + tr("The read quorum list is empty.");
    if(result.isWriteEmpty())
        errorText += "\r\n\r\n" + tr("The write quorum list is empty.");
    if(!result.isReadValid())
        errorText += "\r\n\r\n" + tr("There is at least one read quorum that does not intersect with every write quorum.");
    if(!result.isWriteValid())
        errorText += "\r\n\r\n" + tr("There is at least one write quorum that does not intersect with every other write quorum.");

    QMessageBox::warning(this, tr("1SR validation"), errorText);
}

void MainWindow::on_actionCalculateQuorumList_triggered() {
    if(!handleSyntaxError(tr("Can't calculate")))
        return;

    auto startTime = QDateTime::currentMSecsSinceEpoch();
    auto quorumLists = QuorumListCalculator::calculate(model->votingStructure, model->topology);
    auto duration = qRound64((QDateTime::currentMSecsSinceEpoch() - startTime) / 1000.0);
    if(quorumListDialog) {
        quorumListDialog->fillData(this->model->votingStructure, quorumLists, this->model->topology->getNodes(), duration);
    } else {
        quorumListDialog = new QuorumListDialog(this);
        QObject::connect(quorumListDialog, &QuorumListDialog::closed, this, &MainWindow::on_quorumListDialog_closed);
        quorumListDialog->fillData(this->model->votingStructure, quorumLists, this->model->topology->getNodes(), duration);
        quorumListDialog->show();
    }
}

void MainWindow::on_actionCalculateAvailability_triggered()
{
    if(!handleSyntaxError(tr("Can't calculate")))
        return;

    if(availabilityDialog) {
        availabilityDialog->fillData(this->model->votingStructure, this->model->topology);
    } else {
        availabilityDialog = new AnalysisDialog(this, AnalysisDialog::Availability);
        QObject::connect(availabilityDialog, &AnalysisDialog::closed, this, &MainWindow::on_availabilityDialog_closed);
        availabilityDialog->fillData(this->model->votingStructure, this->model->topology);
        availabilityDialog->show();
    }
}

void MainWindow::on_actionCalculateCosts_triggered()
{
    if(!handleSyntaxError(tr("Can't calculate")))
        return;

    if(costDialog) {
        costDialog->fillData(this->model->votingStructure, this->model->topology);
    } else {
        costDialog = new AnalysisDialog(this, AnalysisDialog::Cost);
        QObject::connect(costDialog, &AnalysisDialog::closed, this, &MainWindow::on_costDialog_closed);
        costDialog->fillData(this->model->votingStructure, this->model->topology);
        costDialog->show();
    }
}

void MainWindow::on_actionAbout_triggered()
{
    AboutDialog dialog;
    dialog.exec();
}

void MainWindow::on_toolOptionMove_toggled(bool checked)
{
    if(!checked)
        return;
    graphRenderer->state = GraphRenderer::Move;
}

void MainWindow::on_toolOptionRemove_toggled(bool checked)
{
    if(!checked)
        return;
    graphRenderer->state = GraphRenderer::Remove;
}

void MainWindow::on_toolOptionPhysical_toggled(bool checked)
{
    if(!checked)
        return;
    graphRenderer->state = GraphRenderer::AddPhysical;
}

void MainWindow::on_toolOptionVirtual_toggled(bool checked)
{
    if(!checked)
        return;
    graphRenderer->state = GraphRenderer::AddVirtual;
}

void MainWindow::on_toolOptionEdge_toggled(bool checked)
{
    if(!checked)
        return;
    graphRenderer->state = GraphRenderer::AddEdge;
}

void MainWindow::on_arrangeButton_clicked()
{
    graphRenderer->arrange();
}

void MainWindow::on_applyScrollButton_clicked()
{
    model->votingStructure->translate(graphRenderer->getRelXPos(), graphRenderer->getRelYPos());
    graphRenderer->resetViewPosition();
    model->setVotingStructureChanged(true);
    updateUi();
}

void MainWindow::on_resetScrollButton_clicked()
{
    graphRenderer->resetViewPosition();
}

void MainWindow::on_resetScaleButton_clicked()
{
    graphRenderer->resetScale();
}

void MainWindow::on_makeRootButton_clicked()
{
    model->votingStructure->setRoot(model->getSelectedNode());
    graphRenderer->update();
}

void MainWindow::on_quorumListDialog_closed() {
    if(quorumListDialog)
        delete quorumListDialog;
    quorumListDialog = nullptr;
}

void MainWindow::on_availabilityDialog_closed() {
    if(availabilityDialog)
        delete availabilityDialog;
    availabilityDialog = nullptr;
}

void MainWindow::on_costDialog_closed() {
    if(costDialog)
        delete costDialog;
    costDialog = nullptr;
}

void MainWindow::updateUi() {
    // status bar
    if(model->isVotingStructureLoaded() && model->votingStructure->getFilePath() != "")
        statusLabel->setText(tr("Voting structure") + " " + model->votingStructure->getFileName() + " " + tr("loaded."));
    else if(model->isVotingStructureLoaded())
        statusLabel->setText(tr("New voting structure."));
    else if(model->isTopologyLoaded())
        statusLabel->setText(tr("Topology") + " " + model->topology->getFileName() + " " + tr("loaded."));
    else
        statusLabel->setText(tr("Nothing loaded."));
    if(model->isVotingStructureLoaded() && model->isVotingStructureChanged())
        statusLabel->setText(statusLabel->text() + "*");

    // enable / disable menu items
    ui->actionNewStructure->setEnabled(model->isTopologyLoaded());
    ui->actionSaveStructure->setEnabled(model->isVotingStructureChanged() && model->isVotingStructureLoaded() && model->votingStructure->getFilePath() != "");
    ui->actionSaveAsStructure->setEnabled(model->isVotingStructureLoaded());
    ui->actionImportStructure->setEnabled(model->isTopologyLoaded());
    ui->actionExportStructure->setEnabled(model->isVotingStructureLoaded());
    ui->actionVerificationOfStructure->setEnabled(model->isVotingStructureLoaded());

    ui->actionValidateSyntax->setEnabled(model->isVotingStructureLoaded());
    ui->actionValidate1SR->setEnabled(model->isVotingStructureLoaded());
    ui->actionCalculateQuorumList->setEnabled(model->isVotingStructureLoaded());
    ui->actionCalculateAvailability->setEnabled(model->isVotingStructureLoaded());
    ui->actionCalculateCosts->setEnabled(model->isVotingStructureLoaded());

    // enable / disable tool box
    ui->toolBox->setEnabled(model->isVotingStructureLoaded());

    // enable / disable buttons box
    ui->buttonsBox->setEnabled(model->isVotingStructureLoaded());
    ui->makeRootButton->setEnabled(model->getSelectedNode() != nullptr);

    // fill properties box
    ui->propertiesPlaceholder->setVisible(false);
    virtualNodePropertiesView->setVisible(false);
    physicalNodePropertiesView->setVisible(false);
    edgePropertiesView->setVisible(false);
    ui->propertiesBox->setEnabled(true);
    if(!model->isVotingStructureLoaded()) {
        ui->propertiesPlaceholder->setVisible(true);
        ui->propertiesBox->setEnabled(false);
    } else {
        if(model->getSelectedNode()) {
            if(model->getSelectedNode()->isVirtual) {
                virtualNodePropertiesView->initView(model->getSelectedNode(), model->votingStructure->getOperations());
                virtualNodePropertiesView->setVisible(true);
            } else {
                QStringList topologyNodes;
                for(QString str : model->topology->getNodes())
                    topologyNodes.push_back(str);
                physicalNodePropertiesView->initView(model->getSelectedNode(), topologyNodes, model->votingStructure->getOperations());
                physicalNodePropertiesView->setVisible(true);
            }
        } else if(model->getSelectedEdge()) {
            edgePropertiesView->initView(model->getSelectedEdge(), model->votingStructure->getOperations());
            edgePropertiesView->setVisible(true);
        } else {
            ui->propertiesPlaceholder->setVisible(true);
            ui->propertiesBox->setEnabled(false);
        }
    }

    graphRenderer->update();
}

bool MainWindow::handleVotingStructureFullyDefined(QString msgBoxTitle) {
    auto validation = model->votingStructure->isFullyDefined();
    if(validation == VotingStructure::FullyDefined)
        return true;

    if(validation == VotingStructure::NoRoot)
        QMessageBox::warning(this, msgBoxTitle, tr("The voting structure must have a root node"));
    else if(validation == VotingStructure::MissingNameForNode)
        QMessageBox::warning(this, msgBoxTitle, tr("All nodes must have an identifier."));
    else
        QMessageBox::warning(this, tr("Error"), msgBoxTitle + ".");
    return false;
}

bool MainWindow::saveVotingStructureAs() {
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save voting structure as"), Path::getStructureDefault(), tr("Voting Structure Files (*.gsv)"));
    if(fileName == "")
        return false;
    if(!fileName.endsWith(".gsv"))
        fileName = fileName + ".gsv";

    Path::usedStructureFile(fileName);
    saveVotingStructure(fileName);
    return true;
}

void MainWindow::saveVotingStructure(QString fileName) {
    if(VotingStructureWriter::writeToFile(model->votingStructure, model->topology, fileName)) {
        model->votingStructure->setFilePath(fileName);
        model->setVotingStructureChanged(false);
        updateUi();
    } else {
        QMessageBox::warning(this, tr("Error"), tr("Voting structure could not be saved."));
    }
}

bool MainWindow::checkVotingStructureToSave() {
    if(!model->isVotingStructureLoaded() || !model->isVotingStructureChanged())
        return true;
    auto canSave = model->votingStructure->isFullyDefined() == VotingStructure::FullyDefined;

    auto msgResult = QMessageBox::question(this,
                                           canSave ? tr("Save?") : tr("Unsaved changes"),
                                           canSave ? tr("The current voting structure has unsaved changes. Do you want to save the voting structure?")
                                                   : tr("The current voting structure has unsaved changes, but can't be saved. Do you want to proceed anyway?"),
                                           canSave ? (QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel) : (QMessageBox::Yes | QMessageBox::No));
    if(msgResult == QMessageBox::Cancel)
        return false;
    if(msgResult == QMessageBox::No)
        return canSave;
    if(!canSave)
        return true;

    if(model->votingStructure->getFilePath() == "")
        return saveVotingStructureAs();

    saveVotingStructure(model->votingStructure->getFilePath());
    return true;
}

bool MainWindow::handleSyntaxError(QString msgBoxTitle) {
    if(!handleVotingStructureFullyDefined(msgBoxTitle))
        return false;
    if(SyntaxValidator::validate(this->model->votingStructure).isValid())
        return true;

    QMessageBox::warning(this, msgBoxTitle, tr("The voting structure has syntax errors."));
    return false;
}

void MainWindow::noteAvailabilitiesOfConnectionsIgnored() {
    auto nodes = model->topology->getNodes();
    for (auto &&node1 : nodes) {
        for(auto &&node2 : nodes) {
            if(model->topology->getAvailability(node1, node2) < 1.0) {
                QMessageBox::information(this, tr("Attention"), tr("The topology contains at least one connection with an availability less than 100%. Those availabilities will be ignored and will be treated as 100% available."));
                return;
            }
        }
    }
}

bool MainWindow::createTopology(int countOfNodes) {
    QString dir = Path::getTopologyDefault() + QDir::separator() + "full" + QString::number(countOfNodes) + ".top";
    QString fileName = QFileDialog::getSaveFileName(this, tr("Create Topology"), dir, tr("Topology Files (*.top)"));
    if(fileName == "")
        return false;
    if(!fileName.endsWith(".top"))
        fileName.append(".top");

    Path::usedTopologyFile(fileName);
    if(!TopologyWriter::writeFullTopologyToFile(fileName, countOfNodes)) {
        QMessageBox::warning(this, tr("Error"), tr("Topology file could not be created"));
        return false;
    }
    model->topology = TopologyReader::readFromFile(fileName);
    model->unselectAll();
    model->votingStructure.reset();
    return true;
}
