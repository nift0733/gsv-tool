#include "AnalysisDialog.h"
#include "ui_AnalysisDialog.h"

#include <QCloseEvent>
#include <QFileDialog>

#include "../Algorithm/AvailabilityCostCalculator.h"
#include "../Algorithm/QuorumListCalculator.h"
#include "../Persistence/CsvExport.h"
#include "../Config/Path.h"

AnalysisDialog::AnalysisDialog(QWidget *parent, Type type) :
    QDialog(parent),
    ui(new Ui::AnalysisDialog), tabs()
{
    ui->setupUi(this);
    if(type == Type::Availability) {
        setWindowTitle(tr("Availability"));
        selector = [](AvailabilityCostCalculator::Result r) { return r.availability; };
    } else if(type == Type::Cost) {
        setWindowTitle(tr("Cost"));
        selector = [](AvailabilityCostCalculator::Result r) { return r.cost; };
    }
}

AnalysisDialog::~AnalysisDialog()
{
    cleanup();
    delete ui;
}

void AnalysisDialog::fillData(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology) {
    std::vector<double> range({0.00, 0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50, 0.55, 0.60, 0.65, 0.70, 0.75, 0.80, 0.85, 0.90, 0.95, 1.00});
    auto quorumLists = QuorumListCalculator::calculate(votingStructure, topology);
    auto operations = votingStructure->getOperations();
    for(int i = 0; i < operations.size(); ++i) {
        auto results = AvailabilityCostCalculator::calcRange("p", range, quorumLists[i], topology);
        std::vector<double> availabilities;
        std::transform(results.begin(), results.end(), std::back_inserter(availabilities), selector);
        addOrUpdateTab(votingStructure->getFileName().split(".")[0], operations[i], range, availabilities);
    }
}

void AnalysisDialog::closeEvent(QCloseEvent *event) {
    QDialog::closeEvent(event);
    if(event->isAccepted())
        emit closed();
}

void AnalysisDialog::cleanup() {
    while(ui->charts->count())
        ui->charts->removeTab(0);
    tabs.clear();
}

void AnalysisDialog::addOrUpdateTab(QString name, QString op, std::vector<double> vars, std::vector<double> availabilities) {
    int index = -1;
    for(size_t i = 0; i < tabs.size(); ++i) {
        if(tabs[i] == op) {
            index = i;
            break;
        }
    }
    if(index < 0) {
        auto chartView = new QChartView(ui->charts);
        chartView->setSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Expanding);
        index = ui->charts->addTab(chartView, op);
        tabs.insert(tabs.begin() + index, op);
    }

    auto chartView = (QChartView*) ui->charts->widget(index);
    for(int i = 0; i < chartView->chart()->series().count(); ++i) {
        if(chartView->chart()->series()[i]->name() == name) {
            chartView->chart()->removeSeries(chartView->chart()->series()[i]);
            break;
        }
    }

    auto lineSeries = new QLineSeries(chartView);
    lineSeries->setName(name);
    lineSeries->setPointsVisible(true);
    for(size_t i = 0; i < vars.size(); ++i)
        if(!qIsNaN(availabilities[i]))
            lineSeries->append(vars[i], availabilities[i]);
    chartView->chart()->addSeries(lineSeries);
    chartView->chart()->createDefaultAxes();
    chartView->chart()->axes(Qt::Horizontal).first()->setRange(vars[0], vars[vars.size()-1]);
    auto yAxis = dynamic_cast<QValueAxis*>(chartView->chart()->axes(Qt::Vertical).first());
    yAxis->setMin(0.0);
}

void AnalysisDialog::on_resetCmd_clicked() {
    cleanup();
}

void AnalysisDialog::on_exportCmd_clicked() {
    auto preferedName = ui->charts->tabText(ui->charts->currentIndex()) + "_" + windowTitle().toLower() + ".csv";
    QString fileName = QFileDialog::getSaveFileName(this, tr("Export ") + windowTitle(), Path::getExportDefault() + preferedName, tr("CSV (*.csv)"));
    if(fileName == "")
        return;
    if(!fileName.endsWith(".csv"))
        fileName.append(".csv");
    Path::usedExportFile(fileName);

    const double eps = 0.01;
    std::vector<QString> headers;
    headers.push_back("p");
    std::vector<std::vector<double>> values;
    std::vector<double> range({0.00, 0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50, 0.55, 0.60, 0.65, 0.70, 0.75, 0.80, 0.85, 0.90, 0.95, 1.00});
    for(auto &&index : range) {
        std::vector<double> row;
        row.push_back(index);
        values.push_back(row);
    }
    size_t column = 1;
    for(auto &&series : ((QChartView*)ui->charts->currentWidget())->chart()->series()) {
        auto lineSeries = (QLineSeries*) series;
        headers.push_back(lineSeries->name());
        for(int i = 0; i < lineSeries->count(); ++i) {
            for(size_t j = 0; j < range.size(); ++j) {
                if(abs(lineSeries->at(i).x() - range[j]) < eps) {
                    values[j].push_back(lineSeries->at(i).y());
                    break;
                }
            }
        }
        ++column;
        for(size_t j = 0; j < range.size(); ++j) {
            if(values[j].size() < column)
                values[j].push_back(NAN);
        }
    }
    if(!CsvExport::exportValues(fileName, headers, values)) {
        QMessageBox::warning(this, tr("Error"), tr("Values could not be exported"));
        return;
    }
}
