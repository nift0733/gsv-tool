#include "VirtualNodePropertiesView.h"
#include "ui_VirtualNodePropertiesView.h"

VirtualNodePropertiesView::VirtualNodePropertiesView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::VirtualNodePropertiesView),
    isInInit(false),
    node(nullptr)
{
    ui->setupUi(this);

    ui->voteEdit->setValidator(new QIntValidator(0, 999999999, this));
    ui->nameEdit->setValidator(new QRegExpValidator(QRegExp("[A-Za-z][A-Za-z0-9]*"), this));

    QStringList quorumHeaders;
    quorumHeaders.append(tr("Name"));
    quorumHeaders.append(tr("Value"));
    ui->quorumEdit->setHorizontalHeaderLabels(quorumHeaders);
}

VirtualNodePropertiesView::~VirtualNodePropertiesView()
{
    delete ui;
}

void VirtualNodePropertiesView::initView(std::shared_ptr<Node> node, QStringList operations) {
    isInInit = true;

    this->node = node;
    ui->idEdit->setText(QString::number(node->getId()));
    ui->nameEdit->setText(node->identifier);
    ui->voteEdit->setText(QString::number(node->vote));
    ui->quorumEdit->setRowCount(0);
    ui->quorumEdit->setRowCount(operations.size());
    for(int i=0; i<operations.size(); ++i) {
        QTableWidgetItem *op = new QTableWidgetItem(operations[i]);
        op->setFlags(op->flags() ^ Qt::ItemIsEditable ^ Qt::ItemIsSelectable);
        ui->quorumEdit->setItem(i, 0, op);
        QTableWidgetItem *quorum = new QTableWidgetItem(QString::number(node->quorumVector[i]));
        ui->quorumEdit->setItem(i, 1, quorum);
    }

    isInInit = false;
}

void VirtualNodePropertiesView::on_nameEdit_textChanged(const QString &arg1)
{
    if(isInInit)
        return;

    if(node && arg1 != "") {
        node->identifier = arg1;
        emitEditDone(ui->nameEdit);
    }
}

void VirtualNodePropertiesView::on_voteEdit_textChanged(const QString &arg1)
{
    if(isInInit)
        return;

    bool ok = false;
    int value = arg1.toInt(&ok);
    if(node && ok && value >= 0) {
        node->vote = value;
        emitEditDone(ui->voteEdit);
    }
}

void VirtualNodePropertiesView::emitEditDone(QLineEdit *edit) {
    int cPos = edit->cursorPosition();
    emit editDone();
    edit->setFocus();
    edit->setCursorPosition(cPos);
}

void VirtualNodePropertiesView::on_quorumEdit_itemChanged(QTableWidgetItem *item)
{
    if(isInInit || item->column() != 1)
        return;

    int value = 0;
    bool ok = false;
    value = item->text().toInt(&ok);
    if(node && ok && value >= 0) {
        node->quorumVector[item->row()] = value;
        emit editDone();
    } else if(node) {
        item->setText(QString::number(node->quorumVector[item->row()]));
    }
}
