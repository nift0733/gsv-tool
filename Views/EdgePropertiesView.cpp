#include "EdgePropertiesView.h"
#include "ui_EdgePropertiesView.h"

#include "../Widgets/PenaltyResetButton.h"

EdgePropertiesView::EdgePropertiesView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EdgePropertiesView),
    isInInit(false),
    edge(nullptr)
{
    ui->setupUi(this);

    QStringList penaltiesHeaders;
    penaltiesHeaders.append(tr("Name"));
    penaltiesHeaders.append(tr("Value"));
    penaltiesHeaders.append(tr(""));
    penaltiesHeaders.append(tr(""));
    ui->penaltiesEdit->setHorizontalHeaderLabels(penaltiesHeaders);
    ui->penaltiesEdit->setColumnWidth(0, 70);
    ui->penaltiesEdit->setColumnWidth(1, 100);
    ui->penaltiesEdit->setColumnWidth(2, 32);
    ui->penaltiesEdit->setColumnWidth(3, 32);
}

EdgePropertiesView::~EdgePropertiesView()
{
    delete ui;
}

void EdgePropertiesView::initView(std::shared_ptr<Edge> edge, QStringList operations) {
    isInInit = true;

    this->edge = edge;
    ui->penaltiesEdit->setRowCount(0);
    ui->penaltiesEdit->setRowCount(operations.size());
    for(int i=0; i<operations.size(); ++i) {
        QTableWidgetItem *op = new QTableWidgetItem(operations[i]);
        ui->penaltiesEdit->setItem(i, 0, op);
        QString penaltyString = edge->penaltyList[i] >= MAX_PENALTY ? PRIO_INF : edge->penaltyList[i] < 0 ? PRIO_FILLUP : QString::number(edge->penaltyList[i]);
        QTableWidgetItem *penalty = new QTableWidgetItem(penaltyString);
        ui->penaltiesEdit->setItem(i, 1, penalty);
        PenaltyResetButton *reset = new PenaltyResetButton(edge, i, MAX_PENALTY);
        reset->setFixedWidth(30);
        ui->penaltiesEdit->setCellWidget(i, 2, reset);
        QObject::connect(reset, &PenaltyResetButton::penaltyChanged, this, &EdgePropertiesView::emitEditDone);
        PenaltyResetButton *star = new PenaltyResetButton(edge, i, -1);
        star->setFixedWidth(30);
        ui->penaltiesEdit->setCellWidget(i, 3, star);
        QObject::connect(star, &PenaltyResetButton::penaltyChanged, this, &EdgePropertiesView::emitEditDone);
    }

    isInInit = false;
}

void EdgePropertiesView::on_penaltiesEdit_itemChanged(QTableWidgetItem *item)
{
    if(isInInit || item->column() != 1)
        return;

    int value = 0;
    bool ok = false;
    value = item->text().toInt(&ok);
    if(edge && ok && value > 0) {
        edge->penaltyList[item->row()] = value;
        emitEditDone();
    } else if(edge) {
        item->setText(QString::number(edge->penaltyList[item->row()]));
    }
}

void EdgePropertiesView::emitEditDone() {
    emit editDone();
}
