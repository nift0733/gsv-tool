#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>

#include "../Models/MainModel.h"
#include "EdgePropertiesView.h"
#include "PhysicalNodePropertiesView.h"
#include "QuorumListDialog.h"
#include "AnalysisDialog.h"
#include "VirtualNodePropertiesView.h"
#include "../Widgets/GraphRenderer.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent* event) override;

private slots:
    void on_somethingChanged();

    void on_viewChanged();

    void on_actionLoadTopology_triggered();

    void on_actionQuit_triggered();

    void on_actionNewStructure_triggered();

    void on_toolOptionMove_toggled(bool checked);

    void on_toolOptionPhysical_toggled(bool checked);

    void on_toolOptionVirtual_toggled(bool checked);

    void on_toolOptionEdge_toggled(bool checked);

    void on_toolOptionRemove_toggled(bool checked);

    void on_arrangeButton_clicked();

    void on_actionNewTopology_triggered();

    void on_actionSaveStructure_triggered();

    void on_actionSaveAsStructure_triggered();

    void on_makeRootButton_clicked();

    void on_actionLoadStructure_triggered();

    void on_resetScrollButton_clicked();

    void on_applyScrollButton_clicked();

    void on_actionAbout_triggered();

    void on_actionExportStructure_triggered();

    void on_actionImportStructure_triggered();

    void on_actionValidateSyntax_triggered();

    void on_actionCalculateQuorumList_triggered();

    void on_quorumListDialog_closed();

    void on_availabilityDialog_closed();

    void on_costDialog_closed();

    void on_actionValidate1SR_triggered();

    void on_actionCalculateAvailability_triggered();

    void on_actionCalculateCosts_triggered();

    void on_actionVerificationOfStructure_triggered();

    void on_action_TLP_triggered();

    void on_resetScaleButton_clicked();

private:
    Ui::MainWindow *ui;
    QLabel *statusLabel;
    GraphRenderer *graphRenderer;
    VirtualNodePropertiesView *virtualNodePropertiesView;
    PhysicalNodePropertiesView *physicalNodePropertiesView;
    EdgePropertiesView *edgePropertiesView;
    QuorumListDialog *quorumListDialog;
    AnalysisDialog *availabilityDialog;
    AnalysisDialog *costDialog;
    std::shared_ptr<MainModel> model;

    /**
     * @brief Updates the UI to represent the current state.
     */
    void updateUi();
    /**
     * @brief Checks if the loaded voting structure is fully defined and shows a message box if not.
     */
    bool handleVotingStructureFullyDefined(QString msgBoxTitle);
    /**
     * @brief Shows a dialog to chose a file name for the voting structure to save at.
     */
    bool saveVotingStructureAs();
    /**
     * @brief Saves the voting structure or shows a message box on failure.
     */
    void saveVotingStructure(QString fileName);
    /**
     * @brief Check if voting structure can be saved and asks user for further handling.
     */
    bool checkVotingStructureToSave();
    /**
     * @brief Handles syntax error by showing a message box.
     */
    bool handleSyntaxError(QString msgBoxTitle);
    /**
     * @brief Shows information text for topologies not fully connected.
     */
    void noteAvailabilitiesOfConnectionsIgnored();
    /**
     * @brief Creates a topology with countOfNodes physical nodes.
     */
    bool createTopology(int countOfNodes);
};
#endif // MAINWINDOW_H
