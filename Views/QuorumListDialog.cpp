#include "QuorumListDialog.h"
#include "ui_QuorumListDialog.h"

#include <QCloseEvent>
#include <QMessageBox>

#include "../Persistence/QuorumListWriter.h"
#include "../Algorithm/QuorumListComparator.h"

QuorumListDialog::QuorumListDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QuorumListDialog),
    numberOfLists(0)
{
    ui->setupUi(this);
    ui->unvisibleWidget1->setVisible(false);
    ui->unvisibleWidget2->setVisible(false);
    ui->unvisibleWidget3->setVisible(false);
}

QuorumListDialog::~QuorumListDialog()
{
    cleanup();
    delete ui;
}

void QuorumListDialog::fillData(std::shared_ptr<VotingStructure> votingStructure, std::vector<std::shared_ptr<QuorumList>> &quorumLists, std::vector<QString> nodes, uint64_t duration) {
    cleanup();
    ui->duration->setText("Duration: ~ " + QString::number(duration) + " sec");
    this->votingStructure = votingStructure;
    this->nodes.assign(nodes.begin(), nodes.end());
    auto operations = votingStructure->getOperations();
    numberOfLists = operations.size() < (int)quorumLists.size() ? operations.size() : quorumLists.size();
    this->quorumLists = new std::shared_ptr<QuorumList>[numberOfLists];
    quorumListLabels = new QLabel[numberOfLists] ();
    quorumListDisplays = new QTextBrowser[numberOfLists] ();
    quorumListTesters = new QPushButton[numberOfLists] ();

    for(int i=0; i<numberOfLists; ++i) {
        this->quorumLists[i] = quorumLists[i];
        quorumListLabels[i].setText(operations[i]);
        quorumListDisplays[i].setMinimumHeight(150);
        quorumListTesters[i].setText(tr("Verify"));
        auto expectedIt = votingStructure->getExpectedQuorumMap().find(operations[i]);
        quorumListTesters[i].setEnabled(expectedIt != votingStructure->getExpectedQuorumMap().end());
        QObject::connect(quorumListTesters + i, &QPushButton::clicked, this, [=](){ this->verify(operations[i]); });
        ui->listWidgetLayout->addWidget(quorumListLabels + i);
        ui->listWidgetLayout->addWidget(quorumListDisplays + i);
        ui->listWidgetLayout->addWidget(quorumListTesters + i);

        if(quorumLists[i]->buckets() > 1)
            ui->optionDisplayLine->setChecked(true);
    }
    setQuorumStrings();
}

void QuorumListDialog::closeEvent(QCloseEvent *event) {
    QDialog::closeEvent(event);
    if(event->isAccepted())
        emit closed();
}

QString QuorumListDialog::quorumListToString(std::shared_ptr<QuorumList> quorumList) {
    return QuorumListWriter::writeQuorumList(quorumList, nodes, ui->optionDisplayBinary->isChecked(), ui->optionDisplayLine->isChecked(), ui->optionDisplayMinimal->isChecked());
}

void QuorumListDialog::verify(const QString &op) {
    int index = -1;
    for(int i=0; i<numberOfLists; ++i) {
        if(quorumListLabels[i].text() == op) {
            index = i;
            break;
        }
    }
    if(index < 0) {
        QMessageBox::warning(this, tr("Error"), tr("Unexpected error occured."));
        return;
    }

    auto quorumMap = votingStructure->getExpectedQuorumMap();
    auto expected = quorumMap.find(op);
    if(expected == votingStructure->getExpectedQuorumMap().end()) {
        QMessageBox::warning(this, tr("Error"), tr("Cannot verify, because the expected quorum list has been deleted."));
        return;
    }

    auto result = QuorumListComparator::compare(quorumLists[index], expected->second, nodes, votingStructure->verifySimpleSet);
    if(result.euqal()) {
        QMessageBox::information(this, tr("Verification succeeded"), tr("The calculated quorum list matches the expected one for operation ") + op + ".");
        return;
    }
    QMessageBox::warning(this, tr("Verification failed"), result.errors());
}

void QuorumListDialog::cleanup() {
    ui->duration->setText("");
    for(int i=0; i<numberOfLists; ++i) {
        ui->listWidgetLayout->removeWidget(quorumListLabels + i);
        ui->listWidgetLayout->removeWidget(quorumListDisplays + i);
        ui->listWidgetLayout->removeWidget(quorumListTesters + i);
    }
    if(numberOfLists) {
        delete [] quorumLists;
        delete [] quorumListLabels;
        delete [] quorumListDisplays;
        delete [] quorumListTesters;
    }
    nodes.clear();
    numberOfLists = 0;
}

void QuorumListDialog::setQuorumStrings() {
    for(int i=0; i<numberOfLists; ++i)
        quorumListDisplays[i].setText(quorumListToString(quorumLists[i]));
}

void QuorumListDialog::on_optionDisplayLine_clicked() {
    setQuorumStrings();
}

void QuorumListDialog::on_optionDisplayCompact_clicked() {
    setQuorumStrings();
}

void QuorumListDialog::on_optionDisplaySet_clicked() {
    setQuorumStrings();
}

void QuorumListDialog::on_optionDisplayBinary_clicked() {
    setQuorumStrings();
}

void QuorumListDialog::on_optionDisplayMinimal_clicked() {
    setQuorumStrings();
}

void QuorumListDialog::on_optionDisplayComplete_clicked() {
    setQuorumStrings();
}
