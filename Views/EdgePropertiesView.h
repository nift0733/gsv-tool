#ifndef EDGEPROPERTIESVIEW_H
#define EDGEPROPERTIESVIEW_H

#include <QWidget>
#include <QTableWidgetItem>
#include <memory>

#include "../Models/Edge.h"

namespace Ui {
class EdgePropertiesView;
}

class EdgePropertiesView : public QWidget
{
    Q_OBJECT

public:
    explicit EdgePropertiesView(QWidget *parent = nullptr);
    ~EdgePropertiesView();

    /**
     * @brief Initializes the view.
     */
    void initView(std::shared_ptr<Edge> edge, QStringList operations);

signals:
    void editDone();

private slots:
    void on_penaltiesEdit_itemChanged(QTableWidgetItem *item);

private:
    Ui::EdgePropertiesView *ui;

    bool isInInit;
    std::shared_ptr<Edge> edge;

    /**
     * @brief Signals a change of penalties.
     */
    void emitEditDone();
};

#endif // EDGEPROPERTIESVIEW_H
