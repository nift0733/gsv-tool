#ifndef AVAILABILITYDIALOG_H
#define AVAILABILITYDIALOG_H

#include <QDialog>
#include <QtCharts>
#include <functional>

#include "../Algorithm/AvailabilityCostCalculator.h"
#include "../Models/VotingStructure.h"
#include "../Models/Topology.h"

namespace Ui {
class AnalysisDialog;
}

class AnalysisDialog : public QDialog
{
    Q_OBJECT

public:
    enum Type {
        Availability,
        Cost
    };

    explicit AnalysisDialog(QWidget *parent = nullptr, Type type = Availability);
    ~AnalysisDialog();

    /**
     * @brief Fills the data for the voting structure.
     */
    void fillData(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology);

signals:
    void closed();

protected:
    void closeEvent(QCloseEvent *) override;

private slots:
    void on_resetCmd_clicked();

    void on_exportCmd_clicked();

private:
    Ui::AnalysisDialog *ui;
    std::vector<QString> tabs;
    std::function<double(AvailabilityCostCalculator::Result)> selector;

    /**
     * @brief Cleans up the dialog.
     */
    void cleanup();
    /**
     * @brief Adds or updates the tab named name for operation op with the given values.
     */
    void addOrUpdateTab(QString name, QString op, std::vector<double> vars, std::vector<double> availabilities);
};

#endif // AVAILABILITYDIALOG_H
