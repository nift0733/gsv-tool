#ifndef NEWVOTINGSTRUCTUREDIALOG_H
#define NEWVOTINGSTRUCTUREDIALOG_H

#include <QDialog>

namespace Ui {
class NewVotingStructureDialog;
}

class NewVotingStructureDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewVotingStructureDialog(QWidget *parent = nullptr);
    ~NewVotingStructureDialog();

    /**
     * @brief Shows the dialog and returns if it was accepted.
     */
    bool show(std::vector<QString> &operationResults);

private:
    Ui::NewVotingStructureDialog *ui;
};

#endif // NEWVOTINGSTRUCTUREDIALOG_H
