#ifndef QUORUMLISTCALCULATOR_H
#define QUORUMLISTCALCULATOR_H

#include "../Models/QuorumList.h"
#include "../Models/Topology.h"
#include "../Models/VotingStructure.h"

class QuorumListCalculator
{
public:
    /**
     * @brief Calculates the quorum lists for all operations of the given voting structure and topology.
     */
    static std::vector<std::shared_ptr<QuorumList>> calculate(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology);

private:
    class Context {
    public:
        Context(std::shared_ptr<Topology> topology, std::shared_ptr<VotingStructure> votingStructure, int quorumSize, int opIndex)
            : topology(topology), votingStructure(votingStructure), quorumSize(quorumSize), opIndex(opIndex), currentPath(), currentPenalty() {}
        std::shared_ptr<Topology> topology;
        std::shared_ptr<VotingStructure> votingStructure;
        int quorumSize;
        int opIndex;
        std::vector<std::shared_ptr<Node>> currentPath;
        std::vector<std::shared_ptr<Edge>> currentPenalty;
    };

    /**
     * @brief Calculates the cooperation sets for the given nodes and current context.
     * Represents the implementation of the formal algorithm for cooperation sets.
     */
    static std::vector<std::pair<uint64_t*, std::vector<std::vector<std::shared_ptr<Edge>>>>> cooperationSets(std::shared_ptr<Node> node, Context &ctx);

    /**
     * @brief Removes all edges that points to a node on the currentPath.
     */
    static void removeCycles(std::vector<std::shared_ptr<Edge>> &edges, const std::vector<std::shared_ptr<Node>> &currentPath);

    /**
     * @brief Determines if the nodes pointed to by the selection of edges represented by perm is min-minimal.
     */
    static bool isMinimal(std::vector<std::shared_ptr<Edge>> edges, uint64_t* perm, int min);

    /**
     * @brief Merges the results of the recursive calls to cooperationSets (cf. line 11 in algorithm in Figure 4.8 of the thesis).
     */
    static void merge(std::vector<std::pair<uint64_t*, std::vector<std::vector<std::shared_ptr<Edge>>>>> &result, const std::vector<std::vector<std::pair<uint64_t*, std::vector<std::vector<std::shared_ptr<Edge>>>>>> &edgeResults, uint64_t *perm, std::shared_ptr<Node> node, const Context &ctx);

    /**
     * @brief Merges two single quorums by set union and the corresponding priority lists.
     */
    static std::pair<uint64_t*, std::vector<std::vector<std::shared_ptr<Edge>>>> mergeSingle(const std::pair<uint64_t*, std::vector<std::vector<std::shared_ptr<Edge>>>> &fst, const std::pair<uint64_t*, std::vector<std::vector<std::shared_ptr<Edge>>>> &snd, Context ctx);

    /**
     * @brief Transforms penalties represented by list of list of edges (tree of edges) to the representation as array of integers (ordering tuple).
     */
    static uint32_t* transformPenalty(std::vector<std::vector<std::shared_ptr<Edge>>> penalty, const Context &ctx);
};

#endif // QUORUMLISTCALCULATOR_H
