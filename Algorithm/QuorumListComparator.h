#ifndef QUORUMLISTCOMPARATOR_H
#define QUORUMLISTCOMPARATOR_H

#include <QString>
#include <memory>

#include "../Models/QuorumList.h"

class QuorumListComparator
{
public:
    class Result {
    public:
        bool euqal() {return isEqual;}
        QString errors() {return errorList;}
    private:
        friend QuorumListComparator;
        bool isEqual = true;
        QString errorList = "";

        void addError(QString err) {
            if(!isEqual)
                errorList += "\r\n";
            errorList += err;
            isEqual = false;
        }

        Result& operator+=(const Result& rhs) {
            if(!rhs.isEqual)
                this->addError(rhs.errorList);
            this->isEqual &= rhs.isEqual;
            return *this;
        }
    };

    /**
     * @brief Compares two quorumLists and returns if they are equal as well as the differences with the node names in nodes if not.
     * If verifySimpleSet is true, the comparision compares only the equivalency of the quorum sets represented by the lists.
     */
    static Result compare(std::shared_ptr<QuorumList> calculated, std::shared_ptr<QuorumList> expected, std::vector<QString> nodes, bool verifySimpleSet);

private:
    /**
     * @brief Do the comparison in list semantics, so that each two buckets have to be equal.
     */
    static Result compareList(std::shared_ptr<QuorumList> calculated, std::shared_ptr<QuorumList> expected, std::vector<QString> nodes);
    /**
     * @brief Do the comparison in set semantics.
     */
    static Result compareSimpleSet(std::shared_ptr<QuorumList> calculated, std::shared_ptr<QuorumList> expected, std::vector<QString> nodes);

    /**
     * @brief Compares the buckets at index bucket to be equal for calculated and expected.
     */
    static Result compareBucket(int bucket, std::shared_ptr<QuorumList> calculated, std::shared_ptr<QuorumList> expected, std::vector<QString> nodes);
    /**
     * @brief Finds unexpected quorums in the bucket of calculated at index bucket.
     */
    static Result findUnexpectedQuorums(int bucket, std::shared_ptr<QuorumList> calculated, std::shared_ptr<QuorumList> expected, std::vector<QString> nodes);
    /**
     * @brief Finds missing quorums in the bucket of calculated at index bucket
     */
    static Result findMissingQuorums(int bucket, std::shared_ptr<QuorumList> calculated, std::shared_ptr<QuorumList> expected, std::vector<QString> nodes);
    /**
     * @brief Determines if the quorum q1 and q2 are equal
     */
    static bool quorumEqual(uint64_t* q1, uint64_t* q2, int qSize);
};

#endif // QUORUMLISTCOMPARATOR_H
