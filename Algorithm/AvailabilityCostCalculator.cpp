#include "AvailabilityCostCalculator.h"

#include "../Algorithm/QuorumUtil.h"

AvailabilityCostCalculator::Result AvailabilityCostCalculator::calc(std::shared_ptr<QuorumList> quorumList, std::shared_ptr<Topology> topology) {
    auto nodeAvailabilities = calcNodeCriteria(topology);
    auto result = calc(quorumList, nodeAvailabilities, topology->getNumberOfNodes());
    delete[] nodeAvailabilities;
    return result;
}

std::vector<AvailabilityCostCalculator::Result> AvailabilityCostCalculator::calcRange(QString var, std::vector<double> range, std::shared_ptr<QuorumList> quorumList, std::shared_ptr<Topology> topology) {
    std::vector<Result> results;
    for(auto value : range) {
        topology->setVariable(var, value);
        results.push_back(calc(quorumList, topology));
    }
    return results;
}

AvailabilityCostCalculator::Result* AvailabilityCostCalculator::calcNodeCriteria(std::shared_ptr<Topology> topology) {
    Result* results = new Result[topology->getNumberOfNodes()];
    int i = 0;
    for(auto node : topology->getNodes()) {
        results[i].availability = topology->getAvailability(node);
        results[i++].cost = topology->getCost(node);
    }
    return results;
}

AvailabilityCostCalculator::Result AvailabilityCostCalculator::calc(std::shared_ptr<QuorumList> quorumList, Result* nodeCriteria, int countOfNodes) {
    Result result;
    result.availability = 0.0;
    result.cost = 0.0;
    auto quorumSize = (countOfNodes + (QUORUM_BITS - 1)) / QUORUM_BITS;
    uint64_t* maxPerm = new uint64_t[quorumSize] ();
    uint64_t* perm = new uint64_t[quorumSize] ();
    QuorumUtil::calcMaxPerm(maxPerm, countOfNodes);
    do {
        for(int bucket=0; bucket<quorumList->buckets(); ++bucket) {
            auto found = false;
            uint64_t* foundQuorum = perm;
            for(int index=0; index<quorumList->bucketSize(bucket); ++index) {
                uint64_t* quorum = quorumList->at(bucket, index);
                bool isPermSuperQuorum;
                bool isFoundQuorumSuperQuorum;
                QUORUM_IS_SUBSET(isPermSuperQuorum, quorum, perm, quorumSize);
                QUORUM_IS_SUBSET(isFoundQuorumSuperQuorum, quorum, perm, quorumSize);
                if(isPermSuperQuorum && isFoundQuorumSuperQuorum) {
                    foundQuorum = quorum;
                    found = true;
                    break;
                }
            }
            if(found) {
                result += calcForQuorum(foundQuorum, perm, nodeCriteria, countOfNodes);
                break;
            }
        }
    } while(QuorumUtil::nextPerm(perm, maxPerm, quorumSize));
    result.cost /= result.availability;
    return result;
}

AvailabilityCostCalculator::Result AvailabilityCostCalculator::calcForQuorum(uint64_t* quorum, uint64_t* perm, Result* nodeCriteria, int countOfNodes) {
    Result result;
    result.availability = 1.0;
    result.cost = 0.0;
    for(int i = 0; i < countOfNodes; ++i) {
        int8_t isSet = (perm[i / QUORUM_BITS] >> (i % QUORUM_BITS)) & 1;
        // if node i is set -> nodeAvailability
        // else -> 1 - nodeAvailability
        result.availability *= isSet * nodeCriteria[i].availability + (1-isSet) * (1 - nodeCriteria[i].availability);
        result.cost += ((quorum[i / QUORUM_BITS] >> (i % QUORUM_BITS)) & 1) * nodeCriteria[i].cost;
    }
    return result;
}
