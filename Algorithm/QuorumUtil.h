#ifndef QUORUMUTIL_H
#define QUORUMUTIL_H

#include <inttypes.h>

#define QUORUM_IS_SUBSET(O,I1,I2,QS) O = true; for(int __q_sub=0; __q_sub<QS; ++__q_sub) O &= ((I1[__q_sub] & I2[__q_sub]) == I1[__q_sub])

class QuorumUtil
{
public:
    /**
     * @brief Calculates the maximum uint64_t*-representation of a quorum of length count.
     */
    static void calcMaxPerm(uint64_t* maxPerm, int count);

    /**
     * @brief Increments perm to the next quorum and returns false if maxPerm is exceeded.
     */
    static bool nextPerm(uint64_t* perm, uint64_t* maxPerm, int quorumSize);
};

#endif // QUORUMUTIL_H
