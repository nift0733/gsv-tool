#ifndef AVAILABILITYCALCULATOR_H
#define AVAILABILITYCALCULATOR_H

#include <map>
#include <memory>

#include "../Models/QuorumList.h"
#include "../Models/Topology.h"

class AvailabilityCostCalculator
{
public:
    struct Result {
        double availability;
        double cost;

        Result& operator+=(const Result& rhs) {
            this->availability += rhs.availability;
            this->cost += rhs.cost * rhs.availability;
            return *this;
        }
    };

    /**
     * @brief Calculates the availability and cost for the given quorumList and topology.
     */
    static Result calc(std::shared_ptr<QuorumList> quorumList, std::shared_ptr<Topology> topology);

    /**
     * @brief Calculates the availability and cost for the given quorumList and topology for the specified values of the single availability in range assigned to variable var.
     */
    static std::vector<Result> calcRange(QString var, std::vector<double> range, std::shared_ptr<QuorumList> quorumList, std::shared_ptr<Topology> topology);

private:
    /**
     * @brief Stores the availabilities and costs of the nodes in topology in an array of type Result, so it can be used for further calculations.
     */
    static inline Result* calcNodeCriteria(std::shared_ptr<Topology> topology);

    /**
     * @brief Calculates the availability and cost for the given quorumList and the availabilites and costs stored in the ordered nodeCriteria of length countOfNodes.
     * The 0-th element in nodeCriteria is used as availability and cost of the 0-th bit of the quorums in quorumList.
     */
    static inline Result calc(std::shared_ptr<QuorumList> quorumList, Result* nodeCriteria, int countOfNodes);

    /**
     * @brief Calculates the availability of perm with given availabilites and costs in the ordered nodeCriteria of length countOfNodes as well as the costs of perm with minimal quorum quorum.
     * The 0-th element in nodeCriteria is used as availability and cost of the 0-th bit of quorum as well as perm, quorum is the minimal quorum of perm.
     */
    static inline Result calcForQuorum(uint64_t* quorum, uint64_t* perm, Result* nodeCriteria, int countOfNodes);
};

#endif // AVAILABILITYCALCULATOR_H
