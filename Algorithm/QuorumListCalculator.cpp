#include "QuorumListCalculator.h"

#include <QStringList>

#include "../Algorithm/QuorumUtil.h"

#define QUORUM_SET(IO,INDEX) IO[INDEX/QUORUM_BITS] |= ((uint64_t)1 << (INDEX%QUORUM_BITS))
#define QUORUM_IS_SET(I,INDEX) (I[INDEX/QUORUM_BITS] & ((uint64_t)1 << (INDEX%QUORUM_BITS)))
#define QUORUM_LIST_FOR(LIST,BUCKET,INDEX) for(int BUCKET=0,INDEX=0; BUCKET<LIST->buckets(); BUCKET += (++INDEX %= LIST->bucketSize(BUCKET)) ? 0 : 1)


std::vector<std::shared_ptr<QuorumList>> QuorumListCalculator::calculate(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology) {
    std::vector<std::shared_ptr<QuorumList>> result;
    for(auto i=0; i < votingStructure->getNumberOfOperations(); ++i) {
        auto opResult = std::make_shared<QuorumList>(topology->getNumberOfNodes());
        Context ctx(topology, votingStructure, opResult->getQuorumSize(), i);
        auto cooperations = cooperationSets(votingStructure->getRoot(), ctx);
        for(auto &&pair : cooperations) {
            auto penalty = transformPenalty(pair.second, ctx);
            opResult->addQuorum(pair.first, penalty);
            delete [] pair.first;
            delete [] penalty;
        }
        result.push_back(opResult);
    }

    return result;
}

std::vector<std::pair<uint64_t*, std::vector<std::vector<std::shared_ptr<Edge>>>>> QuorumListCalculator::cooperationSets(std::shared_ptr<Node> node, Context &ctx) {
    std::vector<std::pair<uint64_t*, std::vector<std::vector<std::shared_ptr<Edge>>>>> result;
    if(ctx.votingStructure->isPartialLeaf(node, ctx.currentPath)) {
        if(node->vote > 0 && !node->isVirtual) {
            auto index = ctx.topology->getNodeIndex(node->identifier);
            auto quorum = new uint64_t[ctx.quorumSize] {};
            QUORUM_SET(quorum, index);
            std::vector<std::vector<std::shared_ptr<Edge>>> penalties;
            for(auto &&edge : ctx.currentPenalty) {
                if(edge->penaltyList[ctx.opIndex] < 0)
                    continue;
                penalties.push_back(std::vector<std::shared_ptr<Edge>>());
                penalties.back().push_back(edge);
            }
            result.push_back(std::make_pair(quorum, penalties));
        }
    } else {
        // recursion step for all edges (not producing a cycle)
        auto edges = ctx.votingStructure->getEdgesOrderedByPenalty(node, ctx.opIndex);
        removeCycles(edges, ctx.currentPath);
        std::vector<std::vector<std::pair<uint64_t*, std::vector<std::vector<std::shared_ptr<Edge>>>>>> edgeResults;
        ctx.currentPath.push_back(node);
        for(auto &&edge : edges) {
            ctx.currentPenalty.push_back(edge);
            edgeResults.push_back(cooperationSets(edge->to, ctx));
            ctx.currentPenalty.pop_back();
        }
        ctx.currentPath.pop_back();

        // merge step for recursion results
        if(edgeResults.size() > 0) {
            const auto qSize = (edgeResults.size() + QUORUM_BITS - 1) / QUORUM_BITS;
            auto maxPerm = new uint64_t[qSize] {};
            auto perm = new uint64_t[qSize] {};
            QuorumUtil::calcMaxPerm(maxPerm, edgeResults.size());
            while(QuorumUtil::nextPerm(perm, maxPerm, qSize)) {
                if(!isMinimal(edges, perm, node->quorumVector[ctx.opIndex]))
                    continue;
                merge(result, edgeResults, perm, node, ctx);
            }
            delete [] maxPerm;
            delete [] perm;
            for(auto &&er : edgeResults) {
                for(auto &&e : er) {
                    delete [] e.first;
                }
            }
        }
    }
    return result;
}

void QuorumListCalculator::removeCycles(std::vector<std::shared_ptr<Edge> > &edges, const std::vector<std::shared_ptr<Node> > &currentPath) {
    for(auto &&node : currentPath) {
        auto it = std::find_if(edges.begin(), edges.end(), [&node](std::shared_ptr<Edge> edge) { return edge->to == node; });
        if(it != edges.end())
            edges.erase(it);
    }
}

bool QuorumListCalculator::isMinimal(std::vector<std::shared_ptr<Edge> > edges, uint64_t *perm, int min) {
    auto sum = 0;
    for(size_t i=0; i<edges.size(); ++i) {
        if(QUORUM_IS_SET(perm, i))
            sum += edges[i]->to->vote;
    }
    if(sum < min)
        return false;

    for(size_t i=0; i<edges.size(); ++i) {
        if(QUORUM_IS_SET(perm, i) && sum - edges[i]->to->vote >= min)
            return false;
    }
    return true;
}

void QuorumListCalculator::merge(std::vector<std::pair<uint64_t *, std::vector<std::vector<std::shared_ptr<Edge>>> > > &result, const std::vector<std::vector<std::pair<uint64_t *, std::vector<std::vector<std::shared_ptr<Edge>>> > > > &edgeResults, uint64_t *perm, std::shared_ptr<Node> node, const Context &ctx) {
    auto intermediateResults = std::vector<std::pair<uint64_t *, std::vector<std::vector<std::shared_ptr<Edge>>>>>();
    auto initQuorum = new uint64_t[ctx.quorumSize] {};
    intermediateResults.push_back(std::make_pair(initQuorum, std::vector<std::vector<std::shared_ptr<Edge>>>()));

    for(size_t i=0; i<edgeResults.size(); ++i) {
        if(!QUORUM_IS_SET(perm, i))
            continue;
        auto oldIntermediateSize = intermediateResults.size();
        for(size_t j=0; j<oldIntermediateSize; ++j) {
            for(auto &&pair : edgeResults[i]) {
                intermediateResults.push_back(mergeSingle(intermediateResults[j], pair, ctx));
            }
        }
        for(auto it=intermediateResults.begin(); it<intermediateResults.begin()+oldIntermediateSize; ++it) {
            delete [] it->first;
        }
        intermediateResults.erase(intermediateResults.begin(), intermediateResults.begin() + oldIntermediateSize);
    }

    for(auto it=intermediateResults.begin(); it<intermediateResults.end(); ++it) {
        if(!node->isVirtual)
            QUORUM_SET(it->first, ctx.topology->getNodeIndex(node->identifier));
        result.push_back(*it);
    }
}

std::pair<uint64_t*, std::vector<std::vector<std::shared_ptr<Edge>>>> QuorumListCalculator::mergeSingle(const std::pair<uint64_t*, std::vector<std::vector<std::shared_ptr<Edge>>>> & fst, const std::pair<uint64_t*, std::vector<std::vector<std::shared_ptr<Edge>>>> &snd, Context ctx) {
    auto quorum = new uint64_t[ctx.quorumSize] {};
    for(int i=0; i<ctx.quorumSize; ++i)
        quorum[i] = fst.first[i] | snd.first[i];

    auto max = std::max(fst.second.size(), snd.second.size());
    std::vector<std::vector<std::shared_ptr<Edge>>> penalty;
    for(size_t i=0; i<max; ++i) {
        penalty.push_back(std::vector<std::shared_ptr<Edge>>());
        if(i < fst.second.size()) {
            for(auto &&edge : fst.second[i])
                if(std::find(penalty.back().begin(), penalty.back().end(), edge) == penalty.back().end())
                    penalty.back().push_back(edge);
        }
        if(i < snd.second.size()) {
            for(auto &&edge : snd.second[i])
                if(std::find(penalty.back().begin(), penalty.back().end(), edge) == penalty.back().end())
                    penalty.back().push_back(edge);
        }
    }

    return std::make_pair(quorum, penalty);
}

uint32_t* QuorumListCalculator::transformPenalty(std::vector<std::vector<std::shared_ptr<Edge> > > penalty, const Context &ctx) {
    auto result = new uint32_t[penalty.size() + 1];
    result[0] = penalty.size();

    auto index=0;
    for(auto &&layer : penalty) {
        auto sum = 0;
        for(auto &&edge : layer) {
            sum += edge->penaltyList[ctx.opIndex];
            if(sum >= MAX_PENALTY) {
                sum = MAX_PENALTY;
                break;
            }
        }
        result[++index] = sum;
    }

    return result;
}
