#include "QuorumListComparator.h"

#include "../Persistence/QuorumListWriter.h"
#include "../Algorithm/QuorumUtil.h"

QuorumListComparator::Result QuorumListComparator::compare(std::shared_ptr<QuorumList> calculated, std::shared_ptr<QuorumList> expected, std::vector<QString> nodes, bool verifySimpleSet) {
    if(verifySimpleSet)
        return compareSimpleSet(calculated, expected, nodes);
    else
        return compareList(calculated, expected, nodes);
}

QuorumListComparator::Result QuorumListComparator::compareSimpleSet(std::shared_ptr<QuorumList> calculated, std::shared_ptr<QuorumList> expected, std::vector<QString> nodes) {
    Result result;
    if(expected->buckets() != 1) {
        result.addError("The expected quorum list must contain exactly one set of quorums.");
        return result;
    }

    auto quorumSize = calculated->getQuorumSize();
    for(int bucket=0; bucket<calculated->buckets(); ++bucket) {
        for(int i=0; i<calculated->bucketSize(bucket); ++i) {
            auto quorum = calculated->at(bucket, i);
            auto found = false;
            for(int j=0; j<expected->bucketSize(0); ++j) {
                auto expectedQuorum = expected->at(0, j);
                bool isSubSet = false;
                QUORUM_IS_SUBSET(isSubSet, expectedQuorum, quorum, quorumSize);
                if(isSubSet) {
                    found = true;
                    break;
                }
            }
            if(!found)
                result.addError("Found unexpected " + QuorumListWriter::quorumToSetString(quorum, nodes) + ".");
        }
    }

    for(int j=0; j<expected->bucketSize(0); ++j) {
        auto expectedQuorum = expected->at(0, j);
        auto found = false;
        for(int bucket=0; bucket<calculated->buckets(); ++bucket) {
            for(int i=0; i<calculated->bucketSize(bucket); ++i) {
                auto quorum = calculated->at(bucket, i);
                if(quorumEqual(quorum, expectedQuorum, quorumSize)) {
                    found = true;
                    break;
                }
            }
            if(found)
                break;
        }
        if(!found)
            result.addError("Missing " + QuorumListWriter::quorumToSetString(expectedQuorum, nodes) + ".");
    }

    return result;
}

QuorumListComparator::Result QuorumListComparator::compareList(std::shared_ptr<QuorumList> calculated, std::shared_ptr<QuorumList> expected, std::vector<QString> nodes) {
    Result result;
    if(calculated->buckets() < expected->buckets()) {
        result.addError("The quorum list has too few quorum sets.");
        return result;
    }
    if(calculated->buckets() > expected->buckets()) {
        result.addError("The quorum list has too many quorum sets.");
        return result;
    }

    for(int i=0; i<calculated->buckets(); ++i) {
        result += compareBucket(i, calculated, expected, nodes);
    }
    return result;
}

QuorumListComparator::Result QuorumListComparator::compareBucket(int bucket, std::shared_ptr<QuorumList> calculated, std::shared_ptr<QuorumList> expected, std::vector<QString> nodes) {
    Result result;
    auto prefix = "Quorum set " + QString::number(bucket) + ": ";
    if(calculated->bucketSize(bucket) < expected->bucketSize(bucket)) {
        result.addError(prefix + "Has too few quorums.");
    } else if(calculated->bucketSize(bucket) > expected->bucketSize(bucket)) {
        result.addError(prefix + "Has too many quorums.");
    }

    result += findUnexpectedQuorums(bucket, calculated, expected, nodes);
    result += findMissingQuorums(bucket, calculated, expected, nodes);
    return result;
}

QuorumListComparator::Result QuorumListComparator::findUnexpectedQuorums(int bucket, std::shared_ptr<QuorumList> calculated, std::shared_ptr<QuorumList> expected, std::vector<QString> nodes) {
    Result result;
    auto prefix = "Quorum set " + QString::number(bucket) + ": ";
    auto quorumSize = calculated->getQuorumSize();
    for(int i = 0; i < calculated->bucketSize(bucket); ++i) {
        auto quorum = calculated->at(bucket, i);
        bool found = false;
        for(int j = 0; j < expected->bucketSize(bucket); ++j) {
            auto expectedQuorum = expected->at(bucket, j);
            if(quorumEqual(quorum, expectedQuorum, quorumSize)) {
                found = true;
                break;
            }
        }
        if(!found)
            result.addError(prefix + "Found unexpected " + QuorumListWriter::quorumToSetString(quorum, nodes) + ".");
    }
    return result;
}

QuorumListComparator::Result QuorumListComparator::findMissingQuorums(int bucket, std::shared_ptr<QuorumList> calculated, std::shared_ptr<QuorumList> expected, std::vector<QString> nodes) {
    Result result;
    auto prefix = "Quorum set " + QString::number(bucket) + ": ";
    auto quorumSize = calculated->getQuorumSize();
    for(int i = 0; i < expected->bucketSize(bucket); ++i) {
        auto quorum = expected->at(bucket, i);
        bool found = false;
        for(int j = 0; j < calculated->bucketSize(bucket); ++j) {
            auto calculatedQuorum = calculated->at(bucket, j);
            if(quorumEqual(quorum, calculatedQuorum, quorumSize)) {
                found = true;
                break;
            }
        }
        if(!found)
            result.addError(prefix + "Missing " + QuorumListWriter::quorumToSetString(quorum, nodes) + ".");
    }
    return result;
}

bool QuorumListComparator::quorumEqual(uint64_t *q1, uint64_t *q2, int qSize) {
    for(int i = 0; i < qSize; ++i) {
        if(q1[i] != q2[i])
            return false;
    }
    return true;
}
