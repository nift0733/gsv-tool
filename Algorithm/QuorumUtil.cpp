#include "QuorumUtil.h"

#include "../Models/QuorumList.h"

#define QUORUM_BITS_64 0xFFFFFFFFFFFFFFFF
#define QUORUM_BITS_32 0xFFFFFFFF
#define QUORUM_BITS_16 0xFFFF
#define QUORUM_BITS_8 0xFF
#define QUORUM_BITS_4 0x0F
#define QUORUM_BITS_2 0x03
#define QUORUM_BITS_1 0x01

#define QUORUM_MAX_PERM_HELPER(X,Y) if(count >= X) { \
        maxPerm[i] |= (Y << shift); \
        shift += X; \
        count -= X; \
    }

void QuorumUtil::calcMaxPerm(uint64_t *maxPerm, int count) {
    int i = 0;
    while(count > QUORUM_BITS) {
        maxPerm[i] = QUORUM_BITS_64;
        ++i;
        count -= QUORUM_BITS;
    }
    int shift = 0;
    QUORUM_MAX_PERM_HELPER(32, QUORUM_BITS_32)
    QUORUM_MAX_PERM_HELPER(16, QUORUM_BITS_16)
    QUORUM_MAX_PERM_HELPER(8, QUORUM_BITS_8)
    QUORUM_MAX_PERM_HELPER(4, QUORUM_BITS_4)
    QUORUM_MAX_PERM_HELPER(2, QUORUM_BITS_2)
    QUORUM_MAX_PERM_HELPER(1, QUORUM_BITS_1)
}

bool QuorumUtil::nextPerm(uint64_t *perm, uint64_t *maxPerm, int quorumSize) {
    int i;
    for(i=0; true; ++i) {
        ++perm[i];
        if(perm[i])
            break;
    }
    return perm[quorumSize-1] <= maxPerm[quorumSize-1];
}
