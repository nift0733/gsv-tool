# gsv-tool

## How to start the program (Linux)

1. Download the latest artifacts in CI/CD (<https://gitlab.uni-oldenburg.de/wola7517/gsv-tool/pipelines>) and unzip the files.
2. Change to the unzipped folder gsv-tool on the command line.
3. Install necessary dependencies via `sudo ./install_deps.sh`.
4. Run the program via `./run.sh`.
5. Test the example files.

## Steps to code

1. Download and install version 5.12.4 of Qt open source (<https://www.qt.io/download>).
2. Install Qt creator (<https://www.qt.io/offline-installers>).
3. Use git to clone this repository.
4. Open the project in Qt creator and read the Qt documentation if necessary (<https://doc.qt.io/>).



