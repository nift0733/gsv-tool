#include <QtTest>

#include "../Algorithm/QuorumListCalculator.h"
#include "../Algorithm/QuorumListComparator.h"
#include "../Persistence/VotingStructureReader.h"

class TestStrategies : public QObject
{
public:
    TestStrategies();

private:
    Q_OBJECT

private slots:
    void testAll();
};

TestStrategies::TestStrategies() {
    std::string timeout("10800000"); // 3 Stunden Timeout
    QByteArray timeoutArray(timeout.c_str(), timeout.length());
    qputenv("QTEST_FUNCTION_TIMEOUT", timeoutArray);
}

void TestStrategies::testAll()
{
    QFile xsd(":/xsd/votingstructure.xsd");
    QFile metaXsd(":/xsd/votingstructure_meta.xsd");
    VotingStructurePersistenceBase::Init(xsd, metaXsd);

    QString outputPrefix = "\r\n----------\r\n";
    QDirIterator it(":/gsv");
    QString results = "";
    while(it.hasNext()) {
        it.next();
        auto gsv = VotingStructureReader::readFromFile(it.filePath());
        if(!gsv.first || !gsv.second || !gsv.first->getExpectedQuorumLists().size()) {
            results += outputPrefix + "ERROR loading '" + it.fileName() + "'.\r\n";
            continue;
        }
        if(gsv.second->getNumberOfNodes() > 20) {
            continue;
        }
        auto calculated = QuorumListCalculator::calculate(gsv.first, gsv.second);
        for(size_t i=0; i<calculated.size(); ++i) {
            auto result = QuorumListComparator::compare(calculated[i], gsv.first->getExpectedQuorumLists()[i], gsv.second->getNodes(), gsv.first->verifySimpleSet);
            if(!result.euqal())
                results += outputPrefix + "ERROR for " + gsv.first->getOperations()[i] + " on '" + it.fileName() + "':\r\n" + result.errors();
        }
    }
    QVERIFY2(results == "", results.append(outputPrefix).replace("\r", "").toLocal8Bit());
}

QTEST_APPLESS_MAIN(TestStrategies)

#include "tst_teststrategies.moc"
