#!/bin/bash

for entry in *.gsv
do
	folder="$(cut -d'.' -f1 <<< $entry)"
	unzip $entry -d $folder
	cd $folder
	ql=../quorumLists/${entry%".gsv"}.txt
	cp $ql verifyQuorumLists.txt
	zip -0 $entry -xi *
	cp $entry ..
	cd ..
	rm -R $folder
done
