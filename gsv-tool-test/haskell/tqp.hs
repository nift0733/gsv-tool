import QuorumSets
import QuorumHelp
import QuorumPersistance
import QuorumCalc

highestLayer :: Int -> Quorum -> Int
highestLayer m q = layerOf $ node $ minimum $ split q where
    layers = [1] : (map (\layer -> concat (map (\n -> [(n-1)*m + k + 1 | k<-[1..m]]) layer)) layers)
    layerOf n = length $ takeWhile (False ==) $ map (any (n <=)) layers

majority :: Int -> Int -> Quorum -> QuorumSet
majority m k q = map (\q' -> q' `addOffset` offset) $ m `chooseSet` k where
    offset = ((node q) - 1) * m + 1

majorityR :: Int -> Quorum -> QuorumSet
majorityR m q = majority m (m `divCeil` 2) q

majorityW :: Int -> Quorum -> QuorumSet
majorityW m q = majority m ((m+1) `divCeil` 2) q

tqpRead :: Int -> Int -> QuorumList
tqpRead m h = groupByLocalSize $ readSort (readHelp (quorum [1]) m h) where
    readSort qs = qSortBy (\q -> (highestLayer m q, size q)) qs
    readHelp :: Quorum -> Int -> Int -> QuorumSet
    readHelp q m 0 = [q]
    readHelp q m h = q : concat (map (\qs -> foldl (cross union) [0] (map (\q' -> readHelp q' m (h-1)) qs)) maj) where
        maj = map split (majorityR m q)

tqpWrite :: Int -> Int -> QuorumList
tqpWrite m h = [writeHelp (quorum [1]) m h] where
    writeHelp :: Quorum -> Int -> Int -> QuorumSet
    writeHelp q m 0 = [q]
    writeHelp q m h = concat (map (\qs -> foldl (cross union) [q] (map (\q' -> writeHelp q' m (h-1)) qs)) maj) where
        maj = map split (majorityW m q)

tqpExport :: [Int] -> [Int] -> IO[()]
tqpExport ms hs = mapM (\(m,h) -> exportList (name m h) False (tqpRead m h) (tqpWrite m h)) [(m,h) | m<-ms, h<-hs] where
    name i j = "tqp_" ++ show i ++ "_" ++ show j
