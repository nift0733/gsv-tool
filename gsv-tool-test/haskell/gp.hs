import QuorumSets
import QuorumPersistance
import QuorumCalc

mkColumns :: Int -> Int -> QuorumSet
mkColumns c r = map (\ck -> quorum [rk*c + ck | rk<-[0..r-1]]) [1..c]

mkCovers :: Int -> Int -> QuorumSet
mkCovers c r = foldl (cross union) [0] $ map split $ mkColumns c r

gpRead :: Int -> Int -> QuorumList
gpRead c r = [mkCovers c r]

gpoRead :: Int -> Int -> QuorumList
gpoRead c r = [(mkCovers c r) ++ (mkColumns c r)]

gpWrite :: Int -> Int -> QuorumList
gpWrite c r = [filter (\q -> q `containsAny` columns && q `containsAny` covers) $ chooseSet (c*r) (c+r-1)] where
    columns = mkColumns c r
    covers = mkCovers c r

gpoWrite = gpWrite

gpExport :: [Int] -> [Int] -> IO[()]
gpExport cs rs = mapM (\(c,r) -> exportList (name c r) False (gpRead c r) (gpWrite c r)) [(c,r) | c<-cs, r<-rs] where
    name i j = "gp_" ++ show i ++ "x" ++ show j

gpoExport :: [Int] -> [Int] -> IO[()]
gpoExport cs rs = mapM (\(c,r) -> exportList (name c r) False (gpoRead c r) (gpoWrite c r)) [(c,r) | c<-cs, r<-rs] where
    name i j = "gpo_" ++ show i ++ "x" ++ show j
