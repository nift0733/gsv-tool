module QuorumSets
( Quorum
, QuorumSet
, QuorumList
, showQuorumList
, node
, subset
, union
, unionIndex
, intersect
, minus
, split
, cross
, containsAny
, chooseSet
, quorum
, size
, addOffset
, removeSuperQuorums
) where

import Data.Bits

maxNodes = 62

type Quorum = Int
type QuorumSet = [Quorum]
type QuorumList = [QuorumSet]

showQuorum :: Quorum -> [Char]
showQuorum n = "{" ++ showHelp (filter (testBit n) [1..maxNodes]) ++ "}" where
    showHelp [] = []
    showHelp (i:[]) = "R" ++ show i
    showHelp (i:xs) = "R" ++ show i ++ "," ++ showHelp xs

showQuorumSet :: QuorumSet -> [Char]
showQuorumSet qs = "{ " ++ showHelp qs ++ " }" where
    showHelp [] = []
    showHelp (q:[]) = showQuorum q
    showHelp (q:qs) = showQuorum q ++ ", " ++ showHelp qs

showQuorumList :: QuorumList -> [Char]
showQuorumList ql = "[ " ++ showHelp ql ++ " ]" where
    showHelp [] = []
    showHelp (qs:[]) = showQuorumSet qs
    showHelp (qs:ql) = showQuorumSet qs ++ ", " ++ showHelp ql

node :: Quorum -> Int
node q
    | popCount q /= 1 = error "node cannot be called on quorum with multiple nodes"
    | otherwise = head $ filter (testBit q) [1..maxNodes]

subset :: Quorum -> Quorum -> Bool
subset q1 q2 = q1 `intersect` q2 == q1

union :: Quorum -> Quorum -> Quorum
union = (.|.)

unionIndex :: Quorum -> Int -> Quorum
unionIndex q k = q `union` (bit k)

intersect :: Quorum -> Quorum -> Quorum
intersect = (.&.)

minus :: Quorum -> Quorum -> Quorum
minus x y = x .&. (complement y)

split :: Quorum -> [Quorum]
split q = filter (0 /=) $ map (q .&.) $ map bit [1..maxNodes]

cross :: (Quorum -> Quorum -> Quorum) -> [Quorum] -> [Quorum] -> [Quorum]
cross op xs ys = [op x y | x<-xs, y<-ys]

containsAny :: Quorum -> QuorumSet -> Bool
containsAny q qs = any (\q' -> q' `subset` q) qs

chooseSet :: Int -> Int -> QuorumSet
chooseSet n 0 = [0]
chooseSet n k = concat $ map chooseSetHelp [1..n-k+1] where
    chooseSetHelp n' = map (\q -> q `union` (bit n')) $ map (\q -> shift q n') $ chooseSet (n-n') (k-1)

quorum :: [Int] -> Quorum
quorum = foldl unionIndex 0

size :: Quorum -> Int
size = popCount

addOffset :: Quorum -> Int -> Quorum
addOffset q n = q `shift` n

remove :: (a -> Bool) -> [a] -> [a]
remove p = filter $ not . p

removeSuperQuorums :: QuorumSet -> QuorumSet -> QuorumSet
removeSuperQuorums xs [] = xs
removeSuperQuorums xs (y:ys) = (remove (y `subset`) xs) `removeSuperQuorums` ys

