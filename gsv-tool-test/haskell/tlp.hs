import PathBased
import QuorumSets
import QuorumPersistance
import QuorumCalc

tlp :: Int -> Int -> Graph
tlp c r = graph c r (hPaths ++ vPaths ++ dPaths) where
    hPaths = map (\n -> [n, n+1]) $ filter (\n -> n `mod` c > 0) [1..c*r]
    vPaths = map (\n -> [n, n+c]) [1..c*(r-1)]
    dPaths = map (\n -> [n, n+c+1]) $ filter (\n -> n `mod` c > 0) [1..c*(r-1)]

tlpRead :: Int -> Int -> QuorumList
tlpRead c r = calcRead g g where
    g = tlp c r

tlpWrite :: Int -> Int -> QuorumList
tlpWrite c r = calcWrite g g where
    g = tlp c r

tlpExport :: [Int] -> [Int] -> IO[()]
tlpExport cs rs = mapM (\(c,r) -> exportList (name c r) True (tlpRead c r) (tlpWrite c r)) [(c,r) | c<-cs, r<-rs] where
    name i j = "tlp" ++ show i ++ "x" ++ show j
