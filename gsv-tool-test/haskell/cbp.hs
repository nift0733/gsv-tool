import PathBased
import QuorumSets
import QuorumPersistance
import QuorumCalc

cbpV :: Graph
cbpV = graph 3 3 [[1,4],[4,7],[2,5],[5,8],[3,6],[6,9],[4,5],[5,6],[2,6],[3,5],[5,7],[4,8]]

cbpH :: Graph
cbpH = graph 3 3 [[1,2],[2,3],[4,5],[5,6],[7,8],[8,9],[2,5],[5,8],[1,5],[2,4],[5,9],[6,8]]

cbpRead :: QuorumList
cbpRead = calcRead cbpV cbpH

cbpWrite :: QuorumList
cbpWrite = calcWrite cbpV cbpH

cbpExport :: IO()
cbpExport = exportList name True cbpRead cbpWrite where
    name = "cbp3x3"
