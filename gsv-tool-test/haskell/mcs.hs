import QuorumSets
import QuorumHelp
import QuorumPersistance
import QuorumCalc

mcsRead :: Int -> QuorumList
mcsRead n = [chooseSet n (n `divCeil` 2)]

mcsWrite :: Int -> QuorumList
mcsWrite n = [chooseSet n ((n+1) `divCeil` 2)]

mcsExport :: [Int] -> IO[()]
mcsExport = mapM (\n -> exportList (name n) False (mcsRead n) (mcsWrite n)) where
   name k = "mcs" ++ show k
