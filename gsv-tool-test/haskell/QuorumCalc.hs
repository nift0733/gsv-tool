module QuorumCalc
( availability,
  costs,
  availabilityR,
  costsR
) where

import QuorumSets
import QuorumHelp
import Text.Printf

showCoordinates :: [(Double,Double)] -> String
showCoordinates [] = []
showCoordinates (x:xs) = (show x) ++ (showCoordinates xs)

showResults :: [(Double,Double)] -> String
showResults [] = []
showResults ((x,y):[]) = (printf "%.3f" y)
showResults ((x,y):xs) = (printf "%.3f" y) ++ "," ++ (showResults xs)

availability n qs = showCoordinates $ availabilityH n qs
costs n qs = showCoordinates $ costsH n qs
availabilityR n qs = showResults $ availabilityH n qs
costsR n qs = showResults $ costsH n qs

availabilityH :: Int -> QuorumList -> [(Double, Double)]
availabilityH n qss = foldl merge [(p,0) | p<-pRange] [availabilityHelp q | q<-[minQ,(minQ+2)..maxQ]] where
    qs = flatten qss
    minQ = minimum qs
    maxQ = quorum [1..n]
    pRange = [j*0.01 | j<-[0..100]]
    availabilityHelp q = [(p, if hasSub then p^k * (1-p)^(n-k) else 0) | p<-pRange] where
        hasSub = any (\q' -> q' `subset` q) qs
        k = size q
    merge [] [] = []
    merge ((p,a):rs) ((p',a'):rs') = if p /= p' then [] else (p,a+a') : (merge rs rs')

costsH :: Int -> QuorumList -> [(Double,Double)]
costsH n qss = map closing $ foldl merge [(p,0,0) | p<-pRange] [costsHelp q qss | q<-[minQ,(minQ+2)..maxQ]] where
    minQ = minimum $ map minimum qss
    maxQ = quorum [1..n]
    pRange = [j*0.01 | j<-[1..100]]
    costsHelp _ [] = [(p,0,0) | p<-pRange]
    costsHelp q (qs:qss) = if hasSubs then [(p, fromIntegral(minimum subs), p^k * (1-p)^(n-k)) | p<-pRange] else costsHelp q qss where
        subs = map size $ filter (\q' -> q' `subset` q) qs
        hasSubs = length subs > 0
        k = size q
    merge [] [] = []
    merge ((p,c,a):rs) ((p',c',a'):rs') = if p /= p' then [] else (p,c+c'*a',a+a') : (merge rs rs')
    closing (p,c,a) = (p,c/a)
