import QuorumSets
import QuorumPersistance
import QuorumCalc

rowaRead :: Int -> QuorumList
rowaRead n = [chooseSet n 1]

rowaWrite :: Int -> QuorumList
rowaWrite n = [chooseSet n n]

rowaExport :: [Int] -> IO[()]
rowaExport = mapM (\n -> exportList (name n) False (rowaRead n) (rowaWrite n)) where
   name k = "rowa" ++ show k
