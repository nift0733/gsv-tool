module QuorumHelp
( divCeil
, qSortBy
, sortBySize
, groupByLocalSize
, flatten
) where

import QuorumSets

divCeil :: Int -> Int -> Int
n `divCeil` m
    | n `mod` m == 0 = n `div` m
    | otherwise = (n `div` m) + 1

qSortBy :: (Ord a) => (b -> a) -> [b] -> [b]
qSortBy _ [] = []
qSortBy by (x:xs) = qSortBy by [y|y<-xs, by y < by x] ++ [x] ++ qSortBy by [y|y<-xs, by y >= by x]

sortBySize :: QuorumSet -> QuorumSet
sortBySize = qSortBy size

groupByLocalSize :: QuorumSet -> QuorumList
groupByLocalSize = groupHelp 0 [] where
    groupHelp _ qs' [] = [qs']
    groupHelp l qs' (q:qs)
        | size q == l = groupHelp l (qs' ++ [q]) qs
        | qs' == [] = groupHelp (size q) [q] qs
        | otherwise = qs' : groupHelp (size q) [q] qs

flatten :: [[a]] -> [a]
flatten [] = []
flatten (xs:xss) = xs ++ (flatten xss)
