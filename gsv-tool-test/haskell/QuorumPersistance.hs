module QuorumPersistance
( exportList
) where

import QuorumSets

exportList :: [Char] -> Bool -> QuorumList -> QuorumList -> IO()
exportList name verifySimpleSet rql wql = writeFile (name ++ ".txt") content where
     content = prefix ++ "read: " ++ (showQuorumList rql) ++ "\r\nwrite: " ++ (showQuorumList wql) where
          prefix = if verifySimpleSet then "verifySimpleSet:true;\r\n" else ""
