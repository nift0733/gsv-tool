module PathBased
( Graph
, graph
, calcRead
, calcWrite
) where

import QuorumSets
import QuorumHelp
import QuorumPersistance

type Graph = (Int,Int,[Quorum])

hStart :: Int -> Int -> Quorum
hStart c r = quorum [1+c*ir | ir<-[0..r-1]]
hEnd :: Int -> Int -> Quorum
hEnd c r = quorum [c*ir | ir<-[1..r]]
vStart :: Int -> Int -> Quorum
vStart c r = quorum [1..c]
vEnd :: Int -> Int -> Quorum
vEnd c r = quorum [ic + (r-1)*c | ic<-[1..c]]

successors :: [Quorum] -> Quorum -> Quorum
successors es q = foldl union 0 $ map (\e -> e `minus` q) $ filter (\e -> (e `intersect` q) /= 0) es

path :: [Quorum] -> Quorum -> Quorum -> Quorum -> Bool
path es start end q = pathRec (filter (\e -> e `subset` q) es) (start `intersect` q) (end `intersect` q) where
    pathRec :: [Int] -> Int -> Int -> Bool
    pathRec es start end
        | start == 0 || end == 0 = False
        | start `intersect` end /= 0 = True
        | start == next = False
        | otherwise = pathRec es next end where
            next = (successors es start) `union` start

vPath :: Graph -> Quorum -> Bool
vPath (c,r,es) q = path es (vStart c r) (vEnd c r) q

hPath :: Graph -> Quorum -> Bool
hPath (c,r,es) q = path es (hStart c r) (hEnd c r) q

calc :: (Quorum -> Bool) -> Int -> QuorumList
calc pred maxi = [tlpHelp 1 []] where
    tlpHelp k results
        | k >= maxi = []
        | otherwise = if newResults == [] then rec else newResults ++ rec where
            rec = tlpHelp (k+1) (results ++ newResults)
            newResults = filter pred $ ((maxi) `chooseSet` k) `removeSuperQuorums` results

graph :: Int -> Int -> [[Int]] -> Graph
graph c r es = (c, r, map quorum es)

calcRead :: Graph -> Graph -> QuorumList
calcRead gv@(c,r,_) gh@(c',r',_) = if c/=c' || r/=r' then error "graphs must have same dimensions" else calc (\q -> (vPath gv q) || (hPath gh q)) (c*r)

calcWrite :: Graph -> Graph -> QuorumList
calcWrite gv@(c,r,_) gh@(c',r',_) = if c/=c' || r/=r' then error "graphs must have same dimensions" else calc (\q -> (vPath gv q) && (hPath gh q)) (c*r)
