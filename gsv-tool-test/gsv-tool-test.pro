QT += testlib xml xmlpatterns
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

DEFINES += TEST_STRATEGIES

TEMPLATE = app

SOURCES += \
    tst_teststrategies.cpp \
    ../Algorithm/AvailabilityCostCalculator.cpp \
    ../Algorithm/QuorumListCalculator.cpp \
    ../Algorithm/QuorumListComparator.cpp \
    ../Algorithm/QuorumUtil.cpp \
    ../Config/Path.cpp \
    ../Models/MainModel.cpp \
    ../Models/Edge.cpp \
    ../Models/Node.cpp \
    ../Models/QuorumList.cpp \
    ../Models/Topology.cpp \
    ../Models/VotingStructure.cpp \
    ../Persistence/CsvExport.cpp \
    ../Persistence/QuorumListReader.cpp \
    ../Persistence/QuorumListWriter.cpp \
    ../Persistence/TopologyReader.cpp \
    ../Persistence/TopologyWriter.cpp \
    ../Persistence/Unzip.cpp \
    ../Persistence/VotingStructurePersistenceBase.cpp \
    ../Persistence/VotingStructureReader.cpp \
    ../Persistence/VotingStructureWriter.cpp \
    ../Persistence/Zip.cpp

HEADERS += \
    ../Algorithm/AvailabilityCostCalculator.h \
    ../Algorithm/QuorumListCalculator.h \
    ../Algorithm/QuorumListComparator.h \
    ../Algorithm/QuorumUtil.h \
    ../Config/Path.h \
    ../Models/MainModel.h \
    ../Models/Edge.h \
    ../Models/Node.h \
    ../Models/QuorumList.h \
    ../Models/Topology.h \
    ../Models/VotingStructure.h \
    ../Persistence/CsvExport.h \
    ../Persistence/QuorumListReader.h \
    ../Persistence/QuorumListWriter.h \
    ../Persistence/TopologyReader.h \
    ../Persistence/TopologyWriter.h \
    ../Persistence/Unzip.h \
    ../Persistence/VotingStructurePersistenceBase.h \
    ../Persistence/VotingStructureReader.h \
    ../Persistence/VotingStructureWriter.h \
    ../Persistence/Zip.h

RESOURCES += \
    strategies.qrc

DISTFILES += \
    cbp3x3.gsv \
    gp_2x2.gsv \
    gp_2x3.gsv \
    gp_2x4.gsv \
    gp_2x5.gsv \
    gp_3x2.gsv \
    gp_3x3.gsv \
    gp_3x4.gsv \
    gp_3x5.gsv \
    gp_4x2.gsv \
    gp_4x3.gsv \
    gp_4x4.gsv \
    gp_4x5.gsv \
    gp_5x2.gsv \
    gp_5x3.gsv \
    gp_5x4.gsv \
    gp_5x5.gsv \
    gpo_2x2.gsv \
    gpo_2x3.gsv \
    gpo_2x4.gsv \
    gpo_2x5.gsv \
    gpo_3x2.gsv \
    gpo_3x3.gsv \
    gpo_3x4.gsv \
    gpo_3x5.gsv \
    gpo_4x2.gsv \
    gpo_4x3.gsv \
    gpo_4x4.gsv \
    gpo_4x5.gsv \
    gpo_5x2.gsv \
    gpo_5x3.gsv \
    gpo_5x4.gsv \
    gpo_5x5.gsv \
    mcs10.gsv \
    mcs11.gsv \
    mcs12.gsv \
    mcs13.gsv \
    mcs14.gsv \
    mcs15.gsv \
    mcs3.gsv \
    mcs4.gsv \
    mcs6.gsv \
    mcs7.gsv \
    mcs8.gsv \
    mcs9.gsv \
    rowa10.gsv \
    rowa2.gsv \
    rowa3.gsv \
    rowa4.gsv \
    rowa5.gsv \
    rowa6.gsv \
    rowa7.gsv \
    rowa8.gsv \
    rowa9.gsv \
    tlp2x2.gsv \
    tlp2x3.gsv \
    tlp2x4.gsv \
    tlp2x5.gsv \
    tlp3x2.gsv \
    tlp3x3.gsv \
    tlp3x4.gsv \
    tlp3x5.gsv \
    tlp4x2.gsv \
    tlp4x3.gsv \
    tlp4x4.gsv \
    tlp5x2.gsv \
    tlp5x3.gsv \
    tqp_3_1.gsv \
    tqp_3_2.gsv \
    tqp_3_3.gsv \
    tqp_4_1.gsv \
    tqp_4_2.gsv \
    tqp_5_1.gsv \
    votingstructure.xsd \
    votingstructure_meta.xsd \
    mcs5.gsv
