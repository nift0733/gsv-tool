#include "PenaltyResetButton.h"

PenaltyResetButton::PenaltyResetButton(std::shared_ptr<Edge> edge, int penaltyIndex, int resetValue, QWidget *parent)
    : QPushButton(resetValue < 0 ? PRIO_FILLUP : PRIO_INF, parent), edge(edge), penaltyIndex(penaltyIndex), resetValue(resetValue) {
    QObject::connect(this, &PenaltyResetButton::clicked, this, &PenaltyResetButton::onClick);
}

void PenaltyResetButton::onClick() {
    edge->penaltyList[penaltyIndex] = resetValue;
    emit penaltyChanged();
}
