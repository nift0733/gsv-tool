#include "GraphRenderer.h"

#include <QtMath>
#include <QMouseEvent>
#include <QPainter>

#define DIAMETER 72
#define RADIUS 36
#define TEXT_H 20
#define TEXT_H2 10
#define TEXT_D 16
#define SELECT_EDGE_RADIUS 16
#define ARRANGE_DELTA 12
#define ANGLE_IN_RANGE(X, Y, Z) (Y + 0.05 <= X && X <= Z - 0.05)
#define ANGLE_EQ(X, Y) (Y - 0.05 <= X && X <= Y + 0.05)
#define SIGN(X1, X2) (X1 > X2 ? 1 : (X2 > X1 ? -1 : 0))
#define IS_BETWEEN(X, X1, X2) ((X1 <= X && X <= X2) || (X2 <= X && X <= X1))
#define MAX(X, Y) (X >= Y ? X : Y)
#define MIN(X, Y) (X <= Y ? X : Y)

GraphRenderer::GraphRenderer(QWidget *parent, std::shared_ptr<MainModel> model) : QWidget(parent),
    nodePen(QColor("#000000")),
    edgePen(QBrush(QColor("#000000")), 2),
    selectedPen(QBrush(QColor("#3F3FFF")), 4),
    rootPen(QColor("#5F000000")),
    virtualNodeBrush(QColor("#FFFF00")),
    physicalNodeBrush(QColor("#3FFF3F")),
    selectedNodeBrush(Qt::NoBrush),
    model(model),
    viewPosition(0,0),
    scale(1.0),
    draggedNode(nullptr),
    tempMousePoint(0,0),
    originalViewPosition(0,0),
    isScrolling(false) {
    //
}

void GraphRenderer::arrange() {
    lineArrange(&Node::x, [] (std::shared_ptr<Node> node, int value) { node->x = value; });
    lineArrange(&Node::y, [] (std::shared_ptr<Node> node, int value) { node->y = value; });
    model->setVotingStructureChanged(true);
    update();
}

void GraphRenderer::resetViewPosition() {
    viewPosition.setX(0);
    viewPosition.setY(0);
    update();
}

void GraphRenderer::resetScale() {
    auto oldScale = scale;
    auto center = QPoint(size().width()/2, size().height()/2);

    scale = 1.0;
    viewPosition = (viewPosition - center) * scale / oldScale + center;
    update();
}

int GraphRenderer::getRelXPos() {
    return viewPosition.x();
}

int GraphRenderer::getRelYPos() {
    return viewPosition.y();
}

void GraphRenderer::mousePressEvent(QMouseEvent *event) {
    if(model->isVotingStructureLoaded() && handlePress(event))
        update();
}

void GraphRenderer::mouseMoveEvent(QMouseEvent *event) {
    if(model->isVotingStructureLoaded() && handleMove(event))
        update();
}

void GraphRenderer::mouseReleaseEvent(QMouseEvent *event) {
    if(model->isVotingStructureLoaded() && handleRelease(event))
        update();
}

void GraphRenderer::wheelEvent(QWheelEvent *event) {
    if(model->isVotingStructureLoaded()) {
        auto oldScale = scale;
        if(event->delta() > 0)
            scale /= 0.9;
        else if(event->delta() < 0)
            scale *= 0.9;
        viewPosition = (viewPosition - event->pos()) * scale / oldScale + event->pos();
        update();
    }
}

bool GraphRenderer::handlePress(QMouseEvent *event) {
    int x = (event->x() - viewPosition.x()) / scale;
    int y = (event->y() - viewPosition.y()) / scale;
    std::shared_ptr<Node> node;
    switch(state) {
    case Move:
        if(event->button() == Qt::MouseButton::RightButton && draggedNode) {
            draggedNode->x = tempMousePoint.x();
            draggedNode->y = tempMousePoint.y();
            draggedNode.reset();
            return true;
        }
        if(event->button() == Qt::MouseButton::RightButton && isScrolling) {
            viewPosition.setX(originalViewPosition.x());
            viewPosition.setY(originalViewPosition.y());
            isScrolling = false;
            return true;
        }
        if(event->button() != Qt::MouseButton::LeftButton)
            return false;
        for(std::shared_ptr<Node> node : model->votingStructure->getNodes()) {
            if(node->x - RADIUS <= x && x <= node->x + RADIUS &&
               node->y - RADIUS <= y && y <= node->y + RADIUS) {
                draggedNode = node;
                tempMousePoint.setX(draggedNode->x);
                tempMousePoint.setY(draggedNode->y);
                model->selectNode(draggedNode);
                return true;
            }
        }
        for(std::shared_ptr<Edge> edge : model->votingStructure->getEdges()) {
            int x1 = edge->from->x;
            int y1 = edge->from->y;
            int x2 = edge->to->x;
            int y2 = edge->to->y;

            if(qAbs(x2 - x1) >= qAbs(y2 - y1) && IS_BETWEEN(x, x1, x2)) {
                int py = ((0.0 + y2 - y1) / (0.0 + x2 - x1)) * (x - x1) + y1;
                if(py - SELECT_EDGE_RADIUS <= y && y <= py + SELECT_EDGE_RADIUS) {
                    model->selectEdge(edge);
                    return true;
                }
            }
            if(qAbs(y2 - y1) >= qAbs(x2 - x1) && IS_BETWEEN(y, y1, y2)) {
                int px = ((0.0 + x2 - x1) / (0.0 + y2 - y1)) * (y - y1) + x1;
                if(px - SELECT_EDGE_RADIUS <= x && x <= px + SELECT_EDGE_RADIUS) {
                    model->selectEdge(edge);
                    return true;
                }
            }
        }
        model->unselectAll();
        tempMousePoint.setX(event->x());
        tempMousePoint.setY(event->y());
        originalViewPosition.setX(viewPosition.x());
        originalViewPosition.setY(viewPosition.y());
        isScrolling = true;
        return true;
    case Remove:
        for(std::shared_ptr<Node> node : model->votingStructure->getNodes()) {
            if(node->x - RADIUS <= x && x <= node->x + RADIUS &&
               node->y - RADIUS <= y && y <= node->y + RADIUS) {
                if(model->votingStructure->removeNode(node)) {
                    model->setVotingStructureChanged(true);
                    return true;
                }
            }
        }
        for(std::shared_ptr<Edge> edge : model->votingStructure->getEdges()) {
            int x1 = edge->from->x;
            int y1 = edge->from->y;
            int x2 = edge->to->x;
            int y2 = edge->to->y;

            if(qAbs(x2 - x1) >= qAbs(y2 - y1) && IS_BETWEEN(x, x1, x2)) {
                int py = ((0.0 + y2 - y1) / (0.0 + x2 - x1)) * (x - x1) + y1;
                if(py - SELECT_EDGE_RADIUS <= y && y <= py + SELECT_EDGE_RADIUS) {
                    if(model->votingStructure->removeEdge(edge)) {
                        model->setVotingStructureChanged(true);
                        return true;
                    }
                }
            }
            if(qAbs(y2 - y1) >= qAbs(x2 - x1) && IS_BETWEEN(y, y1, y2)) {
                int px = ((0.0 + x2 - x1) / (0.0 + y2 - y1)) * (y - y1) + x1;
                if(px - SELECT_EDGE_RADIUS <= x && x <= px + SELECT_EDGE_RADIUS) {
                    if(model->votingStructure->removeEdge(edge)) {
                        model->setVotingStructureChanged(true);
                        return true;
                    }
                }
            }
        }
        return false;
    case AddPhysical:
        if(event->button() != Qt::MouseButton::LeftButton)
            return false;
        node = model->votingStructure->addNode(false, x, y);
        model->selectNode(node);
        node->identifier = model->topology->getNodes()[0];
        model->setVotingStructureChanged(true);
        return true;
    case AddVirtual:
        if(event->button() != Qt::MouseButton::LeftButton)
            return false;
        model->selectNode(model->votingStructure->addNode(true, x, y));
        model->setVotingStructureChanged(true);
        return true;
    case AddEdge:
        if(event->button() == Qt::MouseButton::RightButton) {
            draggedNode.reset();
            return true;
        }
        if(event->button() != Qt::MouseButton::LeftButton)
            return false;
        for(std::shared_ptr<Node> node : model->votingStructure->getNodes()) {
            if(node->x - RADIUS <= x && x <= node->x + RADIUS &&
               node->y - RADIUS <= y && y <= node->y + RADIUS) {
                draggedNode = node;
                tempMousePoint.setX(x);
                tempMousePoint.setY(y);
                return true;
            }
        }
        return false;
    }
    return false;
}

bool GraphRenderer::handleMove(QMouseEvent *event) {
    if(!draggedNode && !isScrolling)
        return false;

    if(isScrolling) {
        viewPosition.setX(event->x() - tempMousePoint.x() + originalViewPosition.x());
        viewPosition.setY(event->y() - tempMousePoint.y() + originalViewPosition.y());
        return true;
    }

    int x = (event->x() - viewPosition.x()) / scale;
    int y = (event->y() - viewPosition.y()) / scale;

    switch (state) {
    case Move:
        draggedNode->x = x;
        draggedNode->y = y;
        return true;
    case Remove:
        return false;
    case AddPhysical:
        return false;
    case AddVirtual:
        return false;
    case AddEdge:
        tempMousePoint.setX(x);
        tempMousePoint.setY(y);
        return true;
    }
    return false;
}

bool GraphRenderer::handleRelease(QMouseEvent *event) {
    if((!draggedNode && !isScrolling) || event->button() != Qt::MouseButton::LeftButton)
        return false;

    if(isScrolling) {
        isScrolling = false;
        return false;
    }

    int x = (event->x() - viewPosition.x()) / scale;
    int y = (event->y() - viewPosition.y()) / scale;

    switch (state) {
    case Move:
        if(draggedNode->x != tempMousePoint.x() || draggedNode->y != tempMousePoint.y())
            model->setVotingStructureChanged(true);
        draggedNode.reset();
        return true;
    case Remove:
        return false;
    case AddPhysical:
        return false;
    case AddVirtual:
        return false;
    case AddEdge:
        for(std::shared_ptr<Node> node : model->votingStructure->getNodes()) {
            if(node != draggedNode &&
               node->x - RADIUS <= x && x <= node->x + RADIUS &&
               node->y - RADIUS <= y && y <= node->y + RADIUS) {
                model->setVotingStructureChanged(true);
                model->selectEdge(model->votingStructure->addEdge(draggedNode, node));                
            }
        }
        draggedNode.reset();
        return true;
    }
    return false;
}

void GraphRenderer::paintEvent(QPaintEvent*) {
    if(!model->isVotingStructureLoaded())
        return;

    QPainter painter(this);
    painter.translate(viewPosition);
    painter.scale(scale, scale);
    painter.setRenderHint(QPainter::Antialiasing, true);

    // draw edge to add
    if(state == AddEdge && draggedNode) {
        drawEdge(painter, draggedNode->x, draggedNode->y, tempMousePoint.x(), tempMousePoint.y());
    }

    // draw virtual nodes
    painter.setPen(nodePen);
    painter.setBrush(virtualNodeBrush);
    for(std::shared_ptr<Node> node : model->votingStructure->getNodes()) {
        if(!node->isVirtual)
            continue;
        painter.drawRoundedRect(node->x - RADIUS, node->y - RADIUS, DIAMETER, DIAMETER, 16, 16);
        painter.drawLine(node->x - RADIUS, node->y - TEXT_D, node->x + RADIUS, node->y - TEXT_D);
        painter.drawLine(node->x - RADIUS, node->y + TEXT_D, node->x + RADIUS, node->y + TEXT_D);
        mayDrawRoot(painter, node);
        painter.setFont(QFont("Helvetica", 16, QFont::Bold));
        painter.drawText(QRect(node->x - RADIUS, node->y - TEXT_H2, DIAMETER, TEXT_H), Qt::AlignCenter | Qt::AlignVCenter, node->identifier);
        painter.setFont(QFont("Helvetica", 12));
        painter.drawText(QRect(node->x - RADIUS, node->y - RADIUS, DIAMETER, TEXT_H), Qt::AlignCenter | Qt::AlignVCenter, QString::number(node->vote));

        QStringList list = QVector<QString>(node->quorumVector.size()).toList();
        std::transform(node->quorumVector.begin(), node->quorumVector.end(), list.begin(), [] (int q) { return QString::number(q); });
        painter.drawText(QRect(node->x - RADIUS, node->y + TEXT_D, DIAMETER, TEXT_H), Qt::AlignCenter | Qt::AlignVCenter, list.join(","));

        if(model->getSelectedNode() == node) {
            painter.setPen(selectedPen);
            painter.setBrush(selectedNodeBrush);
            painter.drawRoundedRect(node->x - RADIUS - 1, node->y - RADIUS - 1, DIAMETER + 2, DIAMETER + 2, 16, 16);
            painter.setPen(nodePen);
            painter.setBrush(virtualNodeBrush);
        }
    }

    // draw physical nodes
    painter.setPen(nodePen);
    painter.setBrush(physicalNodeBrush);
    for(std::shared_ptr<Node> node : model->votingStructure->getNodes()) {
        if(node->isVirtual)
            continue;
        painter.drawRect(node->x - RADIUS, node->y - RADIUS, DIAMETER, DIAMETER);
        painter.drawLine(node->x - RADIUS, node->y - TEXT_D, node->x + RADIUS, node->y - TEXT_D);
        painter.drawLine(node->x - RADIUS, node->y + TEXT_D, node->x + RADIUS, node->y + TEXT_D);
        mayDrawRoot(painter, node);
        painter.setFont(QFont("Helvetica", 16, QFont::Bold));
        painter.drawText(QRect(node->x - RADIUS, node->y - TEXT_H2, DIAMETER, TEXT_H), Qt::AlignCenter | Qt::AlignVCenter, node->identifier);
        painter.setFont(QFont("Helvetica", 12));
        painter.drawText(QRect(node->x - RADIUS, node->y - RADIUS, DIAMETER, TEXT_H), Qt::AlignCenter | Qt::AlignVCenter, QString::number(node->vote));

        QStringList list = QVector<QString>(node->quorumVector.size()).toList();
        std::transform(node->quorumVector.begin(), node->quorumVector.end(), list.begin(), [] (int q) { return QString::number(q); });
        painter.drawText(QRect(node->x - RADIUS, node->y + TEXT_D, DIAMETER, TEXT_H), Qt::AlignCenter | Qt::AlignVCenter, list.join(","));

        if(model->getSelectedNode() == node) {
            painter.setPen(selectedPen);
            painter.setBrush(selectedNodeBrush);
            painter.drawRect(node->x - RADIUS - 1, node->y - RADIUS - 1, DIAMETER + 2, DIAMETER + 2);
            painter.setPen(nodePen);
            painter.setBrush(physicalNodeBrush);
        }
    }

    // draw edges
    painter.setPen(edgePen);
    for(std::shared_ptr<Edge> edge : model->votingStructure->getEdges()) {
        int ox1 = edge->from->x;
        int oy1 = edge->from->y;
        int ox2 = edge->to->x;
        int oy2 = edge->to->y;

        int x1 = ox1 + RADIUS * SIGN(ox2, ox1);
        int y1 = oy1 + RADIUS * SIGN(oy2, oy1);
        int x2 = ox2 + RADIUS * SIGN(ox1, ox2);
        int y2 = oy2 + RADIUS * SIGN(oy1, oy2);

        if(ox1 != ox2) {
            double ny1 = (0.0 + oy2 - oy1) / (0.0 + qAbs(ox2 - ox1)) * RADIUS + oy1;
            double ny2 = (0.0 + oy1 - oy2) / (0.0 + qAbs(ox1 - ox2)) * RADIUS + oy2;
            if(oy1 - RADIUS <= ny1 && ny1 <= oy1 + RADIUS)
                y1 = ny1;
            if(oy2 - RADIUS <= ny2 && ny2 <= oy2 + RADIUS)
                y2 = ny2;
        }
        if(oy1 != oy2) {
            double nx1 = (0.0 + ox2 - ox1) / (0.0 + qAbs(oy2 - oy1)) * RADIUS + ox1;
            double nx2 = (0.0 + ox1 - ox2) / (0.0 + qAbs(oy1 - oy2)) * RADIUS + ox2;
            if(ox1 - RADIUS <= nx1 && nx1 <= ox1 + RADIUS)
                x1 = nx1;
            if(ox2 - RADIUS <= nx2 && nx2 <= ox2 + RADIUS)
                x2 = nx2;
        }

        if(model->getSelectedEdge() == edge)
            painter.setPen(selectedPen);
        double alpha = drawEdge(painter, x1, y1, x2, y2);
        if(model->getSelectedEdge() == edge)
            painter.setPen(edgePen);

        int mx = ox1 + (ox2 - ox1) / 2;
        int my = oy1 + (oy2 - oy1) / 2;
        int alignment = Qt::AlignCenter;
        if(ANGLE_EQ(alpha, 0) || ANGLE_EQ(alpha, M_PI) || ANGLE_EQ(alpha, 2 * M_PI)) {
            mx -= RADIUS;
            my -= TEXT_H;
        } else if(ANGLE_EQ(alpha, M_PI_2) || ANGLE_EQ(alpha, 3 * M_PI_2)) {
            mx += 2;
            my -= TEXT_H2;
            alignment = Qt::AlignLeft;
        } else if(ANGLE_IN_RANGE(alpha, M_PI, 3 * M_PI_2)) {
            my -= TEXT_H;
            alignment = Qt::AlignLeft;
        } else if(ANGLE_IN_RANGE(alpha, 0, M_PI_2)) {
            mx -= DIAMETER + 4;
            my -= TEXT_H2;
            alignment = Qt::AlignRight;
        } else if(ANGLE_IN_RANGE(alpha, M_PI_2, M_PI)) {
            mx += 4;
            my -= TEXT_H2;
            alignment = Qt::AlignLeft;
        } else {
            mx -= DIAMETER;
            my -= TEXT_H;
            alignment = Qt::AlignRight;
        }

        // penalty text
        QRect rect(mx, my, DIAMETER, TEXT_H);
        QStringList list = QVector<QString>(edge->penaltyList.size()).toList();
        std::transform(edge->penaltyList.begin(), edge->penaltyList.end(), list.begin(), [] (int p) { return p>=MAX_PENALTY ? PRIO_INF : p<0 ? PRIO_FILLUP : QString::number(p); });
        painter.drawText(rect, alignment | Qt::AlignVCenter, list.join(","));
    }
}

double GraphRenderer::drawEdge(QPainter& painter, int x1, int y1, int x2, int y2) {
    // main line
    painter.drawLine(x1, y1, x2, y2);

    double alpha = x2 == x1 ? (y2 > y1 ? -M_PI_2 : M_PI_2) : qAtan((0.0 + y2-y1) / (0.0 + x2-x1));
    if(x2 > x1)
        alpha += M_PI;
    while(alpha < 0)
        alpha += 2 * M_PI;
    while(alpha > 2 * M_PI)
        alpha -= 2 * M_PI;
    int x3 = x2 + qCos(alpha - M_PI_4 * 0.8) * 10;
    int y3 = y2 + qSin(alpha - M_PI_4 * 0.8) * 10;
    int x4 = x2 + qCos(alpha + M_PI_4 * 0.8) * 10;
    int y4 = y2 + qSin(alpha + M_PI_4 * 0.8) * 10;

    // arrow head
    painter.drawLine(x2, y2, x3, y3);
    painter.drawLine(x2, y2, x4, y4);

    return alpha;
}

void GraphRenderer::mayDrawRoot(QPainter &painter, std::shared_ptr<Node> node) {
    if(model->votingStructure->getRoot() != node)
        return;

    painter.setPen(rootPen);
    painter.drawLine(node->x - RADIUS, node->y, node->x - RADIUS + TEXT_D, node->y - TEXT_D + 1);
    painter.drawLine(node->x + RADIUS, node->y, node->x + RADIUS - TEXT_D, node->y + TEXT_D);
    painter.drawLine(node->x - TEXT_D, node->y + TEXT_D, node->x + TEXT_D, node->y - TEXT_D + 1);
    painter.drawLine(node->x - RADIUS/2 - TEXT_D, node->y + TEXT_D, node->x - RADIUS/2 + TEXT_D, node->y - TEXT_D + 1);
    painter.drawLine(node->x + RADIUS/2 - TEXT_D, node->y + TEXT_D, node->x + RADIUS/2 + TEXT_D, node->y - TEXT_D + 1);
    painter.drawLine(node->x - RADIUS + TEXT_D, node->y + TEXT_D, node->x - RADIUS, node->y);
    painter.drawLine(node->x + RADIUS - TEXT_D, node->y - TEXT_D + 1, node->x + RADIUS, node->y);
    painter.drawLine(node->x - TEXT_D, node->y - TEXT_D + 1, node->x + TEXT_D, node->y + TEXT_D);
    painter.drawLine(node->x - RADIUS/2 - TEXT_D, node->y - TEXT_D + 1, node->x - RADIUS/2 + TEXT_D, node->y + TEXT_D);
    painter.drawLine(node->x + RADIUS/2 - TEXT_D, node->y - TEXT_D + 1, node->x + RADIUS/2 + TEXT_D, node->y + TEXT_D);
    painter.setPen(nodePen);
}

void GraphRenderer::lineArrange(std::function<int (std::shared_ptr<Node>)> getter, std::function<void (std::shared_ptr<Node>, int)> setter) {
    QList<std::shared_ptr<Node>> nodes;
    for(auto node : model->votingStructure->getNodes())
        nodes.append(node);
    while(nodes.length() > 1) {
        QList<std::shared_ptr<Node>> groupedNodes;
        auto firstNode = nodes.takeFirst();
        groupedNodes.append(firstNode);
        int min = getter(firstNode) - ARRANGE_DELTA;
        int max = getter(firstNode) + ARRANGE_DELTA;
        int index = 0;
        while(index < nodes.length()) {
            int value = getter(nodes.at(index));
            if(min <= value && value <= max) {
                auto node = nodes.takeAt(index);
                groupedNodes.append(node);
                min = MIN(min, getter(node) - ARRANGE_DELTA);
                max = MAX(max, getter(node) + ARRANGE_DELTA);
            } else {
                ++index;
            }
        }
        int value = (min + max) / 2;
        for(auto node : groupedNodes)
            setter(node, value);
    }
}
