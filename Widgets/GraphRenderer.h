#ifndef GRAPHRENDERER_H
#define GRAPHRENDERER_H

#include <QPen>
#include <QWidget>
#include <functional>

#include "../Models/MainModel.h"

class GraphRenderer : public QWidget
{
    Q_OBJECT
public:
    explicit GraphRenderer(QWidget *parent = nullptr, std::shared_ptr<MainModel> model = nullptr);

    /**
     * @brief Arranges the nodes in the canvas.
     */
    void arrange();
    /**
     * @brief Resets the view position to the last saved.
     */
    void resetViewPosition();
    /**
     * @brief Resets the scale to 100%.
     */
    void resetScale();
    /**
     * @brief Returns the current x scroll.
     */
    int getRelXPos();
    /**
     * @brief Returns the current y scroll.
     */
    int getRelYPos();

    enum EditState {
        Move,
        Remove,
        AddPhysical,
        AddVirtual,
        AddEdge,
    };
    EditState state;

protected:
    void paintEvent(QPaintEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;

private:
    const QPen nodePen;
    const QPen edgePen;
    const QPen selectedPen;
    const QPen rootPen;
    const QBrush virtualNodeBrush;
    const QBrush physicalNodeBrush;
    const QBrush selectedNodeBrush;

    std::shared_ptr<MainModel> model;
    QPoint viewPosition;
    double scale;
    std::shared_ptr<Node> draggedNode;
    QPoint tempMousePoint;
    QPoint originalViewPosition;
    bool isScrolling;

    /**
     * @brief Handles a press event of a button of the mouse.
     */
    bool handlePress(QMouseEvent *event);
    /**
     * @brief Handles a move event of the mouse.
     */
    bool handleMove(QMouseEvent *event);
    /**
     * @brief Handles a release event of a button of the mouse.
     */
    bool handleRelease(QMouseEvent *event);
    /**
     * @brief Draws an edge.
     */
    double drawEdge(QPainter& painter, int x1, int y1, int x2, int y2);
    /**
     * @brief Draws the marking for the root node if node is the root.
     */
    void mayDrawRoot(QPainter& painter, std::shared_ptr<Node> node);

    /**
     * @brief Arranges nodes to a line in x or y direction.
     */
    void lineArrange(std::function<int(std::shared_ptr<Node>)> getter, std::function<void(std::shared_ptr<Node>, int)> setter);
};

#endif // GRAPHRENDERER_H
