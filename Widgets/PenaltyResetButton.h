#ifndef PENALTYRESETBUTTON_H
#define PENALTYRESETBUTTON_H

#include <QPushButton>

#include "../Models/Edge.h"

class PenaltyResetButton : public QPushButton
{
    Q_OBJECT
public:
    PenaltyResetButton(std::shared_ptr<Edge> edge, int penaltyIndex, int resetValue, QWidget *parent = nullptr);

signals:
    void penaltyChanged();

private:
    std::shared_ptr<Edge> edge;
    int penaltyIndex;
    int resetValue;

private slots:
    void onClick();
};

#endif // PENALTYRESETBUTTON_H
