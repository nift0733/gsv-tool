#ifndef TLPGENERATOR_H
#define TLPGENERATOR_H

#include <memory>

#include "../Models/VotingStructure.h"
#include "../Models/Topology.h"

class TlpGenerator
{
public:
    /**
     * @brief Generates a voting structure for TLP cxr and the underlying topology given.
     */
    static std::shared_ptr<VotingStructure> generate(std::shared_ptr<Topology> topology, int columns, int rows);

private:
    /**
     * @brief Adds a node to the voting structure with the specified properties.
     */
    static std::shared_ptr<Node> addNode(std::shared_ptr<VotingStructure> votingStructure, bool isVirtual, QString identifier, int vote, int read, int write, int x, int y);
    /**
     * @brief Adds the physical nodes of TLP cxr to the voting structure.
     */
    static void addPhysicalNodes(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology, int columns, int rows, int rx);
    /**
     * @brief Adds the proxy nodes of TLP cxr to the voting structure.
     */
    static void addProxyNodes(std::shared_ptr<VotingStructure> votingStructure, int columns, int rows, int rx);
    /**
     * @brief Adds the vertical and horizontal nodes of TLP cxr to the voting structure.
     */
    static void addGraphNodes(std::shared_ptr<VotingStructure> votingStructure, int columns, int rows, int offsetX, int rx);
    /**
     * @brief Adds an edge to the voting structure resp. two edges in both directions if extended is true.
     */
    static void addEdge(std::shared_ptr<VotingStructure> votingStructure, QString from, QString to, int penalty, bool extended);
    /**
     * @brief Add the edges between vertical nodes.
     */
    static void addVEdges(std::shared_ptr<VotingStructure> votingStructure, int columns, int rows);
    /**
     * @brief Add the edges between horizontal nodes.
     */
    static void addHEdges(std::shared_ptr<VotingStructure> votingStructure, int columns, int rows);
    /**
     * @brief Add the edges to proxy nodes.
     */
    static void addPEdges(std::shared_ptr<VotingStructure> votingStructure, int columns, int rows);
    /**
     * @brief Add the edges to physical nodes.
     */
    static void addREdges(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology, int columns, int rows);

    /**
     * @brief Constructs the name "{prefix}{number}".
     */
    static QString name(QString prefix, int number);
};

#endif // TLPGENERATOR_H
