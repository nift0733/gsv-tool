#include "TlpGenerator.h"

#include <algorithm>

std::shared_ptr<VotingStructure> TlpGenerator::generate(std::shared_ptr<Topology> topology, int columns, int rows) {
    std::vector<QString> operations;
    operations.push_back("read");
    operations.push_back("write");
    auto result = std::make_shared<VotingStructure>(operations);

    auto offsetX = qMax(0, columns * rows + 1 - 2 * (columns + rows - 1));
    auto rx = offsetX + 2 * (columns + rows - 1);
    auto ry = 0;
    auto vx = offsetX + 2 * (rows - 1) + columns - 1;
    auto vy = 2;
    auto hx = rx + 2 + rows - 1;
    auto hy = 2;

    auto root = addNode(result, true, "R", 1, 1, 2, rx, ry);
    result->setRoot(root);

    addNode(result, true, "V", 1, 1, 1, vx, vy);
    addNode(result, true, "H", 1, 1, 1, hx, hy);
    addGraphNodes(result, columns, rows, offsetX, rx);
    addProxyNodes(result, columns, rows, rx);
    addPhysicalNodes(result, topology, columns, rows, rx);

    addEdge(result, "R", "V", -1, false);
    addEdge(result, "R", "H", -1, false);
    addVEdges(result, columns, rows);
    addHEdges(result, columns, rows);
    addPEdges(result, columns, rows);
    addREdges(result, topology, columns, rows);

    return result;
}

std::shared_ptr<Node> TlpGenerator::addNode(std::shared_ptr<VotingStructure> votingStructure, bool isVirtual, QString identifier, int vote, int read, int write, int x, int y) {
    auto node = votingStructure->addNode(isVirtual, 50 + x*50, 50 + y*50);
    node->identifier = identifier;
    node->vote = vote;
    node->quorumVector[0] = read;
    node->quorumVector[1] = write;
    return node;
}

void TlpGenerator::addPhysicalNodes(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology, int columns, int rows, int rx) {
    for(int k=0; k<columns*rows; ++k) {
        addNode(votingStructure, false, topology->getNodes()[k], 1, 0, 0, rx - columns * rows + 2*k + 1, 2 * (3 + columns + rows));
    }
}

void TlpGenerator::addProxyNodes(std::shared_ptr<VotingStructure> votingStructure, int columns, int rows, int rx) {
    for(int k=0; k<columns*rows; ++k) {
        addNode(votingStructure, true, name("P", k+1), 6, 1, 1, rx - columns * rows + 2*k + 1, 2 * (2 + columns + rows));
    }
}

void TlpGenerator::addGraphNodes(std::shared_ptr<VotingStructure> votingStructure, int columns, int rows, int offsetX, int rx) {
    for(int k=0; k<columns*rows; ++k) {
        auto quorum = k/columns == rows - 1 ? 6 : 7;
        auto x = offsetX + 2 * (k%columns + rows - 1 - k/columns);
        auto y = 1 + 2 * (2 + k%columns + k/columns);
        addNode(votingStructure, true, name("V", k+1), 1, quorum, quorum, x, y);

        quorum = k%columns == columns - 1 ? 6 : 7;
        addNode(votingStructure, true, name("H", k+1), 1, quorum, quorum, x + rx + 2 - offsetX, y);
    }
}

void TlpGenerator::addEdge(std::shared_ptr<VotingStructure> votingStructure, QString from, QString to, int penalty, bool extended) {
    auto nodes = votingStructure->getNodes();
    auto fromNode = std::find_if(nodes.begin(), nodes.end(), [&from](std::shared_ptr<Node> node) {return node->identifier == from;});
    auto toNode = std::find_if(nodes.begin(), nodes.end(), [&to](std::shared_ptr<Node> node) {return node->identifier == to;});
    auto edge = votingStructure->addEdge(*fromNode, *toNode);
    edge->penaltyList[0] = penalty;
    edge->penaltyList[1] = penalty;
    if(extended) {
        edge = votingStructure->addEdge(*toNode, *fromNode);
        edge->penaltyList[0] = penalty;
        edge->penaltyList[1] = penalty;
    }
}

void TlpGenerator::addVEdges(std::shared_ptr<VotingStructure> votingStructure, int columns, int rows) {
    for(int k=1; k<=columns; ++k) {
        addEdge(votingStructure, "V", name("V", k), -1, false);
        addEdge(votingStructure, name("V", k), name("V", k+columns), -1, false);
        if(k!=columns)
            addEdge(votingStructure, name("V", k), name("V", k+columns+1), -1, false);
    }
    for(int k=columns+1; k<=columns*(rows-1); ++k) {
        if((k-1)%columns != columns-1) {
            addEdge(votingStructure, name("V", k), name("V", k+1), -1, true);
            addEdge(votingStructure, name("V", k), name("V", k+columns+1), -1, k<=columns*(rows-2));
        }
        addEdge(votingStructure, name("V", k), name("V", k+columns), -1, k<=columns*(rows-2));
    }
}

void TlpGenerator::addHEdges(std::shared_ptr<VotingStructure> votingStructure, int columns, int rows) {
    for(int k=0; k<rows; ++k) {
        addEdge(votingStructure, "H", name("H", k*columns+1), -1, false);
        addEdge(votingStructure, name("H", k*columns+1), name("H", k*columns+2), -1, false);
        if(k!=rows-1)
            addEdge(votingStructure, name("H", k*columns+1), name("H", k*columns+columns+2), -1, false);
    }
    for(int k=1; k<=rows*columns; ++k) {
        if((k-1)%columns == 0 || (k-1)%columns == columns-1)
            continue;
        addEdge(votingStructure, name("H", k), name("H", k+1), -1, (k-1)%columns != columns-2);
        if(k < columns*(rows-1)) {
            addEdge(votingStructure, name("H", k), name("H", k+columns+1), -1, (k-1)%columns != columns-2);
            addEdge(votingStructure, name("H", k), name("H", k+columns), -1, true);
        }
    }
}

void TlpGenerator::addPEdges(std::shared_ptr<VotingStructure> votingStructure, int columns, int rows) {
    for(int k=1; k<=columns*rows; ++k) {
        addEdge(votingStructure, name("V", k), name("P", k), -1, false);
        addEdge(votingStructure, name("H", k), name("P", k), -1, false);
    }
}

void TlpGenerator::addREdges(std::shared_ptr<VotingStructure> votingStructure, std::shared_ptr<Topology> topology, int columns, int rows) {
    auto nodes = topology->getNodes();
    for(int k=0; k<columns*rows; ++k) {
        addEdge(votingStructure, name("P", k+1), nodes[k], 1, false);
    }
}

QString TlpGenerator::name(QString prefix, int number) {
    return prefix + QString::number(number);
}
